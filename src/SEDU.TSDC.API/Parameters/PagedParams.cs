﻿

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Parameters
{
    public class PagedParams
    {
        public string keyword { get; set; }
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
    }
}
