﻿using Microsoft.EntityFrameworkCore;
using SEDU.TSDC.EntityFramework.DbContexts.Interfaces;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.EntityFramework.Extensions.Extensions;
using SEDU.TSDC.EntityFramework.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Repositories
{
    public class HoiDongTuyenSinhRepository<TDbContext> : IHoiDongTuyenSinhRepository
        where TDbContext : ITsdcDbContext
    {
        protected readonly TDbContext dbContext;

        public HoiDongTuyenSinhRepository(TDbContext _dbContext)
        {
            dbContext = _dbContext;
        }

        public async Task<HOI_DONG_TUYEN_SINH> CreateHoiDongTuyenSinhAsync(HOI_DONG_TUYEN_SINH hoiDongTuyenSinh)
        {
            await dbContext.HOI_DONG_TUYEN_SINH.AddAsync(hoiDongTuyenSinh);
            await dbContext.SaveChangeAsync();

            return hoiDongTuyenSinh;
        }

        public async Task<int> DeleteHoiDongTuyenSinhAsync(int id)
        {
            var HoiDongTuyenSinh = await dbContext.HOI_DONG_TUYEN_SINH.FindAsync(id);

            if (HoiDongTuyenSinh != null)
            {
                HoiDongTuyenSinh.IS_DELETED = true;
                HoiDongTuyenSinh.DELETED_DATE = DateTime.Now;

                await dbContext.SaveChangeAsync();
            }

            return id;
        }

        public async Task<HOI_DONG_TUYEN_SINH> EditHoiDongTuyenSinhAsync(HOI_DONG_TUYEN_SINH hoiDongTuyenSinh)
        {
            var HoiDongTuyenSinh = await dbContext.HOI_DONG_TUYEN_SINH.FindAsync(hoiDongTuyenSinh.ID);

            if (HoiDongTuyenSinh != null)
            {
                HoiDongTuyenSinh.ID_CSGD = hoiDongTuyenSinh.ID_CSGD;
                HoiDongTuyenSinh.ID_NGUOI_DUNG= hoiDongTuyenSinh.ID_NGUOI_DUNG;
                HoiDongTuyenSinh.ID_VAI_TRO = hoiDongTuyenSinh.ID_VAI_TRO;
                HoiDongTuyenSinh.UPDATED_DATE = DateTime.Now;
                HoiDongTuyenSinh.NAM_TUYEN_SINH = hoiDongTuyenSinh.NAM_TUYEN_SINH;
                HoiDongTuyenSinh.IS_ACTIVE = hoiDongTuyenSinh.IS_ACTIVE;
                await dbContext.SaveChangeAsync();
            }

            return HoiDongTuyenSinh;
        }

        public async Task<HOI_DONG_TUYEN_SINH> GetHoiDongTuyenSinhByIdAsync(int id)
        {
            return await dbContext.HOI_DONG_TUYEN_SINH.Where(c => c.ID == id && !c.IS_DELETED && c.IS_ACTIVE)
                                           .FirstOrDefaultAsync();
        }
        public async Task<List<HOI_DONG_TUYEN_SINH>> GetListHDTSByIdCSGDAsync(int id_csgd)
        {
            var year = DateTime.Now.Year;
            return await dbContext.HOI_DONG_TUYEN_SINH
                .Include(x => x.VAI_TRO)
                .Include(x => x.NGUOI_DUNG)
                .Where(c => c.ID_CSGD == id_csgd && !c.IS_DELETED && c.IS_ACTIVE && c.NAM_TUYEN_SINH == year).ToListAsync();
        }
        public async Task<PagedList<HOI_DONG_TUYEN_SINH>> GetPagedHoiDongTuyenSinhAsync(string keyword, int pageNumber, int pageSize)
        {
            var pagedList = new PagedList<HOI_DONG_TUYEN_SINH>();
            var querry = dbContext.HOI_DONG_TUYEN_SINH
                .Include(x => x.VAI_TRO)
                .Include(x => x.NGUOI_DUNG)
                .Where(x => !x.IS_DELETED && x.IS_ACTIVE);
            var data = await querry
                .PageBy(x => x.ID, pageNumber, pageSize)
                .ToListAsync();

            pagedList.Data.AddRange(data);
            pagedList.PageSize = pageSize;
            pagedList.TotalCount = await querry.CountAsync();

            return pagedList;
        }
    }
}
