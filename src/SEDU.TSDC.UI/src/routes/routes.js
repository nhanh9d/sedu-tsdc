import LoginCallback from "../pages/login/index";
import Dashboard from "../pages/dashboard/index"
import MainLayout from "../layout/MainLayout";
import LoginLayout from "../layout/LoginLayout";
import Examination from "../pages/examination";
import Committee from "../pages/Committee";
import Management from "../pages/management";
import Statistics from "../pages/statistics";
import ModifyCommittee from "../pages/Committee/ModifyCommittee";
// const DashboardRoute = {
//   path: "/",
//   layout: MainLayout,
//   page: Dashboard,
// };

const LoginCallbackRoute = {
  path: "/login-callback",
  layout: LoginLayout,
  page: LoginCallback,
};

const ExaminationRoute = {
  path: "/",
  layout: MainLayout,
  page: Examination,
};

const CommitteeRoute = {
  path: "/hoi-dong-tuyen-sinh",
  layout: MainLayout,
  page: Committee,
};
const ModifyRoute = {
  path: "/hoi-dong-tuyen-sinh/sua-thong-tin-thanh-vien",
  layout: MainLayout,
  page: ModifyCommittee,
};

const ManagementRoute = {
  path: "/quan-ly-cac-dot-tuyen-sinh",
  layout: MainLayout,
  page: Management,
};

const StatisticsRoute = {
  path: "/bao-cao-thong-ke",
  layout: MainLayout,
  page: Statistics,
};


// export const routes = [DashboardRoute, LoginCallbackRoute, ExaminationRoute, CommitteeRoute, ManagementRoute, StatisticsRoute];
export const routes = [ModifyRoute,LoginCallbackRoute, ExaminationRoute, CommitteeRoute, ManagementRoute, StatisticsRoute];
