﻿using Microsoft.EntityFrameworkCore;
using SEDU.TSDC.EntityFramework.DbContexts.Interfaces;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.EntityFramework.Extensions.Extensions;
using SEDU.TSDC.EntityFramework.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Repositories
{
    public class BaoCaoRepository<TDbContext> : IBaoCaoRepository
        where TDbContext : ITsdcDbContext
    {
        protected readonly TDbContext dbContext;

        public BaoCaoRepository(TDbContext _dbContext)
        {
            dbContext = _dbContext;
        }

        public async Task<BAO_CAO> CreateBaoCaoAsync(BAO_CAO baoCao)
        {
            await dbContext.BAO_CAO.AddAsync(baoCao);
            await dbContext.SaveChangeAsync();

            return baoCao;
        }

        public async Task<int> DeleteBaoCaoAsync(int id)
        {
            var BaoCao = await dbContext.BAO_CAO.FindAsync(id);

            if (BaoCao != null)
            {
                BaoCao.IS_DELETED = true;
                BaoCao.DELETED_DATE = DateTime.Now;

                await dbContext.SaveChangeAsync();
            }

            return id;
        }

        public async Task<BAO_CAO> EditBaoCaoAsync(BAO_CAO baoCao)
        {
            var BaoCao = await dbContext.BAO_CAO.FindAsync(baoCao.ID);

            if (BaoCao != null)
            {
                BaoCao.BC_DE_XUAT = baoCao.BC_DE_XUAT;
                BaoCao.BC_KHO_KHAN = baoCao.BC_KHO_KHAN;
                BaoCao.BC_NOI_DUNG_KHAC = baoCao.BC_NOI_DUNG_KHAC;
                BaoCao.ID_CSGD = baoCao.ID_CSGD;
                BaoCao.DS_THANH_VIEN_HDTS = baoCao.DS_THANH_VIEN_HDTS;
                BaoCao.ID_CHU_TICH_HDTS = baoCao.ID_CHU_TICH_HDTS;
                BaoCao.TONG_BI_LOAI = baoCao.TONG_BI_LOAI;
                BaoCao.TONG_CHI_TIEU = baoCao.TONG_CHI_TIEU;
                BaoCao.TONG_TRUNG_TUYEN = baoCao.TONG_TRUNG_TUYEN;
                BaoCao.TONG_HO_SO = baoCao.TONG_HO_SO;
                BaoCao.UPDATED_DATE = DateTime.Now;
                BaoCao.IS_ACTIVE = baoCao.IS_ACTIVE;
                await dbContext.SaveChangeAsync();
            }

            return BaoCao;
        }

        public async Task<BAO_CAO> GetBaoCaoByIdAsync(int id)
        {
            return await dbContext.BAO_CAO.Where(c => c.ID == id && !c.IS_DELETED && c.IS_ACTIVE)
                                           .FirstOrDefaultAsync();
        }
        public async Task<PagedList<BAO_CAO>> GetPagedBaoCaoAsync(string keyword, int pageNumber, int pageSize)
        {
            var pagedList = new PagedList<BAO_CAO>();
            var querry = dbContext.BAO_CAO
                .Where(x => !x.IS_DELETED && x.IS_ACTIVE);
            var data = await querry
                .PageBy(x => x.ID, pageNumber, pageSize)
                .ToListAsync();

            pagedList.Data.AddRange(data);
            pagedList.PageSize = pageSize;
            pagedList.TotalCount = await querry.CountAsync();

            return pagedList;
        }
        public async Task<List<BAO_CAO>> GetListDotTuyenSinh()
        {
            var year = DateTime.Now.Year;
            return await dbContext.BAO_CAO
                .Where(c => !c.IS_DELETED && c.IS_ACTIVE).OrderByDescending(x => x.THOI_GIAN_BAT_DAU).ThenByDescending(x => x.THOI_GIAN_KET_THUC).ToListAsync();
        }
    }
}
