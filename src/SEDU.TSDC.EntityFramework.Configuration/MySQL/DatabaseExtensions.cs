﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SEDU.TSDC.EntityFramework.Configuration.Configuration;
using System.Reflection;

namespace SEDU.TSDC.EntityFramework.Configuration.MySQL
{
    public static class DatabaseExtensions
    {
        public static void RegisterMySQLDbContexts<TDbContext>(this IServiceCollection services,
            ConnectionStringsConfiguration connectionStrings,
            DatabaseMigrationsConfiguration databaseMigrations)
            where TDbContext : DbContext
        {
            var migrationsAssembly = typeof(DatabaseExtensions).GetTypeInfo().Assembly.GetName().Name;

            // Audit logging connection
            services.AddDbContext<TDbContext>(options => options.UseMySQL(connectionStrings.TsdcDbConnection,
                optionsSql =>
                {
                    optionsSql.MigrationsAssembly(databaseMigrations.TsdcDbMigrationsAssembly ?? migrationsAssembly);
                    optionsSql.UseQuerySplittingBehavior(QuerySplittingBehavior.SingleQuery);
                }));
        }
    }
}
