import React, { useEffect, useState } from 'react';
import { routes } from './routes/routes';
import { BrowserRouter, Switch, Redirect } from 'react-router-dom';
import './App.css';
import SeduRoutes from './routes/index';
// import { ThemeProvider } from '@material-ui/styles';
// import { createTheme } from '@material-ui/core/styles';
import axios from 'axios';

const App = (props) => {
	const [ticket, setTicket] = useState('');
	const IPv4 = localStorage.getItem("ip");
	/* 	const theme = createTheme({
		overrides: {
			MuiOutlinedInput: {
				multiline: {
					fontWeight: 'bold',
					fontSize: '20px',
					color: 'purple',
					width: '50vw',
				},
			},
		},
	}); */

	useEffect(() => {
		//call api get ticket
		if (IPv4 === null || IPv4 === undefined) {
			const getData = async () => {
				const res = await axios.post('https://geolocation-db.com/json/')
				console.log(res)
				if (res && res.data) {
					localStorage.setItem('ip', res.data.IPv4)
				}
			}
			getData();
		}
	});
	useEffect(() => {
		//call api get Hoi Dong Data
		
			const getCSGDData = async () => {
				const res = await axios.post('http://localhost:5000/CoSoGiaoDuc/get-co-so-giao-duc-by-id?coSoGiaoDucId=1')
				if (res && res.data) {
					localStorage.setItem('CSGD', res.data.tenCSGD)
				}
			}
			getCSGDData();
		
	});
	useEffect(() => {
		//call api get Hoi Dong Data
		
			const getUserData = async () => {
				const res = await axios.post('http://localhost:5000/NguoiDung/get-nguoi-dung-by-id?nguoiDungId=2')
				if (res && res.data) {
					localStorage.setItem('user', res.data.hoTen)
				}
			}
			getUserData();
		
	});

	return (
		// <ThemeProvider theme={theme}>
		<BrowserRouter>
			<Switch>
				{routes.map((route, index) => (
					<SeduRoutes
						key={index}
						path={route.path}
						page={route.page}
						layout={route.layout}
						ticket={ticket}
						setTicket={setTicket}
						exact
					/>
				))}
				<Redirect from="*" to="/404" />
			</Switch>
		</BrowserRouter>
		// </ThemeProvider>
	);
};

export default App;
