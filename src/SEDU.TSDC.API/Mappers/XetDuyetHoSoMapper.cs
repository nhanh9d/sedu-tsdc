﻿using AutoMapper;
using SEDU.TSDC.API.Parameters.XetDuyetHoSo;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.API.Mappers
{
    public static class XetDuyetHoSoMapper
    {
        internal static IMapper DtoMapper;

        static XetDuyetHoSoMapper()
        {
            DtoMapper = new MapperConfiguration(config => config.AddProfile<XetDuyetHoSoMapperProfile>()).CreateMapper();
        }

        public static XetDuyetHoSoDto ToDto(this CreateXetDuyetHoSoParams nguoiDung)
        {
            return DtoMapper.Map<XetDuyetHoSoDto>(nguoiDung);
        }

        public static XetDuyetHoSoDto ToDto(this EditXetDuyetHoSoParams nguoiDung)
        {
            return DtoMapper.Map<XetDuyetHoSoDto>(nguoiDung);
        }

        public static PagedList<XetDuyetHoSoDto> ToDto(this PagedList<NGUOI_DUNG> nguoiDung)
        {
            return DtoMapper.Map<PagedList<XetDuyetHoSoDto>>(nguoiDung);
        }
    }
}
