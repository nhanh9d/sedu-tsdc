﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Configuration.Configuration
{
    public class ConnectionStringsConfiguration
    {
        public string TsdcDbConnection { get; set; }
        public string SSODbConnection { get; set; }


        public void SetConnections(string tsdcConnectionString, string _SSODbConnection)
        {
            TsdcDbConnection = tsdcConnectionString;
            SSODbConnection = _SSODbConnection;
        }
    }
}
