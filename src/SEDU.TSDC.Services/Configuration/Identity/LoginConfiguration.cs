﻿using SEDU.TSDC.Services.Enum.Identity;

namespace SEDU.TSDC.Services.Configuration.Identity
{
    public class LoginConfiguration
    {
        public LoginResolutionPolicy ResolutionPolicy { get; set; } = LoginResolutionPolicy.Username;
    }
}
