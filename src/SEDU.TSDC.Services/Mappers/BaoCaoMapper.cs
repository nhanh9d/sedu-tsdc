﻿using AutoMapper;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System.Collections.Generic;

namespace SEDU.TSDC.Services.Mappers
{
    public static class BaoCaoMapper
    {
        internal static IMapper DtoMapper;
        internal static IMapper EntityMapper;

        static BaoCaoMapper()
        {
            DtoMapper = new MapperConfiguration(config => config.AddProfile<BaoCaoDtoMapperProfile>()).CreateMapper();
            EntityMapper = new MapperConfiguration(config => config.AddProfile<BaoCaoEntityMapperProfile>()).CreateMapper();
        }

        //public static List<Activity> ToModel(this List<APP_ACTIVITY> _APP_ACTIVITY)
        //{
        //    return Mapper.Map<List<Activity>>(_APP_ACTIVITY);
        //}

        //public static Activity ToModel(this APP_ACTIVITY _APP_ACTIVITY)
        //{
        //    return Mapper.Map<Activity>(_APP_ACTIVITY);
        //}

        public static BAO_CAO ToEntity(this BaoCaoDto baoCaoDto)
        {
            return EntityMapper.Map<BAO_CAO>(baoCaoDto);
        }

        public static BaoCaoDto ToDto(this BAO_CAO baoCao)
        {
            return DtoMapper.Map<BaoCaoDto>(baoCao);
        }

        public static PagedList<BaoCaoDto> ToDto(this PagedList<BAO_CAO> baoCao)
        {
            return DtoMapper.Map<PagedList<BaoCaoDto>>(baoCao);
        }
        public static List<BaoCaoDto> ToListDto(this List<BAO_CAO> baoCao)
        {
            return DtoMapper.Map<List<BaoCaoDto>>(baoCao);
        }
    }
}
