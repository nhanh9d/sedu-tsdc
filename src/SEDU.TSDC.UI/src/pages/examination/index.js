import React from "react";
import TabsUi from "../../components/TabUIs";

// import fakeData to fetch
import Admitted from "./Admitted";
import AllSubmitted from "./AllSubmitted";
import Approved from "./Approved";
import NotApproved from "./NotApproved";
import Rejected from "./Rejected";
import UpdateResults from "./UpdateResults";
// import axios from "axios";
// import { IP_API } from "../../helper/helper";
// import { options_ticket } from "./../../helper/helper";

// tab UI's names
const tabsNames = [
  {
    name: "Chưa xét duyệt",
    component: NotApproved,
  },
  {
    name: "Đã xét duyệt",
    component: Approved,
  },
  {
    name: "Đã trúng tuyển",
    component: Admitted,
  },
  {
    name: "Không trúng tuyển",
    component: Rejected,
  },
  {
    name: "Tất cả",
    component: AllSubmitted,
  },
  {
    name: "Trả kết quả",
    component: UpdateResults,
  },
];

const Examination = (props) => {
  // const [listData, setListData] = useState([]);

  // context

  // call api to get list of data (right now just use fake data)
  // useEffect(() => {
  //   // setListData(fakeData);
  //   const params = {
  //     trang_thai_nhap_hoc: true,
  //     trang_thai_duyet_ho_so: "4",
  //     nam_xet_tuyen: 2021,
  //     keyword: "",
  //     pageNumber: 1,
  //     pageSize: 100,
  //   };
  //   const fetchData = async () => {
  //     await axios
  //       .post(IP_API + "/HoSo/get-paged-ho-so", params, options_ticket)
  //       .then((reponse) => {
  //         // call API thanh cong
  //         // setListDataReal
  //         if (reponse && reponse.data) {
  //           setListData(reponse.data.data);
  //         }
  //       })
  //       .catch((error) => {});
  //   };
  //   fetchData();
  // }, []);

  return (
    <>
      {/* <FormModal/> */}
      <TabsUi
        setHomeTabName={props.setHomeTabName}
        // listData={listData}
        tabsNames={tabsNames}
      />
    </>
  );
};

export default Examination;
