﻿using AutoMapper;
using SEDU.TSDC.API.Parameters.VaiTro;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System;

namespace SEDU.TSDC.API.Mappers
{
    public class VaiTroMapperProfile : Profile
    {
        public VaiTroMapperProfile()
        {
            CreateMap<CreateVaiTroParams, VaiTroDto>(MemberList.Destination);
            CreateMap<EditVaiTroParams, VaiTroDto>(MemberList.Destination);
        }
    }
}
