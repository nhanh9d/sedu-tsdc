﻿using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.EntityFramework.Repositories.Interfaces;
using SEDU.TSDC.Services.Dtos.Tsdc;
using SEDU.TSDC.Services.Mappers;
using SEDU.TSDC.Services.Services.Interface;
using System;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Services
{
    public class VaiTroService : IVaiTroService
    {
        private readonly IVaiTroRepository vaiTroRepository;

        public VaiTroService(IVaiTroRepository _vaiTroRepository)
        {
            vaiTroRepository = _vaiTroRepository;
        }

        public async Task<VaiTroDto> AddVaiTroAsync(VaiTroDto vaiTro)
        {
            var vaiTroEntity = vaiTro.ToEntity();
            vaiTroEntity = await vaiTroRepository.CreateVaiTroAsync(vaiTroEntity);
            return vaiTroEntity.ToDto();
        }

        public async Task<int> DeleteVaiTroAsync(int vaiTroId)
        {
            return await vaiTroRepository.DeleteVaiTroAsync(vaiTroId);
        }

        public async Task<VaiTroDto> EditVaiTroAsync(VaiTroDto vaiTro)
        {
            var vaiTroEntity = vaiTro.ToEntity();
            vaiTroEntity = await vaiTroRepository.EditVaiTroAsync(vaiTroEntity);
            return vaiTroEntity.ToDto();
        }

        public async Task<VaiTroDto> GetVaiTroByIdAsync(int vaiTroId)
        {
            var vaiTroEntity = await vaiTroRepository.GetVaiTroByIdAsync(vaiTroId);

            if (vaiTroEntity == null)
            {
                return default;
            }

            return vaiTroEntity.ToDto();
        }
        public async Task<PagedList<VaiTroDto>> GetPagedVaiTroAsync(string keyword, int pageNumber, int pageSize)
        {
            var listEntity = await vaiTroRepository.GetPagedVaiTroAsync(keyword, pageNumber, pageSize);
            return listEntity.ToDto();
        }
    }
}
