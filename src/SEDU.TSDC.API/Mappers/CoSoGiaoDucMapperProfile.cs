﻿using AutoMapper;
using SEDU.TSDC.API.Parameters.CoSoGiaoDuc;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System;

namespace SEDU.TSDC.API.Mappers
{
    public class CoSoGiaoDucMapperProfile : Profile
    {
        public CoSoGiaoDucMapperProfile()
        {
            CreateMap<CreateCoSoGiaoDucParams, CoSoGiaoDucDto>(MemberList.Destination);
            CreateMap<EditCoSoGiaoDucParams, CoSoGiaoDucDto>(MemberList.Destination);
        }
    }
}
