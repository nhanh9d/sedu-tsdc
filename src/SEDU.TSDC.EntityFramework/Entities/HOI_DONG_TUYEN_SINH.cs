﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Entities
{
    public class HOI_DONG_TUYEN_SINH : BASE_ENTITY
    {
        //[MaxLength(100, ErrorMessage = "Tên vai trò tối đa 50 kí tự")]
        public int ID_NGUOI_DUNG { get; set; }
        public int ID_CSGD { get; set; }
        public int NAM_TUYEN_SINH { get; set; }
        public int ID_VAI_TRO { get; set; }

        public virtual NGUOI_DUNG NGUOI_DUNG { get; set; }
        public virtual CO_SO_GIAO_DUC CSGD { get; set; }
        public virtual VAI_TRO VAI_TRO { get; set; }
    }
}
