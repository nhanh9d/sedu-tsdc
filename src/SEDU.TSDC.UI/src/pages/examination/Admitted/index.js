import React, { useState, useEffect, } from 'react'
import DataTableCustom from '../../../components/DataTables/DataTableCustom'
import moment from 'moment'
import { IP_API, options_ticket } from "./../../../helper/helper";
import axios from "axios";
// import FetchDataContext from '../../../store/fetchData-context'

// import for Loading effect
import LoadingEffect from "../../../components/LoadingEffect";

const columns = [
  { id: 'STT', label: 'STT', width: 20, align: 'left' },
  { id: 'Mã hồ sơ', label: 'Mã hồ sơ', width: 130, align: 'left' },
  {
    id: 'Họ đệm',
    label: 'Họ đệm',
    width: 130,
    align: 'left',
  },
  {
    id: 'Tên',
    label: 'Tên',
    width: 130,
    align: 'left',
  },
  {
    id: 'Ngày sinh',
    label: 'Ngày sinh',
    width: 130,
    align: 'left',
  },
  // {
  //   id: "Trạng thái",
  //   label: "Trạng thái",
  //   width: 130,
  //   align: "left",
  // },
]



const Admitted = ({ listData }) => {
  // const [columns, setColumns] = useState([]);
  const [rows, setRows] = useState([]);
  const [loading, isLoading] = useState(false);
  const [listAdmitted, setListAdmitted] = useState([]);
  const [totalRecords, setTotalRecords] = useState(0);


  // context
  // const context = useContext(FetchDataContext)

  // function to call api
  const fetchAdmitted = (searchKey = "", pageNumber = 1, pageSize = 5) => {
    const fetchData = async () => {
      // param for calling api
      const params = {
        trang_thai_nhap_hoc: true,
        trang_thai_duyet_ho_so: "2",
        nam_xet_tuyen: 2021,
        keyword: searchKey,
        pageNumber: pageNumber,
        pageSize: pageSize,
      };
      isLoading(true);
      await axios
        .post(
          IP_API + "/XetDuyetHoSo/get-paged-loai-xet-duyet-ho-so",
          params,
          options_ticket
        )
        .then((response) => {
          // call API thanh cong
          // setListDataReal
          if (response && response.data) {
            // console.log(response.data.data);
            setTotalRecords(response.data.totalCount);
            setListAdmitted(response.data.data);
          }
        })
        .catch((error) => {
          if (error.response.status === 401) {
            window.location.href =
              "https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback";
          }
        });
      isLoading(false);
    };
    fetchData();
  };

  useEffect(() => {
    fetchAdmitted();
  }, [])

  // get rows
  useEffect(() => {
    let renderedItems
    if (listAdmitted) {
      renderedItems = listAdmitted.map((item) => {
        let { maHoSo, hoDem, ten, ngaySinh, trangThaiDuyetHoSo, id } = item.hoSo
        ngaySinh = moment(ngaySinh).format('DD/MM/YYYY')
        return { maHoSo, hoDem, ten, ngaySinh, trangThaiDuyetHoSo, id }
      })
      setRows(renderedItems)
    }
  }, [listAdmitted])

  return (
    <>
      <LoadingEffect loading={loading} />
      <DataTableCustom
        columns={columns}
        rows={rows}
        openTooltip={false}
        searchBar={true}
        status="2"
        handleAdmitAndReject={fetchAdmitted}
        handleSearch={fetchAdmitted}
        totalRecords={totalRecords}
      />
    </>
  );
}

export default Admitted
