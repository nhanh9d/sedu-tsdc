﻿using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Repositories.Interfaces
{
    public interface IHoiDongTuyenSinhRepository
    {
        Task<HOI_DONG_TUYEN_SINH> CreateHoiDongTuyenSinhAsync(HOI_DONG_TUYEN_SINH HoiDongTuyenSinh);
        Task<int> DeleteHoiDongTuyenSinhAsync(int id);
        Task<HOI_DONG_TUYEN_SINH> EditHoiDongTuyenSinhAsync(HOI_DONG_TUYEN_SINH HoiDongTuyenSinh);
        Task<HOI_DONG_TUYEN_SINH> GetHoiDongTuyenSinhByIdAsync(int id);
        Task<PagedList<HOI_DONG_TUYEN_SINH>> GetPagedHoiDongTuyenSinhAsync(string keyword, int pageNumber, int pageSize);
        Task<List<HOI_DONG_TUYEN_SINH>> GetListHDTSByIdCSGDAsync(int id_csgd);
    }
}
