﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Parameters.HoiDongTuyenSinh
{
    public class ThemNhieuHoiDongTuyenSinhParams
    {
        public string HoTen { get; set; }
        public string SoDienThoai { get; set; }
        public string ChucVu { get; set; }
        public int IdCoSoGiaoDuc { get; set; }
    }
}
