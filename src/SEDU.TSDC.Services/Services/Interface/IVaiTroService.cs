﻿using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Services.Interface
{
    public interface IVaiTroService
    {
        public Task<VaiTroDto> AddVaiTroAsync(VaiTroDto vaiTroDto);
        public Task<VaiTroDto> GetVaiTroByIdAsync(int vaiTroId);
        public Task<VaiTroDto> EditVaiTroAsync(VaiTroDto vaiTroDto);
        public Task<int> DeleteVaiTroAsync(int vaiTroId);
        Task<PagedList<VaiTroDto>> GetPagedVaiTroAsync(string keyword, int pageNumber, int pageSize);
    }
}
