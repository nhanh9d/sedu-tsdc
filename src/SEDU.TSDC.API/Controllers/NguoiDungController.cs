﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SEDU.TSDC.API.Mappers;
using SEDU.TSDC.API.Parameters;
using SEDU.TSDC.API.Parameters.NguoiDung;
using SEDU.TSDC.Services.Dtos.Tsdc;
using SEDU.TSDC.Services.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors("SeduCors")]
    public class NguoiDungController : ControllerBase
    {

        private readonly ILogger<NguoiDungController> _logger;
        private readonly INguoiDungService _nguoiDungService;

        public NguoiDungController(ILogger<NguoiDungController> logger, INguoiDungService nguoiDungService)
        {
            _logger = logger;
            _nguoiDungService = nguoiDungService;
        }

        [Route("healthcheck")]
        [HttpGet]
        public string HealthCheck()
        {
            return "Ok";
        }

        [Route("add-nguoi-dung")]
        [HttpPost]
        public async Task<NguoiDungDto> AddNguoiDung(CreateNguoiDungParams pr)
        {
            var dto = pr.ToDto();
            return await _nguoiDungService.AddNguoiDungAsync(dto);
        }

        [Route("get-nguoi-dung-by-id")]
        [HttpPost]
        public async Task<object> GetNguoiDungById(int nguoiDungId)
        {
            try
            {
                return await _nguoiDungService.GetNguoiDungByIdAsync(nguoiDungId);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [Route("edit-nguoi-dung")]
        [HttpPost]
        public async Task<NguoiDungDto> EditNguoiDung(EditNguoiDungParams pr)
        {
            var dto = pr.ToDto();
            return await _nguoiDungService.EditNguoiDungAsync(dto);
        }

        [Route("delete-nguoi-dung")]
        [HttpPost]
        public async Task<int> DeleteNguoiDung(int nguoiDungId)
        {
            return await _nguoiDungService.DeleteNguoiDungAsync(nguoiDungId);
        }
        [Route("get-paged-nguoi-dung")]
        [HttpPost]
        public async Task<object> GetPagedNguoiDung(PagedParams pr)
        {
            var lst = await _nguoiDungService.GetPagedNguoiDungAsync(pr.keyword, pr.pageNumber, pr.pageSize);
            return lst;
        }
    }
}
