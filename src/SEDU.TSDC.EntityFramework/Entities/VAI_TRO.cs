﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Entities
{
    public class VAI_TRO : BASE_ENTITY
    {
        [MaxLength(100, ErrorMessage = "Tên vai trò tối đa 50 kí tự")]
        public string TEN_VAI_TRO { get; set; }
        #region Relation props
        /// <summary>
        /// Vai trò trong hội đồng tuyển sinh
        /// </summary>
        public virtual ICollection<HOI_DONG_TUYEN_SINH> VAI_TRO_TRONG_HDTS { get; set; }
        #endregion
    }
}
