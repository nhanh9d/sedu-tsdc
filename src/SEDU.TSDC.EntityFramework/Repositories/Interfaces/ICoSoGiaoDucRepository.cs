﻿using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Repositories.Interfaces
{
    public interface ICoSoGiaoDucRepository
    {
        Task<CO_SO_GIAO_DUC> CreateCoSoGiaoDucAsync(CO_SO_GIAO_DUC client);
        Task<int> DeleteCoSoGiaoDucAsync(int id);
        Task<CO_SO_GIAO_DUC> EditCoSoGiaoDucAsync(CO_SO_GIAO_DUC client);
        Task<CO_SO_GIAO_DUC> GetCoSoGiaoDucByIdAsync(int id);
        Task<PagedList<CO_SO_GIAO_DUC>> GetPagedCoSoGiaoDucAsync(string keyword, int pageNumber, int pageSize);
    }
}
