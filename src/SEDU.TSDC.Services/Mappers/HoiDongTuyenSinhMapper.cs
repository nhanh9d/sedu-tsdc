﻿using AutoMapper;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System.Collections.Generic;

namespace SEDU.TSDC.Services.Mappers
{
    public static class HoiDongTuyenSinhMapper
    {
        internal static IMapper DtoMapper;
        internal static IMapper EntityMapper;

        static HoiDongTuyenSinhMapper()
        {
            DtoMapper = new MapperConfiguration(config => config.AddProfile<HoiDongTuyenSinhDtoMapperProfile>()).CreateMapper();
            EntityMapper = new MapperConfiguration(config => config.AddProfile<HoiDongTuyenSinhEntityMapperProfile>()).CreateMapper();
        }

        //public static List<Activity> ToModel(this List<APP_ACTIVITY> _APP_ACTIVITY)
        //{
        //    return Mapper.Map<List<Activity>>(_APP_ACTIVITY);
        //}

        //public static Activity ToModel(this APP_ACTIVITY _APP_ACTIVITY)
        //{
        //    return Mapper.Map<Activity>(_APP_ACTIVITY);
        //}

        public static HOI_DONG_TUYEN_SINH ToEntity(this HoiDongTuyenSinhDto hoiDongTuyenSinhDto)
        {
            return EntityMapper.Map<HOI_DONG_TUYEN_SINH>(hoiDongTuyenSinhDto);
        }

        public static HoiDongTuyenSinhDto ToDto(this HOI_DONG_TUYEN_SINH hoiDongTuyenSinh)
        {
            return DtoMapper.Map<HoiDongTuyenSinhDto>(hoiDongTuyenSinh);
        }

        public static PagedList<HoiDongTuyenSinhDto> ToDto(this PagedList<HOI_DONG_TUYEN_SINH> hoiDongTuyenSinh)
        {
            return DtoMapper.Map<PagedList<HoiDongTuyenSinhDto>>(hoiDongTuyenSinh);
        }
        public static List<HoiDongTuyenSinhDto> ToListDto(this List<HOI_DONG_TUYEN_SINH> hoiDongTuyenSinh)
        {
            return DtoMapper.Map<List<HoiDongTuyenSinhDto>>(hoiDongTuyenSinh);
        }
    }
}
