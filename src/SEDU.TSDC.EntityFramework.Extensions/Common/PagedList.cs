﻿using System.Collections.Generic;

namespace SEDU.TSDC.EntityFramework.Extensions.Common
{
    public class PagedList<T> where T : class 
    {
        public PagedList()
        {
            Data = new List<T>();
        }

        public List<T> Data { get; }

        public string CategoryTitle { get; set; }

        public int TotalCount { get; set; }

        public int PageSize { get; set; }
    }
}
