﻿using AutoMapper;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.Services.Mappers
{
    public class HoiDongTuyenSinhDtoMapperProfile : Profile
    {
        public HoiDongTuyenSinhDtoMapperProfile()
        {
            SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
            DestinationMemberNamingConvention = new PascalCaseNamingConvention();
            CreateMap<HOI_DONG_TUYEN_SINH, HoiDongTuyenSinhDto>(MemberList.Destination)
                .ForMember(dto => dto.Id, opt => opt.MapFrom(e => e.ID))
                .ForMember(dto => dto.IdCoSoGiaoDuc, opt => opt.MapFrom(e => e.ID_CSGD))
                .ForMember(dto => dto.VaiTro, opt => opt.MapFrom(e => e.VAI_TRO == null ? string.Empty : e.VAI_TRO.TEN_VAI_TRO))
                .ForMember(dto => dto.TenNguoiDung, opt => opt.MapFrom(e => e.NGUOI_DUNG == null ? string.Empty : e.NGUOI_DUNG.HO_TEN))
                .ForMember(dto => dto.SoDienThoai, opt => opt.MapFrom(e => e.NGUOI_DUNG == null ? string.Empty : e.NGUOI_DUNG.SO_DT));
            CreateMap<PagedList<HOI_DONG_TUYEN_SINH>, PagedList<HoiDongTuyenSinhDto>>(MemberList.Destination).ReverseMap();
        }
    }
    public class HoiDongTuyenSinhEntityMapperProfile : Profile
    {
        public HoiDongTuyenSinhEntityMapperProfile()
        {
            SourceMemberNamingConvention = new PascalCaseNamingConvention();
            DestinationMemberNamingConvention = new LowerUnderscoreNamingConvention();
            CreateMap<HoiDongTuyenSinhDto, HOI_DONG_TUYEN_SINH>(MemberList.Destination)
            .ForMember(e => e.ID, opt => opt.MapFrom(dto => dto.Id))
            .ForMember(e => e.ID_CSGD, opt => opt.MapFrom(dto => dto.IdCoSoGiaoDuc));
        }
    }
}
