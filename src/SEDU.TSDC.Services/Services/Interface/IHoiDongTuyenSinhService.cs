﻿using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Services.Interface
{
    public interface IHoiDongTuyenSinhService
    {
        public Task<HoiDongTuyenSinhDto> AddHoiDongTuyenSinhAsync(HoiDongTuyenSinhDto baoCaoDto);
        public Task<HoiDongTuyenSinhDto> GetHoiDongTuyenSinhByIdAsync(int baoCaoId);
        public Task<HoiDongTuyenSinhDto> EditHoiDongTuyenSinhAsync(HoiDongTuyenSinhDto baoCaoDto);
        public Task<int> DeleteHoiDongTuyenSinhAsync(int baoCaoId);
        public Task<PagedList<HoiDongTuyenSinhDto>> GetPagedHoiDongTuyenSinhAsync(string keyword, int pageNumber, int pageSize);
        public Task<List<HoiDongTuyenSinhDto>> GetListHDTSByIdCSGD(int IdCSGD);
        public Task<HoiDongTuyenSinhDto> EditHDTSWithUserAsync(HoiDongTuyenSinhDto hoiDongTuyenSinh);
        Task<List<object>> ThemNhieuHoiDongTuyenSinh(List<HoiDongTuyenSinhDto> lstHdts);
    }
}
