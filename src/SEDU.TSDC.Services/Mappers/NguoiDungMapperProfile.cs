﻿using AutoMapper;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System;

namespace SEDU.TSDC.Services.Mappers
{
    public class NguoiDungDtoMapperProfile : Profile
    {
        public NguoiDungDtoMapperProfile()
        {
            SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
            DestinationMemberNamingConvention = new PascalCaseNamingConvention();
            CreateMap<NGUOI_DUNG, NguoiDungDto>(MemberList.Destination);
            CreateMap<PagedList<NGUOI_DUNG>, PagedList<NguoiDungDto>>(MemberList.Destination).ReverseMap();
        }
    }
    public class NguoiDungEntityMapperProfile : Profile
    {
        public NguoiDungEntityMapperProfile()
        {
            SourceMemberNamingConvention = new PascalCaseNamingConvention();
            DestinationMemberNamingConvention = new LowerUnderscoreNamingConvention();
            CreateMap<NguoiDungDto, NGUOI_DUNG>(MemberList.Destination);
        }
    }
}
