﻿using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using System;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Repositories.Interfaces
{
    public interface IXetDuyetHoSoRepository
    {
        Task<XET_DUYET_HO_SO> CreateXetDuyetHoSoAsync(XET_DUYET_HO_SO category);
        Task<int> DeleteXetDuyetHoSoAsync(int id);
        Task<XET_DUYET_HO_SO> EditXetDuyetHoSoAsync(XET_DUYET_HO_SO category);
        Task<XET_DUYET_HO_SO> GetXetDuyetHoSoByIdAsync(int id);
        Task<PagedList<XET_DUYET_HO_SO>> GetPagedXetDuyetHoSoAsync(string keyword, int pageNumber, int pageSize);
        Task<PagedList<XET_DUYET_HO_SO>> GetPagedXetDuyetHoSoAsync(int trang_thai_duyet_ho_so, bool trang_thai_nhap_hoc, int nam_xet_tuyen, string keyword, int pageNumber, int pageSize);
    }
}
