﻿using AutoMapper;
using SEDU.TSDC.API.Parameters.NguoiDung;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.API.Mappers
{
    public static class NguoiDungMapper
    {
        internal static IMapper DtoMapper;

        static NguoiDungMapper()
        {
            DtoMapper = new MapperConfiguration(config => config.AddProfile<NguoiDungMapperProfile>()).CreateMapper();
        }

        public static NguoiDungDto ToDto(this CreateNguoiDungParams nguoiDung)
        {
            return DtoMapper.Map<NguoiDungDto>(nguoiDung);
        }

        public static NguoiDungDto ToDto(this EditNguoiDungParams nguoiDung)
        {
            return DtoMapper.Map<NguoiDungDto>(nguoiDung);
        }

        public static PagedList<NguoiDungDto> ToDto(this PagedList<NGUOI_DUNG> nguoiDung)
        {
            return DtoMapper.Map<PagedList<NguoiDungDto>>(nguoiDung);
        }
    }
}
