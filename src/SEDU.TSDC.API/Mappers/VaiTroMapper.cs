﻿using AutoMapper;
using SEDU.TSDC.API.Parameters.VaiTro;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.API.Mappers
{
    public static class VaiTroMapper
    {
        internal static IMapper DtoMapper;

        static VaiTroMapper()
        {
            DtoMapper = new MapperConfiguration(config => config.AddProfile<VaiTroMapperProfile>()).CreateMapper();
        }

        public static VaiTroDto ToDto(this CreateVaiTroParams nguoiDung)
        {
            return DtoMapper.Map<VaiTroDto>(nguoiDung);
        }

        public static VaiTroDto ToDto(this EditVaiTroParams nguoiDung)
        {
            return DtoMapper.Map<VaiTroDto>(nguoiDung);
        }

        public static PagedList<VaiTroDto> ToDto(this PagedList<NGUOI_DUNG> nguoiDung)
        {
            return DtoMapper.Map<PagedList<VaiTroDto>>(nguoiDung);
        }
    }
}
