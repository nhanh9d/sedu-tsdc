import React, { useState, useEffect, useContext } from "react";
import { Form, Formik, Field } from "formik";
import {
  Button,
  CircularProgress,
  Grid,
  TextField,
  Card,
  CardContent,
  Box,
  Typography,
  RadioGroup,
  FormControlLabel,
  Radio,
} from "@material-ui/core";
import DataContext from "../../store/PassData-context";
import axios from "axios";
import * as Yup from "yup";
import { useHistory } from "react-router-dom";
// Noti
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "../../components/Alert/AlertCustom";
import { IP_API, options_ticket } from "../../helper/helper";
const ModifyCommittee = (props) => {
  const context = useContext(DataContext);
  console.log(context)
  const [fullNameEntered, setFullNameEntered] = useState(
    context.item.tenNguoiDung
  );
  const [phoneNumberEntered, setPhoneNumberEntered] = useState(
    context.item.soDienThoai
  );
  const [roleSelected, setRoleSelected] = useState(
    context.item.idVaiTro.toString()
  );
  const phoneRegExp =
    /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
  const validationSchema = Yup.object().shape({
    fullName: Yup.string()
      .required("Cần Điền Đầy Đủ Họ Và Tên")
      .min(3, "Tên Phải từ 3 kí tự trở lên"),
    phoneNumber: Yup.string()
      .required("Vui Lòng Điền Số Điện Thoại")
      .matches(phoneRegExp, "Số Điện Thoại Không Hợp Lệ Từ (0-9)"),
    role: Yup.string().required("Vui Lòng Chọn Chức Vụ"),
  });
  //List vai tro
  const [listRole, setListRole] = useState([]);
  // const [isSuccess, setIsSuccess] = useState(null);
  const [loading, isLoading] = useState(false);
  //Notify states
  const [notifySetting, setNotifySetting] = useState({
    isSuccess: true,
    message: "",
  });

  //call API listRole
  useEffect(() => {
    const fetchRoleData = async () => {
      const params = {
        keyword: "",
        pageNumber: 1,
        pageSize: 25,
      };
      //axios
      axios
        .post(IP_API + "/VaiTro/get-paged-vai-tro", params, options_ticket)
        .then((res) => {
          setListRole(res.data.data);
        })
        .catch((error) => {
          if (error.response.status === 401) {
            window.location.href =
              "https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback";
          }
        });
    };
    fetchRoleData();
  }, []);
  //
  const history = useHistory();
  // const { enqueueSnackbar } = useSnackbar();

  // handleSubmit when click submit form
  const onCancle = () => {
    history.push("/hoi-dong-tuyen-sinh");
  };
  const handleSubmitForm = (data) => {
    console.log(data.role)
    const params = {
      idNguoiDung: context.item.idNguoiDung,
      idCoSoGiaoDuc: 1,
      namTuyenSinh: 2021,
      idVaiTro: parseInt(data.role),
      tenNguoiDung: data.fullName,
      soDienThoai: data.phoneNumber,
      id: +context.item.id,
    };
    axios
      .post(IP_API + "/HoiDongTuyenSinh/edit-hoi-dong-tuyen-sinh-va-nguoi-dung", params)
      .then((response) => {
        // Call Api thanh cong
        isLoading(true);
        setNotifySetting({
          isSuccess: true,
          message: "Thêm mới thành công",
        });
      })
      .catch((error) => {
        isLoading(false);
        // Call Api that bai -> error Serve
        setNotifySetting({
          isSuccess: false,
          message: "Thêm mới thất bại",
        });
      });
  };
  const promise1 = async (values) => {
    let promise = new Promise((res) => {
      setTimeout(() => {
        res(handleSubmitForm(values));
      }, 1000);
    });
    return await promise;
  };
  // const promise2 = async () => {
  //   await     enqueueSnackbar('This is a success message!', "variant" );
  //   ;
  // }
  const handleSubmit = async (values) => {
    await promise1(values);
    setTimeout(() => {
      history.push("/hoi-dong-tuyen-sinh");
    }, 2000);
  };
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      isLoading(false);
      return;
    }
    isLoading(false);
  };
  return (
    <>
      <Box m={4}>
        <Card>
          <CardContent>
            <Formik
              //gia tri khoi tao
              initialValues={{
                fullName: fullNameEntered,
                phoneNumber: phoneNumberEntered,
                role: roleSelected,
              }}
              //validation
              validationSchema={validationSchema}
              //khi submit
              onSubmit={handleSubmit}
            >
              {/* render */}
              {({
                values,
                errors,
                isSubmitting,
                touched,
                setFieldValue,
                handleChange,
              }) => (
                <Form>
                  <Grid container direction="column" spacing={3}>
                    <Grid item>
                      <Field
                        name="fullName"
                        value={values.fullNameEntered}
                        render={({ field }) => (
                          <TextField
                            onInput={(e) => setFieldValue("fullName", e.currentTarget.value)}
                            required
                            style={{ width: 500 }}
                            label="Họ và tên"
                            autoFocus
                            // bốc tách toàn bộ props từ field???
                            {...field}
                          />
                        )}
                      />
                      <Grid item xs={12} sm={12}>
                        <Typography variant="inherit" color="error">
                          {errors.fullName}
                        </Typography>
                      </Grid>
                    </Grid>
                    <Grid item>
                      <Field name="phoneNumber" value={values.phoneNumberEntered}>
                        {({ field }) => (
                          <TextField
                            required
                            onInput={(e) =>
                              setFieldValue("phoneNumber", e.currentTarget.value)
                            }
                            style={{ width: 500 }}
                            label="Số Điện Thoại"
                            autoFocus
                            // bốc tách toàn bộ props từ field???
                            {...field}
                          />
                        )}
                      </Field>
                      <Grid item xs={12} sm={12}>
                        <Typography variant="inherit" color="error">
                          {errors.phoneNumber}
                        </Typography>
                      </Grid>
                    </Grid>
                    <Grid item>
                      <Field name="role">
                        {({ field }) => (
                          <>
                            <RadioGroup
                              {...field}
                              value={values.role}
                              onChange={(e) => {
                                console.log(e.currentTarget.value)
                                setFieldValue("role", e.currentTarget.value);
                              }}
                            >
                              {listRole.length > 0
                                ? listRole.map((item, index) => {
                                  return (
                                    <FormControlLabel
                                      key={index}
                                      {...field}
                                      value={item.id.toString()}
                                      control={<Radio />}
                                      label={item.tenVaiTro}
                                    />
                                  );
                                })
                                : ""}
                            </RadioGroup>
                            <Grid item xs={12} sm={12}>
                              <Typography variant="inherit" color="error">
                                {errors.role}
                              </Typography>
                            </Grid>
                          </>
                        )}
                      </Field>
                    </Grid>
                    <Grid container spacing={3}>
                      <Grid item>
                        <Box ml={1}>
                          <Button
                            disabled={isSubmitting}
                            startIcon={
                              isSubmitting ? (
                                <CircularProgress size="1rem" />
                              ) : undefined
                            }
                            type="submit"
                            color="primary"
                            variant="contained"
                          >
                            {isSubmitting ? "Submiting..." : "Submit"}
                          </Button>
                        </Box>
                      </Grid>

                      <Grid item>
                        <Button
                          onClick={onCancle}
                          type="button"
                          color="secondary"
                          variant="contained"
                        >
                          Hủy Bỏ
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </Form>
              )}
            </Formik>
          </CardContent>
        </Card>
        <Snackbar open={loading} autoHideDuration={1000} onClose={handleClose}>
          <Alert
            onClose={handleClose}
            severity={notifySetting.isSuccess ? "success" : "error"}
          >
            {notifySetting.message}
          </Alert>
        </Snackbar>
      </Box>
    </>
  );
};

export default ModifyCommittee;
