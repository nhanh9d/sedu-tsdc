﻿using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Repositories.Interfaces
{
    public interface IBaoCaoRepository
    {
        Task<BAO_CAO> CreateBaoCaoAsync(BAO_CAO client);
        Task<int> DeleteBaoCaoAsync(int id);
        Task<BAO_CAO> EditBaoCaoAsync(BAO_CAO client);
        Task<BAO_CAO> GetBaoCaoByIdAsync(int id);
        Task<PagedList<BAO_CAO>> GetPagedBaoCaoAsync(string keyword, int pageNumber, int pageSize);
        Task<List<BAO_CAO>> GetListDotTuyenSinh();
    }
}
