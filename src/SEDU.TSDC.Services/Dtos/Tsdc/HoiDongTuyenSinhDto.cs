﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Dtos.Tsdc
{
    public class HoiDongTuyenSinhDto : BaseDto
    {
        public HoiDongTuyenSinhDto() : base()
        {
        }
        public int IdNguoiDung { get; set; }
        public int IdCoSoGiaoDuc { get; set; }
        public int NamTuyenSinh { get; set; }
        public int IdVaiTro { get; set; }
        public string VaiTro_QuickAdd { get; set; }
        public string VaiTro { get; set; }
        public string TenNguoiDung { get; set; }
        public string SoDienThoai { get; set; }
    }
}
