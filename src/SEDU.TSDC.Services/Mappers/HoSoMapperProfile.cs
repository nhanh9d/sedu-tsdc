﻿using AutoMapper;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.Services.Mappers
{
    public class HoSoDtoMapperProfile : Profile
    {
        public HoSoDtoMapperProfile()
        {
            SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
            DestinationMemberNamingConvention = new PascalCaseNamingConvention();
            CreateMap<HO_SO, HoSoDto>(MemberList.Destination)
                .ForMember(dto => dto.HoDem, opt => opt.MapFrom(e => e.HO_DEM));
            CreateMap<PagedList<HO_SO>, PagedList<HoSoDto>>(MemberList.Destination).ReverseMap();
        }
    }
    public class HoSoEntityMapperProfile : Profile
    {
        public HoSoEntityMapperProfile()
        {
            SourceMemberNamingConvention = new PascalCaseNamingConvention();
            DestinationMemberNamingConvention = new LowerUnderscoreNamingConvention();
            CreateMap<HoSoDto, HO_SO>(MemberList.Destination)
                .ForMember(e => e.HO_DEM, opt => opt.MapFrom(dto => dto.HoDem));
        }
    }
}
