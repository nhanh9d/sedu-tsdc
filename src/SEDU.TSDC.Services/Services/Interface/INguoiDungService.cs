﻿using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Services.Interface
{
    public interface INguoiDungService
    {
        public Task<NguoiDungDto> AddNguoiDungAsync(NguoiDungDto nguoiDungDto);
        public Task<NguoiDungDto> GetNguoiDungByIdAsync(int nguoiDungId);
        public Task<NguoiDungDto> EditNguoiDungAsync(NguoiDungDto nguoiDungDto);
        public Task<int> DeleteNguoiDungAsync(int nguoiDungId);
        Task<PagedList<NguoiDungDto>> GetPagedNguoiDungAsync(string keyword, int pageNumber, int pageSize);
    }
}
