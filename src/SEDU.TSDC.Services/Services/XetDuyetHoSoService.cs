﻿using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.EntityFramework.Repositories.Interfaces;
using SEDU.TSDC.Services.Dtos.Tsdc;
using SEDU.TSDC.Services.Mappers;
using SEDU.TSDC.Services.Services.Interface;
using System;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Services
{
    public class XetDuyetHoSoService : IXetDuyetHoSoService
    {
        private readonly IXetDuyetHoSoRepository xetDuyetHoSoRepository;
        private readonly IHoSoRepository hoSoRepository;

        public XetDuyetHoSoService(IXetDuyetHoSoRepository _xetDuyetHoSoRepository, IHoSoRepository _hoSoRepository)
        {
            xetDuyetHoSoRepository = _xetDuyetHoSoRepository;
            hoSoRepository = _hoSoRepository;
        }

        public async Task<XetDuyetHoSoDto> AddXetDuyetHoSoAsync(XetDuyetHoSoDto xetDuyetHoSo)
        {
            var xetDuyetHoSoEntity = xetDuyetHoSo.ToEntity();
            xetDuyetHoSoEntity = await xetDuyetHoSoRepository.CreateXetDuyetHoSoAsync(xetDuyetHoSoEntity);
            //Get Ho So By Id
            var HoSoEntity = await hoSoRepository.GetHoSoByIdAsync(xetDuyetHoSo.IdHoSo);
            HoSoEntity.TRANG_THAI_DUYET_HO_SO = xetDuyetHoSoEntity.KET_QUA;
            HoSoEntity.UPDATED_DATE = DateTime.Now;
            HoSoEntity.THOI_DIEM_CAP_NHAT_CUOI = DateTime.Now;
            if (HoSoEntity != null)
            {
                var resultEdit = await hoSoRepository.EditHoSoAsync(HoSoEntity);
            }
            return xetDuyetHoSoEntity.ToDto();
        }

        public async Task<int> DeleteXetDuyetHoSoAsync(int xetDuyetHoSoId)
        {
            return await xetDuyetHoSoRepository.DeleteXetDuyetHoSoAsync(xetDuyetHoSoId);
        }

        public async Task<XetDuyetHoSoDto> EditXetDuyetHoSoAsync(XetDuyetHoSoDto xetDuyetHoSo)
        {
            var xetDuyetHoSoEntity = xetDuyetHoSo.ToEntity();
            xetDuyetHoSoEntity = await xetDuyetHoSoRepository.EditXetDuyetHoSoAsync(xetDuyetHoSoEntity);
            return xetDuyetHoSoEntity.ToDto();
        }

        public async Task<XetDuyetHoSoDto> GetXetDuyetHoSoByIdAsync(int xetDuyetHoSoId)
        {
            var xetDuyetHoSoEntity = await xetDuyetHoSoRepository.GetXetDuyetHoSoByIdAsync(xetDuyetHoSoId);

            if (xetDuyetHoSoEntity == null)
            {
                return default;
            }

            return xetDuyetHoSoEntity.ToDto();
        }
        public async Task<PagedList<XetDuyetHoSoDto>> GetPagedXetDuyetHoSoAsync(string keyword, int pageNumber, int pageSize)
        {
            var listEntity = await xetDuyetHoSoRepository.GetPagedXetDuyetHoSoAsync(keyword, pageNumber, pageSize);
            return listEntity.ToDto();
        }
        public async Task<PagedList<XetDuyetHoSoDto>> GetPagedXetDuyetHoSoAsync(string trang_thai_duyet_ho_so, bool trang_thai_nhap_hoc, int nam_xet_tuyen, string keyword, int pageNumber, int pageSize)
        {
            var listEntity = await xetDuyetHoSoRepository.GetPagedXetDuyetHoSoAsync(Convert.ToInt32(trang_thai_duyet_ho_so), trang_thai_nhap_hoc, nam_xet_tuyen, keyword.Trim().ToUpper(), pageNumber, pageSize);
            return listEntity.ToDto();
        }
    }
}
