﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SEDU.TSDC.EntityFramework.PostgreSql.Migrations
{
    public partial class ChinhSuaDuLieu : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "TONG_CHI_TIEU",
                table: "BAO_CAO",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ID_CHU_TICH_HDTS",
                table: "BAO_CAO",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "TONG_CHI_TIEU",
                table: "BAO_CAO",
                type: "text",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<string>(
                name: "ID_CHU_TICH_HDTS",
                table: "BAO_CAO",
                type: "text",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");
        }
    }
}
