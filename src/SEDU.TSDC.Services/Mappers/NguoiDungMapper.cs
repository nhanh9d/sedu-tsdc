﻿using AutoMapper;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.Services.Mappers
{
    public static class NguoiDungMapper
    {
        internal static IMapper DtoMapper;
        internal static IMapper EntityMapper;

        static NguoiDungMapper()
        {
            DtoMapper = new MapperConfiguration(config => config.AddProfile<NguoiDungDtoMapperProfile>()).CreateMapper();
            EntityMapper = new MapperConfiguration(config => config.AddProfile<NguoiDungEntityMapperProfile>()).CreateMapper();
        }

        //public static List<Activity> ToModel(this List<APP_ACTIVITY> _APP_ACTIVITY)
        //{
        //    return Mapper.Map<List<Activity>>(_APP_ACTIVITY);
        //}

        //public static Activity ToModel(this APP_ACTIVITY _APP_ACTIVITY)
        //{
        //    return Mapper.Map<Activity>(_APP_ACTIVITY);
        //}

        public static NGUOI_DUNG ToEntity(this NguoiDungDto nguoiDungDto)
        {
            return EntityMapper.Map<NGUOI_DUNG>(nguoiDungDto);
        }

        public static NguoiDungDto ToDto(this NGUOI_DUNG nguoiDung)
        {
            return DtoMapper.Map<NguoiDungDto>(nguoiDung);
        }

        public static PagedList<NguoiDungDto> ToDto(this PagedList<NGUOI_DUNG> nguoiDung)
        {
            return DtoMapper.Map<PagedList<NguoiDungDto>>(nguoiDung);
        }
    }
}
