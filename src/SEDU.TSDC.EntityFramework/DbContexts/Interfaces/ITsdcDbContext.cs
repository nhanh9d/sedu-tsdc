﻿using Microsoft.EntityFrameworkCore;
using SEDU.TSDC.EntityFramework.Entities;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.DbContexts.Interfaces
{
    public interface ITsdcDbContext
    {
        DbSet<CO_SO_GIAO_DUC> CO_SO_GIAO_DUC { get; set; }
        DbSet<BAO_CAO> BAO_CAO { get; set; }
        DbSet<NGUOI_DUNG> NGUOI_DUNG { get; set; }
        DbSet<VAI_TRO> VAI_TRO { get; set; }
        DbSet<HOI_DONG_TUYEN_SINH> HOI_DONG_TUYEN_SINH { get; set; }
        DbSet<HO_SO> HO_SO { get; set; }
        DbSet<XET_DUYET_HO_SO> XET_DUYET_HO_SO { get; set; }
        Task<int> SaveChangeAsync();
    }
}
