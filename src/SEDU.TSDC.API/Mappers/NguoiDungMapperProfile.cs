﻿using AutoMapper;
using SEDU.TSDC.API.Parameters.NguoiDung;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System;

namespace SEDU.TSDC.API.Mappers
{
    public class NguoiDungMapperProfile : Profile
    {
        public NguoiDungMapperProfile()
        {
            CreateMap<CreateNguoiDungParams, NguoiDungDto>(MemberList.Destination);
            CreateMap<EditNguoiDungParams, NguoiDungDto>(MemberList.Destination);
        }
    }
}
