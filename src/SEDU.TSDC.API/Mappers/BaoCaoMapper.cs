﻿using AutoMapper;
using SEDU.TSDC.API.Parameters.BaoCao;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.API.Mappers
{
    public static class BaoCaoMapper
    {
        internal static IMapper DtoMapper;

        static BaoCaoMapper()
        {
            DtoMapper = new MapperConfiguration(config => config.AddProfile<BaoCaoMapperProfile>()).CreateMapper();
        }

        public static BaoCaoDto ToDto(this CreateBaoCaoParams baoCao)
        {
            return DtoMapper.Map<BaoCaoDto>(baoCao);
        }

        public static BaoCaoDto ToDto(this EditBaoCaoParams baoCao)
        {
            return DtoMapper.Map<BaoCaoDto>(baoCao);
        }

        public static PagedList<BaoCaoDto> ToDto(this PagedList<NGUOI_DUNG> baoCao)
        {
            return DtoMapper.Map<PagedList<BaoCaoDto>>(baoCao);
        }
    }
}
