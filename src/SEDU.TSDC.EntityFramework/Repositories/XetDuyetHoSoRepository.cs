﻿using Microsoft.EntityFrameworkCore;
using SEDU.TSDC.EntityFramework.DbContexts.Interfaces;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.EntityFramework.Extensions.Extensions;
using SEDU.TSDC.EntityFramework.Repositories.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Repositories
{
    public class XetDuyetHoSoRepository<TDbContext> : IXetDuyetHoSoRepository
        where TDbContext : ITsdcDbContext
    {
        protected readonly TDbContext dbContext;

        public XetDuyetHoSoRepository(TDbContext _dbContext)
        {
            dbContext = _dbContext;
        }

        public async Task<XET_DUYET_HO_SO> CreateXetDuyetHoSoAsync(XET_DUYET_HO_SO xetDuyetHoSo)
        {
            xetDuyetHoSo.THOI_GIAN_DUYET = DateTime.Now;
            await dbContext.XET_DUYET_HO_SO.AddAsync(xetDuyetHoSo);
            await dbContext.SaveChangeAsync();

            return xetDuyetHoSo;
        }

        public async Task<int> DeleteXetDuyetHoSoAsync(int id)
        {
            var XetDuyetHoSo = await dbContext.XET_DUYET_HO_SO.FindAsync(id);

            if (XetDuyetHoSo != null)
            {
                XetDuyetHoSo.IS_DELETED = true;
                XetDuyetHoSo.DELETED_DATE = DateTime.Now;

                await dbContext.SaveChangeAsync();
            }

            return id;
        }

        public async Task<XET_DUYET_HO_SO> EditXetDuyetHoSoAsync(XET_DUYET_HO_SO xetDuyetHoSo)
        {
            var XetDuyetHoSo = await dbContext.XET_DUYET_HO_SO.FindAsync(xetDuyetHoSo.ID);

            if (XetDuyetHoSo != null)
            {
                XetDuyetHoSo.GHI_CHU = xetDuyetHoSo.GHI_CHU;
                XetDuyetHoSo.ID_HO_SO = xetDuyetHoSo.ID_HO_SO;
                XetDuyetHoSo.IS_ACTIVE = xetDuyetHoSo.IS_ACTIVE;
                XetDuyetHoSo.UPDATED_DATE = DateTime.Now;
                XetDuyetHoSo.ID_NGUOI_DUYET = xetDuyetHoSo.ID_NGUOI_DUYET;
                XetDuyetHoSo.IP_DUYET = xetDuyetHoSo.IP_DUYET;
                XetDuyetHoSo.KET_QUA = xetDuyetHoSo.KET_QUA;
                XetDuyetHoSo.THOI_GIAN_DUYET = xetDuyetHoSo.THOI_GIAN_DUYET;
                await dbContext.SaveChangeAsync();
            }

            return XetDuyetHoSo;
        }

        public async Task<XET_DUYET_HO_SO> GetXetDuyetHoSoByIdAsync(int id)
        {
            return await dbContext.XET_DUYET_HO_SO.Where(c => c.ID == id && !c.IS_DELETED && c.IS_ACTIVE)
                                           .FirstOrDefaultAsync();
        }
        public async Task<PagedList<XET_DUYET_HO_SO>> GetPagedXetDuyetHoSoAsync(string keyword, int pageNumber, int pageSize)
        {
            var pagedList = new PagedList<XET_DUYET_HO_SO>();

            var data = await dbContext.XET_DUYET_HO_SO
                .Where(x => !x.IS_DELETED && x.IS_ACTIVE)
                .PageBy(x => x.ID, pageNumber, pageSize)
                .ToListAsync();

            pagedList.Data.AddRange(data);
            pagedList.PageSize = pageSize;
            pagedList.TotalCount = await dbContext.XET_DUYET_HO_SO.CountAsync();

            return pagedList;
        }
        public async Task<PagedList<XET_DUYET_HO_SO>> GetPagedXetDuyetHoSoAsync(int trang_thai_duyet_ho_so, bool trang_thai_nhap_hoc, int nam_xet_tuyen, string keyword, int pageNumber, int pageSize)
        {
            var pagedList = new PagedList<XET_DUYET_HO_SO>();

            var query = dbContext.XET_DUYET_HO_SO
                .Include(x => x.HO_SO)
                .Where(x => (string.IsNullOrEmpty(keyword) || x.HO_SO.MA_HO_SO.ToUpper().Contains(keyword) || x.HO_SO.TEN.ToUpper().Contains(keyword)) &&
                    (
                        //Đã từ chối && k nhập học
                        (trang_thai_duyet_ho_so == 1 && x.HO_SO.TRANG_THAI_DUYET_HO_SO == 1 && x.HO_SO.TRANG_THAI_NHAP_HOC == trang_thai_nhap_hoc) ||
                        //Đã đồng ý & nhập học
                        (trang_thai_duyet_ho_so == 2 && x.HO_SO.TRANG_THAI_DUYET_HO_SO == 2 && x.HO_SO.TRANG_THAI_NHAP_HOC == trang_thai_nhap_hoc)
                    ) &&
                    (nam_xet_tuyen == 0 || x.HO_SO.NAM_XET_TUYEN == nam_xet_tuyen) &&
                    !x.HO_SO.IS_DELETED &&
                    x.HO_SO.IS_ACTIVE
                );

            var data = await query
                .PageBy(x => x.ID, pageNumber, pageSize)
                .ToListAsync();

            pagedList.Data.AddRange(data);
            pagedList.PageSize = pageSize;
            pagedList.TotalCount = await query.CountAsync();

            return pagedList;
        }
    }
}
