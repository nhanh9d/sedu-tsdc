﻿using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Repositories.Interfaces
{
    public interface IVaiTroRepository
    {
        Task<VAI_TRO> CreateVaiTroAsync(VAI_TRO client);
        Task<int> DeleteVaiTroAsync(int id);
        Task<VAI_TRO> EditVaiTroAsync(VAI_TRO client);
        Task<VAI_TRO> GetVaiTroByIdAsync(int id);
        Task<PagedList<VAI_TRO>> GetPagedVaiTroAsync(string keyword, int pageNumber, int pageSize);
        Task<VAI_TRO> GetVaiTroByNameAsync(string name);
        Task<bool> KiemTraTonTaiVaiTroBangTen(string ChucVu);
    }
}
