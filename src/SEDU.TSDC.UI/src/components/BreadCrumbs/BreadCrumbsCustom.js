import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
// import Typography from "@material-ui/core/Typography";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import "./BreadCrumbsCustom.css";
// import { NavLink } from 'react-router-dom';
// import Link from '@material-ui/core/Link';
import { NavLink } from "react-router-dom";
import Box from "@material-ui/core/Box";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));

const getNameAndPath = (path, homeTabName) => {
  let name = "";
  if (path === "") {
    name = homeTabName;
  }
  if (path === "hoi-dong-tuyen-sinh") {
    name = "Hội đồng tuyển sinh";
  }
  if (path === "sua-thong-tin-thanh-vien") {
    name = "Sửa thông tin thành viên";
    path = "hoi-dong-tuyen-sinh/sua-thong-tin-thanh-vien";
  }
  if (path === "quan-ly-cac-dot-tuyen-sinh") {
    name = "Quản lý các đợt tuyển sinh";
  }

  if (path === "bao-cao-thong-ke") {
    name = "Báo cáo - thống kê";
  }
  return { name: name, path: `/${path}` };
};

const BreadCrumbsCustom = (props) => {
  const [list, setList] = useState([{ name: "Trang chủ", path: "/" }]);

  // update list base on current path and tab name in homepage
  useEffect(() => {
    let listPath = [{ name: "Trang chủ", path: "/" }];
    for (let path of props.pathname.split("/").slice(1)) {
      listPath.push(getNameAndPath(path, props.homeTabName));
    }

    setList(listPath);
  }, [props.pathname, props.homeTabName]);

  const classes = useStyles();

  // render list
  const renderedList = list.map((item, index) => {
    return (
      <NavLink key={index} exact activeClassName="active" to={item.path}>
        {item.name}
      </NavLink>
    );
  });

  return (
    <div className={classes.root}>
      <Box mt={1} mb={2}>
        <Breadcrumbs
          separator={<NavigateNextIcon fontSize="small" />}
          aria-label="breadcrumb"
        >
          {renderedList}
        </Breadcrumbs>
      </Box>
    </div>
  );
};
export default BreadCrumbsCustom;
