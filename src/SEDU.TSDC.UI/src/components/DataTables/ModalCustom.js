import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

import Modal from '@material-ui/core/Modal'
const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[2],
    padding: theme.spacing(2, 4, 3),
    zIndex: 999,
  },
}))
const ModalCustom = (props) => {
  const classes = useStyles()
  const [loading, isLoading] = React.useState(true)

  const handleClose = () => {
    isLoading(false)
    props.handleCloseModal()
  }

  return (
    <Modal
      open={loading}
      onClose={handleClose}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
      className={classes.modal}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <div className={classes.paper}>{props.children}</div>
    </Modal>
  )
}
export default ModalCustom
