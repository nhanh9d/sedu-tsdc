﻿using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.EntityFramework.Repositories.Interfaces;
using SEDU.TSDC.Services.Dtos.Tsdc;
using SEDU.TSDC.Services.Mappers;
using SEDU.TSDC.Services.Services.Interface;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Services
{
    public class NguoiDungService : INguoiDungService
    {
        private readonly INguoiDungRepository nguoiDungRepository;

        public NguoiDungService(INguoiDungRepository _nguoiDungRepository)
        {
            nguoiDungRepository = _nguoiDungRepository;
            // To save physical files to a path provided by configuration:
        }

        public async Task<NguoiDungDto> AddNguoiDungAsync(NguoiDungDto nguoiDung)
        {
            var nguoiDungEntity = nguoiDung.ToEntity();
            nguoiDungEntity = await nguoiDungRepository.CreateNguoiDungAsync(nguoiDungEntity);
            return nguoiDungEntity.ToDto();
        }

        public async Task<int> DeleteNguoiDungAsync(int nguoiDungId)
        {
            return await nguoiDungRepository.DeleteNguoiDungAsync(nguoiDungId);
        }

        public async Task<NguoiDungDto> EditNguoiDungAsync(NguoiDungDto nguoiDung)
        {
            var nguoiDungEntity = nguoiDung.ToEntity();
            nguoiDungEntity = await nguoiDungRepository.EditNguoiDungAsync(nguoiDungEntity);
            return nguoiDungEntity.ToDto();
        }

        public async Task<NguoiDungDto> GetNguoiDungByIdAsync(int nguoiDungId)
        {
            var nguoiDungEntity = await nguoiDungRepository.GetNguoiDungByIdAsync(nguoiDungId);

            if (nguoiDungEntity == null)
            {
                return default;
            }

            return nguoiDungEntity.ToDto();
        }
        public async Task<PagedList<NguoiDungDto>> GetPagedNguoiDungAsync(string keyword, int pageNumber, int pageSize)
        {
            var listEntity = await nguoiDungRepository.GetPagedNguoiDungAsync(keyword, pageNumber, pageSize);
            return listEntity.ToDto();
        }
    }
}
