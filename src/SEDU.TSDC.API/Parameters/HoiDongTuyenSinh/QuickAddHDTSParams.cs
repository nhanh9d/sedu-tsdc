﻿

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Parameters.HoiDongTuyenSinh
{
    public class QuickAddHDTSParams: CreateHoiDongTuyenSinhParams
    {
        [MaxLength(30, ErrorMessage = "Họ và đệm tối đa 30 kí tự")]
        public string TenNguoiDung { get; set; }
        [MaxLength(12, ErrorMessage = "Số điện thoại tối đa 12 kí tự")]
        public string SoDienThoai { get; set; }
        public string TenVaiTro { get; set; }
    }
}
