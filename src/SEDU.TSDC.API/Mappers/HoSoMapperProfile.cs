﻿using AutoMapper;
using SEDU.TSDC.API.Parameters.HoSo;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System;

namespace SEDU.TSDC.API.Mappers
{
    public class HoSoMapperProfile : Profile
    {
        public HoSoMapperProfile()
        {
            CreateMap<CreateHoSoParams, HoSoDto>(MemberList.Destination);
            CreateMap<EditHoSoParams, HoSoDto>(MemberList.Destination);
        }
    }
}
