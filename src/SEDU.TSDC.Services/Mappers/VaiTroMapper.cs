﻿using AutoMapper;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.Services.Mappers
{
    public static class VaiTroMapper
    {
        internal static IMapper DtoMapper;
        internal static IMapper EntityMapper;

        static VaiTroMapper()
        {
            DtoMapper = new MapperConfiguration(config => config.AddProfile<VaiTroDtoMapperProfile>()).CreateMapper();
            EntityMapper = new MapperConfiguration(config => config.AddProfile<VaiTroEntityMapperProfile>()).CreateMapper();
        }

        //public static List<Activity> ToModel(this List<APP_ACTIVITY> _APP_ACTIVITY)
        //{
        //    return Mapper.Map<List<Activity>>(_APP_ACTIVITY);
        //}

        //public static Activity ToModel(this APP_ACTIVITY _APP_ACTIVITY)
        //{
        //    return Mapper.Map<Activity>(_APP_ACTIVITY);
        //}

        public static VAI_TRO ToEntity(this VaiTroDto vaiTroDto)
        {
            return EntityMapper.Map<VAI_TRO>(vaiTroDto);
        }

        public static VaiTroDto ToDto(this VAI_TRO vaiTro)
        {
            return DtoMapper.Map<VaiTroDto>(vaiTro);
        }

        public static PagedList<VaiTroDto> ToDto(this PagedList<VAI_TRO> vaiTro)
        {
            return DtoMapper.Map<PagedList<VaiTroDto>>(vaiTro);
        }
    }
}
