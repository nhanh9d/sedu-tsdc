export const options_ticket = {
  headers: {
    Ticket: localStorage.getItem("ticket"),
  },
};
// export const IP_API = "http://35.202.151.50:8000"
export const IP_API = process.env.NODE_ENV !== 'production' ? "http://localhost:5000" :  "http://35.202.151.50:8000"
