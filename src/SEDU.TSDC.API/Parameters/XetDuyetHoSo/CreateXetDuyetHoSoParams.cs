﻿

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Parameters.XetDuyetHoSo
{
    public class CreateXetDuyetHoSoParams
    {
        /// <summary>
        /// Thời gian duyệt hồ sơ
        /// </summary>
        public DateTime ThoiGianDuyet { get; set; }
        /// <summary>
        /// Địa chỉ IP của người duyệt hồ sơ.
        /// </summary>
        [MaxLength(50, ErrorMessage = "Địa chỉ IP của người duyệt hồ sơ tối đa 50 kí tự")]
        public string IpDuyet { get; set; }
        /// <summary>
        /// 1: Từ chối
        /// 2: Đồng ý
        /// </summary>
        public int KetQua { get; set; }
        /// <summary>
        /// Lý do bị loại (nếu có)
        /// </summary>
        public string GhiChu { get; set; }
        /// <summary>
        /// id hồ sơ, tham chiếu tới ho_so.id
        /// </summary>
        public int IdHoSo { get; set; }
        /// <summary>
        /// ID của người duyệt hồ sơ trong bảng nguoi_dung
        /// </summary>
        public int IdNguoiDuyet { get; set; }
    }
}
