﻿using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.EntityFramework.Repositories.Interfaces;
using SEDU.TSDC.Services.Dtos.Tsdc;
using SEDU.TSDC.Services.Mappers;
using SEDU.TSDC.Services.Services.Interface;
using SEDU.TSDC.Services.Shared.Validation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Services
{
    public class HoiDongTuyenSinhService : IHoiDongTuyenSinhService
    {
        private readonly IHoiDongTuyenSinhRepository hoiDongTuyenSinhRepository;
        private readonly INguoiDungRepository nguoiDungRepository;
        private readonly IVaiTroRepository vaiTroRepository;

        public HoiDongTuyenSinhService(IHoiDongTuyenSinhRepository _hoiDongTuyenSinhRepository, INguoiDungRepository _nguoiDungRepository, IVaiTroRepository _vaiTroRepository)
        {
            hoiDongTuyenSinhRepository = _hoiDongTuyenSinhRepository;
            nguoiDungRepository = _nguoiDungRepository;
            vaiTroRepository = _vaiTroRepository;
            // To save physical files to a path provided by configuration:
        }

        public async Task<HoiDongTuyenSinhDto> AddHoiDongTuyenSinhAsync(HoiDongTuyenSinhDto hoiDongTuyenSinh)
        {
            NGUOI_DUNG ItemNguoiDung = new NGUOI_DUNG();
            ItemNguoiDung.HO_TEN = hoiDongTuyenSinh.TenNguoiDung;
            ItemNguoiDung.SO_DT = hoiDongTuyenSinh.SoDienThoai;
            var ResultNguoiDung = await nguoiDungRepository.CreateNguoiDungAsync(ItemNguoiDung);
            var ItemVaiTro = await vaiTroRepository.GetVaiTroByNameAsync(hoiDongTuyenSinh.VaiTro_QuickAdd);
            var hoiDongTuyenSinhEntity = hoiDongTuyenSinh.ToEntity();
            var CurrentYear = DateTime.Now.Year;
            if (ItemVaiTro != null && ResultNguoiDung != null)
            {
                hoiDongTuyenSinhEntity.ID_NGUOI_DUNG = ResultNguoiDung.ID;
                hoiDongTuyenSinhEntity.ID_VAI_TRO = ItemVaiTro.ID;
                hoiDongTuyenSinhEntity.NAM_TUYEN_SINH = CurrentYear;
                hoiDongTuyenSinhEntity.ID_CSGD = hoiDongTuyenSinh.IdCoSoGiaoDuc;
            }
            hoiDongTuyenSinhEntity = await hoiDongTuyenSinhRepository.CreateHoiDongTuyenSinhAsync(hoiDongTuyenSinhEntity);
            return hoiDongTuyenSinhEntity.ToDto();
        }

        public async Task<int> DeleteHoiDongTuyenSinhAsync(int hoiDongTuyenSinhId)
        {
            return await hoiDongTuyenSinhRepository.DeleteHoiDongTuyenSinhAsync(hoiDongTuyenSinhId);
        }

        public async Task<HoiDongTuyenSinhDto> EditHoiDongTuyenSinhAsync(HoiDongTuyenSinhDto hoiDongTuyenSinh)
        {
            var hoiDongTuyenSinhEntity = hoiDongTuyenSinh.ToEntity();
            hoiDongTuyenSinhEntity = await hoiDongTuyenSinhRepository.EditHoiDongTuyenSinhAsync(hoiDongTuyenSinhEntity);
            return hoiDongTuyenSinhEntity.ToDto();
        }
        public async Task<HoiDongTuyenSinhDto> EditHDTSWithUserAsync(HoiDongTuyenSinhDto hoiDongTuyenSinh)
        {
            var hoiDongTuyenSinhEntity = hoiDongTuyenSinh.ToEntity();
            hoiDongTuyenSinhEntity = await hoiDongTuyenSinhRepository.EditHoiDongTuyenSinhAsync(hoiDongTuyenSinhEntity);

            // Edit Người dùng
            var nguoiDungEntity = await nguoiDungRepository.GetNguoiDungByIdAsync(hoiDongTuyenSinh.IdNguoiDung);
            if(nguoiDungEntity != null)
            {
                nguoiDungEntity.HO_TEN = hoiDongTuyenSinh.TenNguoiDung;
                nguoiDungEntity.SO_DT = hoiDongTuyenSinh.SoDienThoai;
                var resultEdit = await nguoiDungRepository.EditNguoiDungAsync(nguoiDungEntity); 
            }
            return hoiDongTuyenSinhEntity.ToDto();
        }

        public async Task<HoiDongTuyenSinhDto> GetHoiDongTuyenSinhByIdAsync(int hoiDongTuyenSinhId)
        {
            var hoiDongTuyenSinhEntity = await hoiDongTuyenSinhRepository.GetHoiDongTuyenSinhByIdAsync(hoiDongTuyenSinhId);

            if (hoiDongTuyenSinhEntity == null)
            {
                return default;
            }

            return hoiDongTuyenSinhEntity.ToDto();
        }
        public async Task<PagedList<HoiDongTuyenSinhDto>> GetPagedHoiDongTuyenSinhAsync(string keyword, int pageNumber, int pageSize)
        {
            var listEntity = await hoiDongTuyenSinhRepository.GetPagedHoiDongTuyenSinhAsync(keyword, pageNumber, pageSize);
            return listEntity.ToDto();
        }
        public async Task<List<HoiDongTuyenSinhDto>> GetListHDTSByIdCSGD(int IdCSGD)
        {
            var listEntities = await hoiDongTuyenSinhRepository.GetListHDTSByIdCSGDAsync(IdCSGD);

            return listEntities.ToListDto();
        }

        public async Task<List<object>> ThemNhieuHoiDongTuyenSinh(List<HoiDongTuyenSinhDto> lstHdts)
        {
            var CurrentYear = DateTime.Now.Year;
            var response = new List<object>();
            var message = "Lưu trữ thành công.";
            var isValid = true;
            var isValid1 = true;
            var isValid2 = true;
            for (int i = 0; i < lstHdts.Count; i++)
            {
                var hdts = lstHdts[i];
                /*
                 * Kiểm tra dữ liệu:
                 * - Check null
                 * - Check các điều kiện như: tên không dài quá, số điện thoại
                 * - Check dưới db:
                 *   + Check xem họ tên với số điện thoại nó đã tồn tại hay chưa => Có rồi thì reject record
                 *   + Check xem vai trò có tồn tại dưới db không => Chưa có thì reject record
                 */
                isValid1 = StringValidation.CheckSoDienThoai(hdts.SoDienThoai);
                if (!isValid1)
                {
                    message = "Số điện thoại không hợp lệ.";
                }
                if(hdts.TenNguoiDung != "" && hdts.SoDienThoai != "")
                {
                    isValid2 = !await nguoiDungRepository.KiemTraTonTaiNguoiDung(hdts.TenNguoiDung.Trim().ToUpper(), hdts.SoDienThoai.Trim().ToUpper());
                    if (!isValid2)
                    {
                        message = "Họ tên và số điện thoại đã được sử dụng.";
                    }
                }    
                var ItemVaiTro = await vaiTroRepository.GetVaiTroByNameAsync(hdts.VaiTro);
                if (ItemVaiTro == null)
                {
                    message = "Chức vụ không tồn tại.";
                }
                if (ItemVaiTro != null && isValid1 && isValid2)
                {
                 /*
                 * Sau khi đã validate các thông tin
                 * Thì lưu xuống database
                 */
                    var nguoiDung = await nguoiDungRepository.CreateNguoiDungAsync(new()
                    {
                        HO_TEN = hdts.TenNguoiDung,
                        SO_DT = hdts.SoDienThoai
                    });
                    await hoiDongTuyenSinhRepository.CreateHoiDongTuyenSinhAsync(new()
                    {
                        ID_NGUOI_DUNG = nguoiDung.ID,
                        ID_VAI_TRO = ItemVaiTro.ID,
                        ID_CSGD = hdts.IdCoSoGiaoDuc,
                        NAM_TUYEN_SINH = CurrentYear
                    });
                }
                else
                {
                    isValid = false;
                }    
                response.Add(new { STT = i, isValid, message });
            }
            return response;
        }
    }
}
