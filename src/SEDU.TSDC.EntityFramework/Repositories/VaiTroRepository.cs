﻿using Microsoft.EntityFrameworkCore;
using SEDU.TSDC.EntityFramework.DbContexts.Interfaces;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.EntityFramework.Extensions.Extensions;
using SEDU.TSDC.EntityFramework.Repositories.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Repositories
{
    public class VaiTroRepository<TDbContext> : IVaiTroRepository
        where TDbContext : ITsdcDbContext
    {
        protected readonly TDbContext dbContext;

        public VaiTroRepository(TDbContext _dbContext)
        {
            dbContext = _dbContext;
        }

        public async Task<VAI_TRO> CreateVaiTroAsync(VAI_TRO vaiTro)
        {
            await dbContext.VAI_TRO.AddAsync(vaiTro);
            await dbContext.SaveChangeAsync();

            return vaiTro;
        }

        public async Task<int> DeleteVaiTroAsync(int id)
        {
            var VaiTro = await dbContext.VAI_TRO.FindAsync(id);

            if (VaiTro != null)
            {
                VaiTro.IS_DELETED = true;
                VaiTro.DELETED_DATE = DateTime.Now;

                await dbContext.SaveChangeAsync();
            }

            return id;
        }

        public async Task<VAI_TRO> EditVaiTroAsync(VAI_TRO vaiTro)
        {
            var VaiTro = await dbContext.VAI_TRO.FindAsync(vaiTro.ID);

            if (VaiTro != null)
            {
                VaiTro.TEN_VAI_TRO = vaiTro.TEN_VAI_TRO;
                VaiTro.UPDATED_DATE = DateTime.Now;
                VaiTro.IS_ACTIVE = vaiTro.IS_ACTIVE;
                await dbContext.SaveChangeAsync();
            }

            return VaiTro;
        }

        public async Task<VAI_TRO> GetVaiTroByIdAsync(int id)
        {
            return await dbContext.VAI_TRO.Where(c => c.ID == id && !c.IS_DELETED && c.IS_ACTIVE)
                                           .FirstOrDefaultAsync();
        }
        public async Task<VAI_TRO> GetVaiTroByNameAsync(string name)
        {
            return await dbContext.VAI_TRO.Where(c => c.TEN_VAI_TRO.ToUpper().Equals(name.ToUpper().Trim()) && !c.IS_DELETED && c.IS_ACTIVE)
                                           .FirstOrDefaultAsync();
        }
        public async Task<PagedList<VAI_TRO>> GetPagedVaiTroAsync(string keyword, int pageNumber, int pageSize)
        {
            var pagedList = new PagedList<VAI_TRO>();
            var querry = dbContext.VAI_TRO
                .Where(x => !x.IS_DELETED && x.IS_ACTIVE && x.TEN_VAI_TRO.Contains(keyword));
            var data = await querry
                .PageBy(x => x.ID, pageNumber, pageSize)
                .ToListAsync();

            pagedList.Data.AddRange(data);
            pagedList.PageSize = pageSize;
            pagedList.TotalCount = await querry.CountAsync();

            return pagedList;
        }

        public async Task<bool> KiemTraTonTaiVaiTroBangTen(string ChucVu)
        {
            return await dbContext.VAI_TRO.Where(vt => vt.TEN_VAI_TRO.ToUpper() == ChucVu).AnyAsync();
        }
    }
}
