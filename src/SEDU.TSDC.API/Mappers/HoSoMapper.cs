﻿using AutoMapper;
using SEDU.TSDC.API.Parameters.HoSo;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.API.Mappers
{
    public static class HoSoMapper
    {
        internal static IMapper DtoMapper;

        static HoSoMapper()
        {
            DtoMapper = new MapperConfiguration(config => config.AddProfile<HoSoMapperProfile>()).CreateMapper();
        }

        public static HoSoDto ToDto(this CreateHoSoParams nguoiDung)
        {
            return DtoMapper.Map<HoSoDto>(nguoiDung);
        }

        public static HoSoDto ToDto(this EditHoSoParams nguoiDung)
        {
            return DtoMapper.Map<HoSoDto>(nguoiDung);
        }

        public static PagedList<HoSoDto> ToDto(this PagedList<NGUOI_DUNG> nguoiDung)
        {
            return DtoMapper.Map<PagedList<HoSoDto>>(nguoiDung);
        }
    }
}
