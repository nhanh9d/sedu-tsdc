﻿using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.EntityFramework.Repositories.Interfaces;
using SEDU.TSDC.Services.Dtos.Tsdc;
using SEDU.TSDC.Services.Mappers;
using SEDU.TSDC.Services.Services.Interface;
using System;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Services
{
    public class HoSoService : IHoSoService
    {
        private readonly IHoSoRepository hoSoRepository;

        public HoSoService(IHoSoRepository _hoSoRepository)
        {
            hoSoRepository = _hoSoRepository;
        }

        public async Task<HoSoDto> AddHoSoAsync(HoSoDto hoSo)
        {
            var hoSoEntity = hoSo.ToEntity();
            hoSoEntity = await hoSoRepository.CreateHoSoAsync(hoSoEntity);
            return hoSoEntity.ToDto();
        }

        public async Task<int> DeleteHoSoAsync(int hoSoId)
        {
            return await hoSoRepository.DeleteHoSoAsync(hoSoId);
        }

        public async Task<HoSoDto> EditHoSoAsync(HoSoDto hoSo)
        {
            var hoSoEntity = hoSo.ToEntity();
            hoSoEntity = await hoSoRepository.EditHoSoAsync(hoSoEntity);
            return hoSoEntity.ToDto();
        }

        public async Task<HoSoDto> GetHoSoByIdAsync(int hoSoId)
        {
            var hoSoEntity = await hoSoRepository.GetHoSoByIdAsync(hoSoId);

            if (hoSoEntity == null)
            {
                return default;
            }

            return hoSoEntity.ToDto();
        }
        public async Task<PagedList<HoSoDto>> GetPagedHoSoAsync(string trang_thai_duyet_ho_so, bool trang_thai_nhap_hoc, int nam_xet_tuyen, string keyword, int pageNumber, int pageSize)
        {
            var listEntity = await hoSoRepository.GetPagedHoSoAsync(Convert.ToInt32(trang_thai_duyet_ho_so), trang_thai_nhap_hoc, nam_xet_tuyen, keyword.Trim().ToUpper(), pageNumber, pageSize);
            return listEntity.ToDto();
        }
    }
}
