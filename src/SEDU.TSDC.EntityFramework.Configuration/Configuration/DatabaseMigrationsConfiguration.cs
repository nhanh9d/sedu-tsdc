﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Configuration.Configuration
{
	public class DatabaseMigrationsConfiguration
	{
		public bool ApplyDatabaseMigrations { get; set; } = false;

		public string TsdcDbMigrationsAssembly { get; set; }

		public void SetMigrationsAssemblies(string commonMigrationsAssembly)
		{
			TsdcDbMigrationsAssembly = commonMigrationsAssembly;
		}
	}
}
