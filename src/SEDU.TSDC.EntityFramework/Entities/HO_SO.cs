﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Entities
{
    public class HO_SO : BASE_ENTITY
    {
        [MaxLength(50, ErrorMessage = "Mã hồ sơ tối đa 50 kí tự")]
        public string MA_HO_SO { get; set; }
        public int NAM_XET_TUYEN { get; set; }
        [MaxLength(30, ErrorMessage = "Họ và đệm tối đa 30 kí tự")]
        public string HO_DEM { get; set; }
        [MaxLength(10, ErrorMessage = "Tên tối đa 10 kí tự")]
        public string TEN { get; set; }
        public DateTime NGAY_SINH { get; set; }
        [MaxLength(50, ErrorMessage = "ID của CSGD trên cổng dịch vụ tối đa 50 kí tự")]
        public string ID_CSGD_CDV { get; set; }
        public int TRANG_THAI_DUYET_HO_SO { get; set; } 
        public DateTime THOI_DIEM_CAP_NHAT_CUOI { get; set; }
        public bool TRANG_THAI_NHAP_HOC { get; set; }

        #region Relation props
        public virtual ICollection<XET_DUYET_HO_SO> BO_XET_DUYET { get; set; }
        #endregion

    }
}
