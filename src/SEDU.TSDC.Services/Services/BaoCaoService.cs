﻿using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.EntityFramework.Repositories.Interfaces;
using SEDU.TSDC.Services.Dtos.Tsdc;
using SEDU.TSDC.Services.Mappers;
using SEDU.TSDC.Services.Services.Interface;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Services
{
    public class BaoCaoService : IBaoCaoService
    {
        private readonly IBaoCaoRepository baoCaoRepository;

        public BaoCaoService(IBaoCaoRepository _baoCaoRepository)
        {
            baoCaoRepository = _baoCaoRepository;
        }

        public async Task<BaoCaoDto> AddBaoCaoAsync(BaoCaoDto baoCao)
        {
            var baoCaoEntity = baoCao.ToEntity();
            baoCaoEntity = await baoCaoRepository.CreateBaoCaoAsync(baoCaoEntity);
            return baoCaoEntity.ToDto();
        }

        public async Task<int> DeleteBaoCaoAsync(int baoCaoId)
        {
            return await baoCaoRepository.DeleteBaoCaoAsync(baoCaoId);
        }

        public async Task<BaoCaoDto> EditBaoCaoAsync(BaoCaoDto baoCao)
        {
            var baoCaoEntity = baoCao.ToEntity();
            baoCaoEntity = await baoCaoRepository.EditBaoCaoAsync(baoCaoEntity);
            return baoCaoEntity.ToDto();
        }

        public async Task<BaoCaoDto> GetBaoCaoByIdAsync(int baoCaoId)
        {
            var baoCaoEntity = await baoCaoRepository.GetBaoCaoByIdAsync(baoCaoId);

            if (baoCaoEntity == null)
            {
                return default;
            }

            return baoCaoEntity.ToDto();
        }
        public async Task<PagedList<BaoCaoDto>> GetPagedBaoCaoAsync(string keyword, int pageNumber, int pageSize)
        {
            var listEntity = await baoCaoRepository.GetPagedBaoCaoAsync(keyword, pageNumber, pageSize);
            return listEntity.ToDto();
        }
        public async Task<List<BaoCaoDto>> GetListDotTuyenSinh()
        {
            var listEntities = await baoCaoRepository.GetListDotTuyenSinh();

            return listEntities.ToListDto();
        }
    }
}
