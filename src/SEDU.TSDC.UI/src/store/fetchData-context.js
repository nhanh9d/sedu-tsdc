// context to fetch all data in the app BUT no longer being used because of exhausting the server.

import React, { useState } from 'react'
import axios from 'axios'
import { IP_API } from '../helper/helper'
import { options_ticket } from '../helper/helper'
import { makeStyles } from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'
import Backdrop from '@material-ui/core/Backdrop'

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}))
const FetchDataContext = React.createContext({
  listData: [],
  listNotApproved: [],
  listApproved: [],
  listAdmitted: [],
  listRejected: [],
  // searchKeyWord: '',
  // editItem: (item) => {},
  handleAdmitReject: () => {},
  // handleSearchKeyWord: (key) => {},
})

export const FetchDataContextProvider = (props) => {
  const [listNotApproved, setlistNotApproved] = useState([])
  const [listApproved, setListApproved] = useState([])
  const [listAdmitted, setListAdmitted] = useState([])
  const [listRejected, setListRejected] = useState([])
  const [listData, setListData] = useState([])
  const classes = useStyles()
  const [loading, isLoading] = React.useState(false)
  // const [key, setKey] = useState('')
  const params = {
    trang_thai_nhap_hoc: true,
    trang_thai_duyet_ho_so: '0',
    nam_xet_tuyen: 2021,
    keyword: '',
    pageNumber: 1,
    pageSize: 100,
  }
  /*
   * Call api in order:
      + Not Approved => 0 
      + Rejected => 1 
      + Admitted => 2 
   */

  /* -------------- Functions to call apis --------------- */

  // fetch all not approved submitees
  const fetchNotApproved = () => {
    const fetchData = async () => {
      isLoading(true)
      await axios
        .post(IP_API + '/HoSo/get-paged-ho-so', params, options_ticket)
        .then((response) => {
          // call API thanh cong
          // setListDataReal
          if (response && response.data) {
            setlistNotApproved(response.data.data)
          }
        })
        .catch((error) => {
          console.log('unauthorize')
          if (error.response.status === 401) {
            window.location.href =
              'https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback'
          }
        })
      isLoading(false)
    }
    fetchData()
  }

  // fetch all approved submitees
  const fetchApproved = () => {
    params.trang_thai_duyet_ho_so = '3'
    const fetchData = async () => {
      isLoading(true)
      await axios
        .post(
          IP_API + '/XetDuyetHoSo/get-paged-loai-xet-duyet-ho-so',
          params,
          options_ticket
        )
        .then((response) => {
          // call API thanh cong
          // setListDataReal
          if (response && response.data) {
            setListApproved(response.data.data)
          }
        })
        .catch((error) => {
          if (error.response.status === 401) {
            window.location.href =
              'https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback'
          }
        })
      isLoading(false)
    }
    fetchData()
  }

  // fetch all rejected submitees
  const fetchRejected = () => {
    params.trang_thai_duyet_ho_so = '1'
    const fetchData = async () => {
      isLoading(true)
      try {
        await axios
          .post(IP_API + '/HoSo/get-paged-ho-so', params, options_ticket)
          .then((response) => {
            // call API thanh cong
            // setListDataReal
            if (response && response.data) {
              // setListData([...listData, ...response.data.data]);
              setListRejected(response.data.data)
            }
          })
          .catch((error) => {
            if (error.response.status === 401) {
              window.location.href =
                'https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback'
            }
          })
      } catch (error) {}
      isLoading(false)
    }
    fetchData()
  }

  // get all 'Admitted'
  const fetchAdmitted = () => {
    params.trang_thai_duyet_ho_so = '2'
    const fetchData = async () => {
      isLoading(true)
      try {
        await axios
          .post(
            IP_API + '/XetDuyetHoSo/get-paged-loai-xet-duyet-ho-so',
            params,
            options_ticket
          )
          .then((response) => {
            // call API thanh cong
            // setListDataReal
            if (response && response.data) {
              setListAdmitted(response.data.data)
            }
          })
          .catch((error) => {
            if (error.response.status === 401) {
              window.location.href =
                'https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback'
            }
          })
      } catch (error) {}
      isLoading(false)
    }
    fetchData()
  }

  // get all 'All Data'
  const fetchAllData = () => {
    params.trang_thai_duyet_ho_so = '4'
    const fetchData = async () => {
      isLoading(true)
      try {
        await axios
          .post(IP_API + '/HoSo/get-paged-ho-so', params, options_ticket)
          .then((response) => {
            // call API thanh cong
            // setListDataReal
            if (response && response.data) {
              setListData(response.data.data)
            }
          })
          .catch((error) => {
            if (error.response.status === 401) {
              window.location.href =
                'https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback'
            }
          })
      } catch (error) {}
      isLoading(false)
    }
    fetchData()
  }

  // useEffect(() => {
  //   fetchNotApproved()
  //   fetchApproved()
  //   fetchRejected()
  //   fetchAdmitted()
  //   fetchAllData()
  // }, [])

  // Handle search
  const handleSearch = (searchKey, status) => {
    // set route
    const route = ['2', '3'].includes(status)
      ? '/XetDuyetHoSo/get-paged-loai-xet-duyet-ho-so'
      : '/HoSo/get-paged-ho-so'
    // call api
    params.keyword = searchKey
    params.trang_thai_duyet_ho_so = status
    const fetchData = async () => {
      isLoading(true)
      try {
        await axios
        .post(IP_API + route, params, options_ticket)
        .then((response) => {
          // call API thanh cong
          // setListDataReal
          if (response && response.data) {
            switch (status) {
              case "0":
                setlistNotApproved(response.data.data);
                break;
              case "1":
                setListRejected(response.data.data);
                break;
              case "2":
                // console.log(response.data.data);
                setListAdmitted(response.data.data);
                break;
              case "3":
                setListApproved(response.data.data);
                break;
              default:
                setListData(response.data.data);
                break;
            }
          }})
          .catch((error) => {
            if (error.response.status === 401) {
              window.location.href =
                'https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback'
            }
          })
      } catch (error) {}
      isLoading(false)
    }
    fetchData()
  }


  // Handle admit || reject
  const handleAdmitReject = () => {
    fetchNotApproved()
    fetchApproved()
    fetchRejected()
    fetchAdmitted()
    fetchAllData()
  }
  // const handleSearchKeyWord = (keyword) => {
  //   setKey(keyword)
  // }
  return (
    <FetchDataContext.Provider
      value={{
        listData: listData,
        listNotApproved: listNotApproved,
        listApproved: listApproved,
        listAdmitted: listAdmitted,
        listRejected: listRejected,
        handleSearch: handleSearch,
        handleAdmitReject: handleAdmitReject,
        // searchKeyWord: key,
        // handleSearchKeyWord: handleSearchKeyWord,
        // editItem: handleEditItem,
      }}
    >
      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
      {props.children}
    </FetchDataContext.Provider>
  )
}
export default FetchDataContext
