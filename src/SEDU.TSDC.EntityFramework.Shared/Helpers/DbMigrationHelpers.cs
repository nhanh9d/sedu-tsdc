﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SEDU.TSDC.EntityFramework.Configuration.Configuration;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Shared.Helpers
{
    public static class DbMigrationHelpers
    {
        public static async Task EnsureDatabasesMigratedAsync<TTsdcDbContext>(IHost host, DatabaseMigrationsConfiguration databaseMigrationsConfiguration)
            where TTsdcDbContext : DbContext
        {
            using var serviceScope = host.Services.CreateScope();
            var services = serviceScope.ServiceProvider;
            using var scope = services.GetRequiredService<IServiceScopeFactory>().CreateScope();
            using var context = scope.ServiceProvider.GetRequiredService<TTsdcDbContext>();
            await context.Database.MigrateAsync();
        }
    }
}
