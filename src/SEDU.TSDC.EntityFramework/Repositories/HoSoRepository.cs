﻿using Microsoft.EntityFrameworkCore;
using SEDU.TSDC.EntityFramework.DbContexts.Interfaces;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.EntityFramework.Extensions.Extensions;
using SEDU.TSDC.EntityFramework.Repositories.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Repositories
{
    public class HoSoRepository<TDbContext> : IHoSoRepository
        where TDbContext : ITsdcDbContext
    {
        protected readonly TDbContext dbContext;

        public HoSoRepository(TDbContext _dbContext)
        {
            dbContext = _dbContext;
        }

        public async Task<HO_SO> CreateHoSoAsync(HO_SO hoSo)
        {
            await dbContext.HO_SO.AddAsync(hoSo);
            await dbContext.SaveChangeAsync();

            return hoSo;
        }

        public async Task<int> DeleteHoSoAsync(int id)
        {
            var HoSo = await dbContext.HO_SO.FindAsync(id);

            if (HoSo != null)
            {
                HoSo.IS_DELETED = true;
                HoSo.DELETED_DATE = DateTime.Now;

                await dbContext.SaveChangeAsync();
            }

            return id;
        }

        public async Task<HO_SO> EditHoSoAsync(HO_SO hoSo)
        {
            var HoSo = await dbContext.HO_SO.FindAsync(hoSo.ID);

            if (HoSo != null)
            {
                HoSo.HO_DEM = hoSo.HO_DEM;
                HoSo.TEN = hoSo.TEN;
                HoSo.IS_ACTIVE = hoSo.IS_ACTIVE;
                HoSo.UPDATED_DATE = DateTime.Now;
                HoSo.ID_CSGD_CDV = hoSo.ID_CSGD_CDV;
                HoSo.MA_HO_SO = hoSo.MA_HO_SO;
                HoSo.NAM_XET_TUYEN = hoSo.NAM_XET_TUYEN;
                HoSo.NGAY_SINH = hoSo.NGAY_SINH;
                HoSo.THOI_DIEM_CAP_NHAT_CUOI = hoSo.THOI_DIEM_CAP_NHAT_CUOI;
                HoSo.TRANG_THAI_DUYET_HO_SO = hoSo.TRANG_THAI_DUYET_HO_SO;
                HoSo.TRANG_THAI_NHAP_HOC = hoSo.TRANG_THAI_NHAP_HOC;
                await dbContext.SaveChangeAsync();
            }

            return HoSo;
        }

        public async Task<HO_SO> GetHoSoByIdAsync(int id)
        {
            return await dbContext.HO_SO.Where(c => c.ID == id && !c.IS_DELETED && c.IS_ACTIVE)
                                           .FirstOrDefaultAsync();
        }
        public async Task<PagedList<HO_SO>> GetPagedHoSoAsync(int trang_thai_duyet_ho_so, bool trang_thai_nhap_hoc, int nam_xet_tuyen, string keyword, int pageNumber, int pageSize)
        {
            var pagedList = new PagedList<HO_SO>();

            var query = dbContext.HO_SO
                .Where(x => (string.IsNullOrEmpty(keyword) || x.MA_HO_SO.ToUpper().Contains(keyword) || x.TEN.ToUpper().Contains(keyword)) &&
                    (
                        //Lấy những hồ sơ chưa duyệt
                        (trang_thai_duyet_ho_so == 0 && x.TRANG_THAI_DUYET_HO_SO == 0) ||
                        //Đã duyệt lấy tất
                        (trang_thai_duyet_ho_so == 3 && x.TRANG_THAI_DUYET_HO_SO != 0) ||
                        (trang_thai_duyet_ho_so == 4)
                    ) &&
                    (nam_xet_tuyen == 0 || x.NAM_XET_TUYEN == nam_xet_tuyen) &&
                    !x.IS_DELETED &&
                    x.IS_ACTIVE
                );

            var data = await query
                .PageBy(x => x.ID, pageNumber, pageSize)
                .ToListAsync();

            pagedList.Data.AddRange(data);
            pagedList.PageSize = pageSize;
            pagedList.TotalCount = await query.CountAsync();

            return pagedList;
        }
    }
}
