import React, { useState } from 'react';
import Header from '../components/shared/Header';
import Footer from '../components/shared/Footer';
import styles from './MainLayout.module.css';

const MainLayout = (props) => {
	// for bread scrum in home page
	const [homeTabName, setHomeTabName] = useState('chưa xét duyệt');

	return (
		<>
			<Header homeTabName={homeTabName} />
			<div className={styles['container']}>
				{React.cloneElement(props.children, { homeTabName, setHomeTabName })}
			</div>
			<Footer />
		</>
	);
};
export default MainLayout;
