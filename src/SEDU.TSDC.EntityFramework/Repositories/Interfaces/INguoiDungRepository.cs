﻿using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Repositories.Interfaces
{
    public interface INguoiDungRepository
    {
        Task<NGUOI_DUNG> CreateNguoiDungAsync(NGUOI_DUNG client);
        Task<int> DeleteNguoiDungAsync(int id);
        Task<NGUOI_DUNG> EditNguoiDungAsync(NGUOI_DUNG client);
        Task<NGUOI_DUNG> GetNguoiDungByIdAsync(int id);
        Task<PagedList<NGUOI_DUNG>> GetPagedNguoiDungAsync(string keyword, int pageNumber, int pageSize);
        Task<bool> KiemTraTonTaiNguoiDung(string HoTen, string SoDienThoai);
    }
}
