﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace SEDU.TSDC.EntityFramework.PostgreSql.Migrations
{
    public partial class InitDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CO_SO_GIAO_DUC",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TEN_CSGD = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    ID_CSGD_CDV = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    TEN_HUYEN = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    TEN_XA = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    CHI_TIEU = table.Column<int>(type: "integer", nullable: false),
                    ID_CHA = table.Column<int>(type: "integer", nullable: false),
                    CREATED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DELETED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    IS_DELETED = table.Column<bool>(type: "boolean", nullable: false),
                    IS_ACTIVE = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CO_SO_GIAO_DUC", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CO_SO_GIAO_DUC_CO_SO_GIAO_DUC_ID_CHA",
                        column: x => x.ID_CHA,
                        principalTable: "CO_SO_GIAO_DUC",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HO_SO",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    MA_HO_SO = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    NAM_XET_TUYEN = table.Column<int>(type: "integer", nullable: false),
                    HO_DEM = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: true),
                    TEN = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: true),
                    NGAY_SINH = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ID_CSGD_CDV = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    TRANG_THAI_DUYET_HO_SO = table.Column<int>(type: "integer", nullable: false),
                    THOI_DIEM_CAP_NHAT_CUOI = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    TRANG_THAI_NHAP_HOC = table.Column<bool>(type: "boolean", nullable: false),
                    CREATED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DELETED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    IS_DELETED = table.Column<bool>(type: "boolean", nullable: false),
                    IS_ACTIVE = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HO_SO", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "NGUOI_DUNG",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    HO_TEN = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    SO_DT = table.Column<string>(type: "character varying(12)", maxLength: 12, nullable: true),
                    MAT_KHAU = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    EMAIL = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    DVC_TOKEN = table.Column<string>(type: "text", nullable: true),
                    CREATED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DELETED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    IS_DELETED = table.Column<bool>(type: "boolean", nullable: false),
                    IS_ACTIVE = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NGUOI_DUNG", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "VAI_TRO",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    TEN_VAI_TRO = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    CREATED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DELETED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    IS_DELETED = table.Column<bool>(type: "boolean", nullable: false),
                    IS_ACTIVE = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VAI_TRO", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "BAO_CAO",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    THOI_GIAN_BAT_DAU = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    THOI_GIAN_KET_THUC = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    THOI_GIAN_TAO = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    TONG_CHI_TIEU = table.Column<string>(type: "text", nullable: true),
                    TONG_HO_SO = table.Column<int>(type: "integer", nullable: false),
                    TONG_TRUNG_TUYEN = table.Column<int>(type: "integer", nullable: false),
                    TONG_BI_LOAI = table.Column<int>(type: "integer", nullable: false),
                    BC_THUAN_LOI = table.Column<string>(type: "text", nullable: true),
                    BC_KHO_KHAN = table.Column<string>(type: "text", nullable: true),
                    BC_DE_XUAT = table.Column<string>(type: "text", nullable: true),
                    BC_NOI_DUNG_KHAC = table.Column<string>(type: "text", nullable: true),
                    ID_CSGD = table.Column<int>(type: "integer", nullable: false),
                    ID_CHU_TICH_HDTS = table.Column<string>(type: "text", nullable: true),
                    CHU_TICH_HDTSID = table.Column<int>(type: "integer", nullable: true),
                    DS_THANH_VIEN_HDTS = table.Column<string>(type: "text", nullable: true),
                    ID_NGUOI_TAO = table.Column<int>(type: "integer", nullable: false),
                    CREATED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DELETED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    IS_DELETED = table.Column<bool>(type: "boolean", nullable: false),
                    IS_ACTIVE = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BAO_CAO", x => x.ID);
                    table.ForeignKey(
                        name: "FK_BAO_CAO_CO_SO_GIAO_DUC_ID_CSGD",
                        column: x => x.ID_CSGD,
                        principalTable: "CO_SO_GIAO_DUC",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BAO_CAO_NGUOI_DUNG_CHU_TICH_HDTSID",
                        column: x => x.CHU_TICH_HDTSID,
                        principalTable: "NGUOI_DUNG",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BAO_CAO_NGUOI_DUNG_ID_NGUOI_TAO",
                        column: x => x.ID_NGUOI_TAO,
                        principalTable: "NGUOI_DUNG",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "XET_DUYET_HO_SO",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    THOI_GIAN_DUYET = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    IP_DUYET = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    KET_QUA = table.Column<int>(type: "integer", nullable: false),
                    GHI_CHU = table.Column<string>(type: "text", nullable: true),
                    ID_HO_SO = table.Column<int>(type: "integer", nullable: false),
                    ID_NGUOI_DUYET = table.Column<int>(type: "integer", nullable: false),
                    CREATED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DELETED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    IS_DELETED = table.Column<bool>(type: "boolean", nullable: false),
                    IS_ACTIVE = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_XET_DUYET_HO_SO", x => x.ID);
                    table.ForeignKey(
                        name: "FK_XET_DUYET_HO_SO_HO_SO_ID_HO_SO",
                        column: x => x.ID_HO_SO,
                        principalTable: "HO_SO",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_XET_DUYET_HO_SO_NGUOI_DUNG_ID_NGUOI_DUYET",
                        column: x => x.ID_NGUOI_DUYET,
                        principalTable: "NGUOI_DUNG",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HOI_DONG_TUYEN_SINH",
                columns: table => new
                {
                    ID = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ID_NGUOI_DUNG = table.Column<int>(type: "integer", nullable: false),
                    ID_CSGD = table.Column<int>(type: "integer", nullable: false),
                    NAM_TUYEN_SINH = table.Column<int>(type: "integer", nullable: false),
                    ID_VAI_TRO = table.Column<int>(type: "integer", nullable: false),
                    CREATED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UPDATED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    DELETED_DATE = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    IS_DELETED = table.Column<bool>(type: "boolean", nullable: false),
                    IS_ACTIVE = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HOI_DONG_TUYEN_SINH", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HOI_DONG_TUYEN_SINH_CO_SO_GIAO_DUC_ID_CSGD",
                        column: x => x.ID_CSGD,
                        principalTable: "CO_SO_GIAO_DUC",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HOI_DONG_TUYEN_SINH_NGUOI_DUNG_ID_NGUOI_DUNG",
                        column: x => x.ID_NGUOI_DUNG,
                        principalTable: "NGUOI_DUNG",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HOI_DONG_TUYEN_SINH_VAI_TRO_ID_VAI_TRO",
                        column: x => x.ID_VAI_TRO,
                        principalTable: "VAI_TRO",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BAO_CAO_CHU_TICH_HDTSID",
                table: "BAO_CAO",
                column: "CHU_TICH_HDTSID");

            migrationBuilder.CreateIndex(
                name: "IX_BAO_CAO_ID_CHU_TICH_HDTS",
                table: "BAO_CAO",
                column: "ID_CHU_TICH_HDTS");

            migrationBuilder.CreateIndex(
                name: "IX_BAO_CAO_ID_CSGD",
                table: "BAO_CAO",
                column: "ID_CSGD");

            migrationBuilder.CreateIndex(
                name: "IX_BAO_CAO_ID_NGUOI_TAO",
                table: "BAO_CAO",
                column: "ID_NGUOI_TAO");

            migrationBuilder.CreateIndex(
                name: "IX_CO_SO_GIAO_DUC_ID_CHA",
                table: "CO_SO_GIAO_DUC",
                column: "ID_CHA");

            migrationBuilder.CreateIndex(
                name: "IX_HOI_DONG_TUYEN_SINH_ID_CSGD",
                table: "HOI_DONG_TUYEN_SINH",
                column: "ID_CSGD");

            migrationBuilder.CreateIndex(
                name: "IX_HOI_DONG_TUYEN_SINH_ID_NGUOI_DUNG",
                table: "HOI_DONG_TUYEN_SINH",
                column: "ID_NGUOI_DUNG");

            migrationBuilder.CreateIndex(
                name: "IX_HOI_DONG_TUYEN_SINH_ID_VAI_TRO",
                table: "HOI_DONG_TUYEN_SINH",
                column: "ID_VAI_TRO");

            migrationBuilder.CreateIndex(
                name: "IX_NGUOI_DUNG_EMAIL",
                table: "NGUOI_DUNG",
                column: "EMAIL");

            migrationBuilder.CreateIndex(
                name: "IX_XET_DUYET_HO_SO_ID_HO_SO",
                table: "XET_DUYET_HO_SO",
                column: "ID_HO_SO");

            migrationBuilder.CreateIndex(
                name: "IX_XET_DUYET_HO_SO_ID_NGUOI_DUYET",
                table: "XET_DUYET_HO_SO",
                column: "ID_NGUOI_DUYET");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BAO_CAO");

            migrationBuilder.DropTable(
                name: "HOI_DONG_TUYEN_SINH");

            migrationBuilder.DropTable(
                name: "XET_DUYET_HO_SO");

            migrationBuilder.DropTable(
                name: "CO_SO_GIAO_DUC");

            migrationBuilder.DropTable(
                name: "VAI_TRO");

            migrationBuilder.DropTable(
                name: "HO_SO");

            migrationBuilder.DropTable(
                name: "NGUOI_DUNG");
        }
    }
}
