﻿using AutoMapper;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.Services.Mappers
{
    public class VaiTroDtoMapperProfile : Profile
    {
        public VaiTroDtoMapperProfile()
        {
            SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
            DestinationMemberNamingConvention = new PascalCaseNamingConvention();
            CreateMap<VAI_TRO, VaiTroDto>(MemberList.Destination);
            CreateMap<PagedList<VAI_TRO>, PagedList<VaiTroDto>>(MemberList.Destination).ReverseMap();
        }
    }
    public class VaiTroEntityMapperProfile : Profile
    {
        public VaiTroEntityMapperProfile()
        {
            SourceMemberNamingConvention = new PascalCaseNamingConvention();
            DestinationMemberNamingConvention = new LowerUnderscoreNamingConvention();
            CreateMap<VaiTroDto, VAI_TRO>(MemberList.Destination);
        }
    }
}
