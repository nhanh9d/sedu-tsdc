﻿using AutoMapper;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System;

namespace SEDU.TSDC.Services.Mappers
{
    public class BaoCaoDtoMapperProfile : Profile
    {
        public BaoCaoDtoMapperProfile()
        {
            SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
            DestinationMemberNamingConvention = new PascalCaseNamingConvention();
            CreateMap<BAO_CAO, BaoCaoDto>(MemberList.Destination)
                .ForMember(dto => dto.IDChuTichHDTS, opt => opt.MapFrom(e => e.ID_CHU_TICH_HDTS))
                .ForMember(dto => dto.IDCoSoGiaoDuc, opt => opt.MapFrom(e => e.ID_CSGD))
                .ForMember(dto => dto.IDNguoiTao, opt => opt.MapFrom(e => e.ID_NGUOI_TAO));
            CreateMap<PagedList<BAO_CAO>, PagedList<BaoCaoDto>>(MemberList.Destination).ReverseMap();
        }
    }
    public class BaoCaoEntityMapperProfile : Profile
    {
        public BaoCaoEntityMapperProfile()
        {
            SourceMemberNamingConvention = new PascalCaseNamingConvention();
            DestinationMemberNamingConvention = new LowerUnderscoreNamingConvention();
            CreateMap<BaoCaoDto, BAO_CAO>(MemberList.Destination)
                .ForMember(e => e.ID_CHU_TICH_HDTS, opt => opt.MapFrom(dto => dto.IDChuTichHDTS))
                .ForMember(e => e.ID_CSGD, opt => opt.MapFrom(dto => dto.IDCoSoGiaoDuc))
                .ForMember(e => e.ID_NGUOI_TAO, opt => opt.MapFrom(dto => dto.IDNguoiTao));
        }
    }
}
