﻿using AutoMapper;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.Services.Mappers
{
    public static class CoSoGiaoDucMapper
    {
        internal static IMapper DtoMapper;
        internal static IMapper EntityMapper;

        static CoSoGiaoDucMapper()
        {
            DtoMapper = new MapperConfiguration(config => config.AddProfile<CoSoGiaoDucDtoMapperProfile>()).CreateMapper();
            EntityMapper = new MapperConfiguration(config => config.AddProfile<CoSoGiaoDucEntityMapperProfile>()).CreateMapper();
        }

        //public static List<Activity> ToModel(this List<APP_ACTIVITY> _APP_ACTIVITY)
        //{
        //    return Mapper.Map<List<Activity>>(_APP_ACTIVITY);
        //}

        //public static Activity ToModel(this APP_ACTIVITY _APP_ACTIVITY)
        //{
        //    return Mapper.Map<Activity>(_APP_ACTIVITY);
        //}

        public static CO_SO_GIAO_DUC ToEntity(this CoSoGiaoDucDto coSoGiaoDucDto)
        {
            return EntityMapper.Map<CO_SO_GIAO_DUC>(coSoGiaoDucDto);
        }

        public static CoSoGiaoDucDto ToDto(this CO_SO_GIAO_DUC coSoGiaoDuc)
        {
            return DtoMapper.Map<CoSoGiaoDucDto>(coSoGiaoDuc);
        }

        public static PagedList<CoSoGiaoDucDto> ToDto(this PagedList<CO_SO_GIAO_DUC> coSoGiaoDuc)
        {
            return DtoMapper.Map<PagedList<CoSoGiaoDucDto>>(coSoGiaoDuc);
        }
    }
}
