﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Entities
{
    public class CO_SO_GIAO_DUC : BASE_ENTITY
    {
        [MaxLength(100, ErrorMessage = "Tên cơ sở giáo dục tối đa 100 kí tự")]
        public string TEN_CSGD { get; set; }
        [MaxLength(50, ErrorMessage = "ID cơ sở giáo dục tối đa 50 kí tự")]
        public string ID_CSGD_CDV { get; set; }
        [MaxLength(100, ErrorMessage = "Tên huyện tối đa 100 kí tự")]
        public string TEN_HUYEN { get; set; }
        [MaxLength(100, ErrorMessage = "Tên xã tối đa 100 kí tự")]
        public string TEN_XA { get; set; }
        public int CHI_TIEU { get; set; }
        public int ID_CHA { get; set; }

        #region Relation props
        public virtual CO_SO_GIAO_DUC CSGD_CHU_QUAN { get; set; }
        /// <summary>
        /// Người dùng trong hội đồng tuyển sinh có cơ sở giáo dục này
        /// </summary>
        public virtual ICollection<CO_SO_GIAO_DUC> CSGD_TRUC_THUOC { get; set; }
        /// <summary>
        /// Người dùng trong hội đồng tuyển sinh có cơ sở giáo dục này
        /// </summary>
        public virtual ICollection<HOI_DONG_TUYEN_SINH> CSGD_THUOC_HDTS { get; set; }
        /// <summary>
        /// Người dùng trong hội đồng tuyển sinh có cơ sở giáo dục này
        /// </summary>
        public virtual ICollection<BAO_CAO> CSGD_TRONG_BAO_CAO { get; set; }

        #endregion
    }
}
