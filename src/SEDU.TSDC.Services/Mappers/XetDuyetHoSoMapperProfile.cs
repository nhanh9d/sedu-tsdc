﻿using AutoMapper;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.Services.Mappers
{
    public class XetDuyetHoSoDtoMapperProfile : Profile
    {
        public XetDuyetHoSoDtoMapperProfile()
        {
            SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
            DestinationMemberNamingConvention = new PascalCaseNamingConvention();
            CreateMap<XET_DUYET_HO_SO, XetDuyetHoSoDto>(MemberList.Destination)
                .ForMember(dto => dto.IdHoSo, opt => opt.MapFrom(e => e.ID_HO_SO))
                .ForMember(dto => dto.IpDuyet, opt => opt.MapFrom(e => e.IP_DUYET == null ? string.Empty : e.IP_DUYET))
                .ForMember(dto => dto.IdNguoiDuyet, opt => opt.MapFrom(e => e.ID_NGUOI_DUYET))
                .ForMember(dto => dto.GhiChu, opt => opt.MapFrom(e => e.GHI_CHU == null ? string.Empty : e.GHI_CHU))
                .ForMember(dto => dto.KetQua, opt => opt.MapFrom(e => e.KET_QUA))
                .ForMember(dto => dto.HoSo, opt => opt.MapFrom(e => e.HO_SO.ToDto()));
            CreateMap<PagedList<XET_DUYET_HO_SO>, PagedList<XetDuyetHoSoDto>>(MemberList.Destination).ReverseMap();
        }
    }
    public class XetDuyetHoSoEntityMapperProfile : Profile
    {
        public XetDuyetHoSoEntityMapperProfile()
        {
            SourceMemberNamingConvention = new PascalCaseNamingConvention();
            DestinationMemberNamingConvention = new LowerUnderscoreNamingConvention();
            CreateMap<XetDuyetHoSoDto, XET_DUYET_HO_SO>(MemberList.Destination)
                .ForMember(e => e.ID_HO_SO, opt => opt.MapFrom(dto => dto.IdHoSo))
                .ForMember(e => e.IP_DUYET, opt => opt.MapFrom(dto => dto.IpDuyet))
                .ForMember(e => e.ID_NGUOI_DUYET, opt => opt.MapFrom(dto => dto.IdNguoiDuyet))
                .ForMember(e => e.GHI_CHU, opt => opt.MapFrom(dto => dto.GhiChu))
                .ForMember(e => e.KET_QUA, opt => opt.MapFrom(dto => dto.KetQua));
        }
    }
}
