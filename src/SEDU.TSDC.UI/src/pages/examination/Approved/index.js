import React, { useState, useEffect } from "react";
import DataTableCustom from "../../../components/DataTables/DataTableCustom";
import moment from "moment";
import { IP_API, options_ticket } from "./../../../helper/helper";
import axios from "axios";

// import for Loading effect
import LoadingEffect from "../../../components/LoadingEffect";

// param for calling api

const columns = [
  { id: "STT", label: "STT", width: 20, align: "left" },
  { id: "Mã hồ sơ", label: "Mã hồ sơ", width: 130, align: "left" },
  {
    id: "Họ đệm",
    label: "Họ đệm",
    width: 130,
    align: "left",
  },
  {
    id: "Tên",
    label: "Tên",
    width: 130,
    align: "left",
  },
  {
    id: "Ngày sinh",
    label: "Ngày sinh",
    width: 130,
    align: "left",
  },
  {
    id: "Trạng thái",
    label: "Trạng thái",
    width: 130,
    align: "left",
  },
];

const Approved = ({ listData }) => {
  // const [columns, setColumns] = useState([]);
  const [rows, setRows] = useState([]);
  const [loading, isLoading] = useState(false);
  const [listApproved, setlistApproved] = useState([]);
  const [totalRecords, setTotalRecords] = useState(0);

  // context
  // const context = useContext(FetchDataContext)

  // function to call api
  const fetchApproved = (searchKey = "", pageNumber = 1, pageSize = 5) => {
    const params = {
      trang_thai_nhap_hoc: true,
      trang_thai_duyet_ho_so: "3",
      nam_xet_tuyen: 2021,
      keyword: searchKey,
      pageNumber: pageNumber,
      pageSize: pageSize,
    };
    const fetchData = async () => {
      isLoading(true);
      await axios
        .post(IP_API + "/HoSo/get-paged-ho-so", params, options_ticket)
        .then((response) => {
          // call API thanh cong
          // setListDataReal
          if (response && response.data) {
            setTotalRecords(response.data.totalCount);
            setlistApproved(response.data.data);
          }
        })
        .catch((error) => {
          console.log("unauthorize");
          if (error.response.status === 401) {
            window.location.href =
              "https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback";
          }
        });
      isLoading(false);
    };
    fetchData();
  };

  // call api to get data
  useEffect(() => {
    fetchApproved();
  }, []);

  // get rows
  useEffect(() => {
    let renderedItems;
    if (listApproved) {
      renderedItems = listApproved.map((item) => {
        let { maHoSo, hoDem, ten, ngaySinh, trangThaiDuyetHoSo, id } = item;
        ngaySinh = moment(ngaySinh).format("DD/MM/YYYY");
        let trangThaiDuyetHoSo_Text =
          trangThaiDuyetHoSo === 2 ? "Đồng ý" : "Từ chối";
        return {
          maHoSo,
          hoDem,
          ten,
          ngaySinh,
          trangThaiDuyetHoSo,
          trangThaiDuyetHoSo_Text,
          id,
        };
      });
      setRows(renderedItems);
    }
  }, [listApproved]);

  return (
    <>
    
      <LoadingEffect loading={loading} />
      <DataTableCustom
        columns={columns}
        openTooltip={false}
        rows={rows}
        searchBar={true}
        status="3"
        handleAdmitAndReject={fetchApproved}
        handleSearch={fetchApproved}
        totalRecords={totalRecords}
      />
    </>
  );
};

export default Approved;
