﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SEDU.TSDC.API.Mappers;
using SEDU.TSDC.API.Parameters;
using SEDU.TSDC.API.Parameters.CoSoGiaoDuc;
using SEDU.TSDC.Services.Dtos.Tsdc;
using SEDU.TSDC.Services.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors("SeduCors")]
    public class CoSoGiaoDucController : ControllerBase
    {

        private readonly ILogger<CoSoGiaoDucController> _logger;
        private readonly ICoSoGiaoDucService _coSoGiaoDucService;

        public CoSoGiaoDucController(ILogger<CoSoGiaoDucController> logger, ICoSoGiaoDucService coSoGiaoDucService)
        {
            _logger = logger;
            _coSoGiaoDucService = coSoGiaoDucService;
        }

        [Route("healthcheck")]
        [HttpGet]
        public string HealthCheck()
        {
            return "Ok";
        }

        [Route("add-co-so-giao-duc")]
        [HttpPost]
        public async Task<CoSoGiaoDucDto> AddCoSoGiaoDuc(CreateCoSoGiaoDucParams pr)
        {
            var dto = pr.ToDto();
            return await _coSoGiaoDucService.AddCoSoGiaoDucAsync(dto);
        }

        [Route("get-co-so-giao-duc-by-id")]
        [HttpPost]
        public async Task<object> GetCoSoGiaoDucById(int coSoGiaoDucId)
        {
            try
            {
                return await _coSoGiaoDucService.GetCoSoGiaoDucByIdAsync(coSoGiaoDucId);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [Route("edit-co-so-giao-duc")]
        [HttpPost]
        public async Task<CoSoGiaoDucDto> EditCoSoGiaoDuc(EditCoSoGiaoDucParams pr)
        {
            var dto = pr.ToDto();
            return await _coSoGiaoDucService.EditCoSoGiaoDucAsync(dto);
        }

        [Route("delete-co-so-giao-duc")]
        [HttpPost]
        public async Task<int> DeleteCoSoGiaoDuc(int coSoGiaoDucId)
        {
            return await _coSoGiaoDucService.DeleteCoSoGiaoDucAsync(coSoGiaoDucId);
        }
        [Route("get-paged-co-so-giao-duc")]
        [HttpPost]
        public async Task<object> GetPagedCoSoGiaoDuc(PagedParams pr)
        {
            var lst = await _coSoGiaoDucService.GetPagedCoSoGiaoDucAsync(pr.keyword, pr.pageNumber, pr.pageSize);
            return lst;
        }
    }
}
