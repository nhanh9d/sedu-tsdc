﻿using Microsoft.EntityFrameworkCore;
using SEDU.TSDC.EntityFramework.DbContexts.Interfaces;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.EntityFramework.Extensions.Extensions;
using SEDU.TSDC.EntityFramework.Repositories.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Repositories
{
    public class NguoiDungRepository<TDbContext> : INguoiDungRepository
        where TDbContext : ITsdcDbContext
    {
        protected readonly TDbContext dbContext;

        public NguoiDungRepository(TDbContext _dbContext)
        {
            dbContext = _dbContext;
        }

        public async Task<NGUOI_DUNG> CreateNguoiDungAsync(NGUOI_DUNG nguoiDung)
        {
            await dbContext.NGUOI_DUNG.AddAsync(nguoiDung);
            await dbContext.SaveChangeAsync();

            return nguoiDung;
        }

        public async Task<int> DeleteNguoiDungAsync(int id)
        {
            var NguoiDung = await dbContext.NGUOI_DUNG.FindAsync(id);

            if (NguoiDung != null)
            {
                NguoiDung.IS_DELETED = true;
                NguoiDung.DELETED_DATE = DateTime.Now;

                await dbContext.SaveChangeAsync();
            }

            return id;
        }

        public async Task<NGUOI_DUNG> EditNguoiDungAsync(NGUOI_DUNG nguoiDung)
        {
            var NguoiDung = await dbContext.NGUOI_DUNG.FindAsync(nguoiDung.ID);

            if (NguoiDung != null)
            {
                NguoiDung.DVC_TOKEN = nguoiDung.DVC_TOKEN;
                NguoiDung.UPDATED_DATE = DateTime.Now;
                NguoiDung.IS_ACTIVE = nguoiDung.IS_ACTIVE;
                NguoiDung.EMAIL = nguoiDung.EMAIL;
                NguoiDung.HO_TEN = nguoiDung.HO_TEN;
                NguoiDung.MAT_KHAU = nguoiDung.MAT_KHAU;
                NguoiDung.SO_DT = nguoiDung.SO_DT;
                await dbContext.SaveChangeAsync();
            }

            return NguoiDung;
        }

        public async Task<NGUOI_DUNG> GetNguoiDungByIdAsync(int id)
        {
            return await dbContext.NGUOI_DUNG.Where(c => c.ID == id && !c.IS_DELETED && c.IS_ACTIVE)
                                           .FirstOrDefaultAsync();
        }
        public async Task<PagedList<NGUOI_DUNG>> GetPagedNguoiDungAsync(string keyword, int pageNumber, int pageSize)
        {
            var pagedList = new PagedList<NGUOI_DUNG>();
            var querry = dbContext.NGUOI_DUNG
                .Where(x => !x.IS_DELETED && x.IS_ACTIVE && x.HO_TEN.Contains(keyword));
            var data = await querry
                .PageBy(x => x.ID, pageNumber, pageSize)
                .ToListAsync();

            pagedList.Data.AddRange(data);
            pagedList.PageSize = pageSize;
            pagedList.TotalCount = await querry.CountAsync();

            return pagedList;
        }

        public async Task<bool> KiemTraTonTaiNguoiDung(string HoTen, string SoDienThoai)
        {
            return await dbContext.NGUOI_DUNG.Where(nd => nd.HO_TEN.ToUpper() == HoTen && nd.SO_DT.ToUpper() == SoDienThoai).AnyAsync();
        }
    }
}
