import React, { useState, useEffect } from "react";
import DataTableCustom from "../../../components/DataTables/DataTableCustom";
import moment from "moment";
import { IP_API, options_ticket } from "./../../../helper/helper";
import axios from "axios";

// import for Loading effect
import LoadingEffect from "../../../components/LoadingEffect";

// param for calling api

const columns = [
  { id: "STT", label: "STT", width: 20, align: "left" },
  { id: "Mã hồ sơ", label: "Mã hồ sơ", width: 130, align: "left" },
  {
    id: "Họ đệm",
    label: "Họ đệm",
    width: 130,
    align: "left",
  },
  {
    id: "Tên",
    label: "Tên",
    width: 130,
    align: "left",
  },
  {
    id: "Ngày sinh",
    label: "Ngày sinh",
    width: 130,
    align: "left",
  },
  {
    id: "Trạng thái",
    label: "Trạng thái",
    width: 130,
    align: "left",
  },
];

const AllSubmitted = (props) => {
  const [rows, setRows] = useState([]);
  const [loading, isLoading] = useState(false);
  const [listData, setListData] = useState([]);
  const [totalRecords, setTotalRecords] = useState(0);

  // context
  // const context = useContext(FetchDataContext)

  // function to call api
  const fetchAllData = (searchKey = "", pageNumber = 1, pageSize = 5) => {
    const params = {
      trang_thai_nhap_hoc: true,
      trang_thai_duyet_ho_so: "4",
      nam_xet_tuyen: 2021,
      keyword: searchKey,
      pageNumber: pageNumber,
      pageSize: pageSize,
    };
    const fetchData = async () => {
      isLoading(true);
      await axios
        .post(IP_API + "/HoSo/get-paged-ho-so", params, options_ticket)
        .then((response) => {
          // call API thanh cong
          // setListDataReal
          if (response && response.data) {
            setTotalRecords(response.data.totalCount);
            setListData(response.data.data);
          }
        })
        .catch((error) => {
          console.log("unauthorize");
          if (error.response.status === 401) {
            window.location.href =
              "https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback";
          }
        });
      isLoading(false);
    };
    fetchData();
  };

  // call api to get data
  useEffect(() => {
    fetchAllData();
  }, []);

  // get rows
  useEffect(() => {
    let renderedItems;
    if (listData) {
      renderedItems = listData.map((item) => {
        let { maHoSo, hoDem, ten, ngaySinh, trangThaiDuyetHoSo, id } = item;
        ngaySinh = moment(ngaySinh).format("DD/MM/YYYY");
        let trangThaiDuyetHoSo_Text =
          trangThaiDuyetHoSo === 0
            ? "Xét duyệt"
            : trangThaiDuyetHoSo === 1
            ? "Đồng ý"
            : "Từ chối";
        return {
          maHoSo,
          hoDem,
          ten,
          ngaySinh,
          trangThaiDuyetHoSo,
          trangThaiDuyetHoSo_Text,
          id,
        };
      });
      setRows(renderedItems);
    }
  }, [listData]);

  return (
    <>
      <LoadingEffect loading={loading} />
      <DataTableCustom
        columns={columns}
        rows={rows}
        openTooltip={false}
        searchBar={true}
        handleAdmitAndReject={fetchAllData}
        handleSearch={fetchAllData}
        totalRecords={totalRecords}
      />
    </>
  );
};

export default AllSubmitted;
