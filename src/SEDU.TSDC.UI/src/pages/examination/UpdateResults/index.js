import { Box, Button } from '@material-ui/core'
import React, { useState, useEffect } from 'react'
import DataTableCustom from '../../../components/DataTables/DataTableCustom'

const columns = [
  { id: 'STT', label: 'STT', width: 20, align: 'left' },
  { id: 'Mã hồ sơ', label: 'Mã hồ sơ', width: 200, align: 'left' },
  {
    id: 'Họ Tên',
    label: 'Họ Tên',
    width: 200,
    align: 'left',
  },
  {
    id: 'Ngày sinh',
    label: 'Ngày sinh',
    width: 200,
    align: 'left',
  },
  {
    id: 'Lần Cập Nhật Cuối Cùng',
    label: 'Lần Cập Nhật Cuối Cùng',
    width: 300,
    align: 'left',
  },
  {
    id: 'Trạng Thái Cập Nhật',
    label: 'Trạng Thái Cập Nhật',
    width: 300,
    align: 'left',
  },
  {
    id: 'Kết Quả Tuyển Sinh',
    label: 'Kết Quả Tuyển Sinh',
    width: 300,
    align: 'left',
  },
]
const UpdateResult = ({ listData }) => {
  const [rows, setRows] = useState([])
  const handleUpdate = () => {
    console.log('clicked')
  }
  // get rows
  useEffect(() => {
    let renderedItems = [...listData]
    let newArray
    if (listData) {
      newArray = renderedItems.map((item) => {
        item.fullName = item.firstName + ' ' + item.lastName
        if (item.isUpdate === true) {
          item.isUpdate = 'Thành Công'
        } else if (item.isUpdate === false) {
          item.isUpdate = 'Thất Bại'
        }
        if (item.isAccept === true) {
          item.isAccept = 'Được Chấp Nhận'
        } else if (item.isAccept === false) {
          item.isAccept = 'Bị Từ Chối'
        }
        let {
          submitID,
          fullName,
          birthDay,
          lastUpdate,
          isUpdate,
          isAccept,
          id,
        } = item
        return {
          submitID,
          fullName,
          birthDay,
          lastUpdate,
          isUpdate,
          isAccept,
          id,
        }
      })
      setRows(newArray)
    }
  }, [listData])
  return (
    <>
      <Box mb={3}>
        <Button onClick={handleUpdate} variant="contained" color="primary">
          Thực Hiện Cập Nhật
        </Button>
      </Box>
      <DataTableCustom columns={columns} rows={rows} />
      <Box mt={3}>
        <input id="autoUpdate" type="checkbox" />
        <label htmlFor="autoUpdate">Tự động cập nhật các hồ sơ thất bại</label>
      </Box>
    </>
  )
}

export default UpdateResult
