﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SEDU.TSDC.EntityFramework.Configuration.Configuration;
using System;
using System.Reflection;

namespace SEDU.TSDC.EntityFramework.Configuration.PostgreSql
{
    public static class DatabaseExtensions
    {
        public static void RegisterPostgreSqlDbContexts<TDbContext>(this IServiceCollection services,
            ConnectionStringsConfiguration connectionStrings,
            DatabaseMigrationsConfiguration databaseMigrations)
            where TDbContext : DbContext
        {
            var migrationsAssembly = typeof(DatabaseExtensions).GetTypeInfo().Assembly.GetName().Name;
            // Audit logging connection
            services.AddDbContext<TDbContext>(options => options.UseNpgsql(connectionStrings.TsdcDbConnection,
                optionsSql =>
                {
                    optionsSql.MigrationsAssembly(databaseMigrations.TsdcDbMigrationsAssembly ?? migrationsAssembly);
                    optionsSql.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                }));
        }
    }
}
