import React, { useState, useContext, useEffect } from "react";
import styles from "./DataTableCustom.module.css";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import ToolTipsCustom from "./ToolTipsCustom";
import CustomTablePagination from "./CustomTablePagination";
import { Box } from "@material-ui/core";
import FetchDataContext from "../../store/fetchData-context";

// Make Custom Reuseable Classes
const useStyles = makeStyles({
  table: {
    minWidth: 650,
    width: "100%",
  },
  /* container: {
    maxHeight: 200,
  }, */
});

// ********************************
// LOGIC

const DataTableCustom = ({
  columns,
  rows,
  openTooltip,
  searchBar,
  status,
  handleAdmitAndReject,
  handleSearch,
  totalRecords,
}) => {
  // const context = useContext(FetchDataContext);

  const classes = useStyles();
  const [searched, setSearched] = useState("");

  const onInputChange = (e) => {
    e.preventDefault();
    setSearched(e.target.value);
    // context.handleSearchKeyWord(e.target.value);
  };

  const requestSearch = (e) => {
    e.preventDefault();
    handleSearch(searched);
  };

  // Pagination
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  // const emptyRows =
  // 	rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);




  const handleChangeRowsPerPage = (event) => {
    handleSearch("", page, event.target.value);
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleChangePage = (event, newPage) => {
    handleSearch("", newPage +1, rowsPerPage);
    setPage(newPage);
  
  };

  // rendered columns
  const renderedColumns = columns ? (
    columns.map((column, index) => (
      <TableCell
        key={index}
        align={column.align}
        style={{ width: column.width }}
      >
        {column.label}
      </TableCell>
    ))
  ) : (
    <></>
  );
  //handle Modal

  // render rows
  const renderedRows = (rows && rows !== 0) ? (
    // (rowsPerPage > 0
    //   ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
    //   : rows
    // )
    rows.map((row, index) => {
      return (
        <ToolTipsCustom
          key={index}
          row={row}
          openTooltip={openTooltip}
          handleAdmitAndReject={handleAdmitAndReject}
        >
          <TableRow>
            <TableCell align="left">{index + 1}</TableCell>
            {Object &&
              Object.keys(row)
                .filter((i) => i !== "id" && i !== "trangThaiDuyetHoSo")
                .map((key, i) => {
                  return (
                    <TableCell key={i * Math.random() * 1000} align="left">
                      {row[key]}
                    </TableCell>
                  );
                })}
          </TableRow>
        </ToolTipsCustom>
      );
    })
  ) : (
    <></>
  );

  return (
    <Box>
      <div className={styles["box-wrapper"]}>
        <div className={`${styles["searchbox"]} ${!searchBar ? "d-none" : ""}`}>
          <i className={`fas fa-search ${styles["searchbox__icon"]}`}></i>
          <input
            className={`${styles["searchbox__input"]}`}
            value={searched}
            onInput={onInputChange}
            // ref={inputRef}
            placeholder="Nhập mã hồ sơ hoặc họ tên"
          // ref={inputRef}
          />
          <button
            type="button"
            className={`btn btn-primary ${styles["searchbox__button"]}`}
            onClick={requestSearch}
          >
            Tìm kiếm
          </button>
        </div>

        <TableContainer className={classes.container}>
          <Table
            stickyHeader
            className={classes.table}
            aria-label="simple table"
          >
            <TableHead>
              <TableRow>{renderedColumns}</TableRow>
            </TableHead>
            <TableBody>{renderedRows}</TableBody>
          </Table>
        </TableContainer>
        <CustomTablePagination
          data={rows}
          page={page}
          rowsPerPage={rowsPerPage}
          handleChangePage={handleChangePage}
          handleChangeRowsPerPage={handleChangeRowsPerPage}
          totalRecords={Math.ceil(totalRecords)}
        />
      </div>
    </Box>
  );
};

export default DataTableCustom;
