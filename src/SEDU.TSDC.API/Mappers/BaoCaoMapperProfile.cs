﻿using AutoMapper;
using SEDU.TSDC.API.Parameters.BaoCao;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System;

namespace SEDU.TSDC.API.Mappers
{
    public class BaoCaoMapperProfile : Profile
    {
        public BaoCaoMapperProfile()
        {
            CreateMap<CreateBaoCaoParams, BaoCaoDto>(MemberList.Destination);
            CreateMap<EditBaoCaoParams, BaoCaoDto>(MemberList.Destination);
        }
    }
}
