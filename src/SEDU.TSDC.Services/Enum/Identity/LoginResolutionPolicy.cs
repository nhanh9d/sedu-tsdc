﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Enum.Identity
{
    public enum LoginResolutionPolicy
    {
        Username = 0,
        Email = 1
    }
}
