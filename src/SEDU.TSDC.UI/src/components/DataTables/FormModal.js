import React, { useState } from "react";
import { Form, Field, Formik } from "formik";
import * as Yup from "yup";
import {
  Button,
  CircularProgress,
  Grid,
  TextField,
  Box,
  Typography,
} from "@material-ui/core";
import ModalCustom from "./ModalCustom";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "../Alert/AlertCustom";
const validationSchema = Yup.object().shape({
  reasonText: Yup.string()
    .required("Cần Điền Lý Do Từ Chối")
    .min(3, "Lý Do Từ Chối Phải từ 3 kí tự trở lên!"),
});

const FormModal = (props) => {
  console.log(props);
  const [reasonEntered, setReasonEntered] = useState("");
  const [notifySetting, setNotifySetting] = useState({
    isSuccess: true,
    message: "",
  });
  const [isOpen, SetisOpen] = useState(false);
  const [isOpenModal, setIsOpenModal] = useState(true);

  const handleClose = () => {
    SetisOpen(false);
  };
  // const handleSubmitReason = async () => {
  //     //call API
  //     await SetisOpen(true);
  //     await setTimeout(() => {
  //         setIsOpenModal(false)
  //     }, 2000);
  // }
  const handleSubmitReason = (values) => {
    //call API
    debugger;
    props.item.ghiChu = values.reasonText;
    props.saveData(props.item, 1);
    
    return true;
  };
  return (
    <>
      {isOpenModal && (
        <ModalCustom handleCloseModal={props.handleCloseModal}>
          <Formik
            //gia tri khoi tao
            initialValues={{
              reasonText: reasonEntered,
            }}
            validationSchema={validationSchema}
            //khi submit
            onSubmit={handleSubmitReason}
          >
            {({ values, errors, isSubmitting }) => (
              <Form>
                <Grid container direction="column" spacing={3}>
                  <Grid item>
                    <Typography variant="h4">
                      {" "}
                      Lý Do Từ Chối Mã Hồ Sơ : {props.maHoSo}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Field
                      name="reasonText"
                      value={reasonEntered}
                      render={({ field }) => (
                        <TextField
                          variant="filled"
                          onInput={(e) => setReasonEntered(e.target.value)}
                          multiline
                          minRows={10}
                          style={{ width: 500 }}
                          autoFocus
                          {...field}
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12}>
                    <Typography variant="inherit" color="error">
                      {errors.reasonText}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid container spacing={3}>
                  <Grid item>
                    <Box ml={1}>
                      <Button
                        disabled={isSubmitting}
                        startIcon={
                          isSubmitting ? (
                            <CircularProgress size="1rem" />
                          ) : undefined
                        }
                        type="submit"
                        color="primary"
                        variant="contained"
                      >
                        {isSubmitting ? "Submiting..." : "Lưu"}
                      </Button>
                      <Button
                        type="button"
                        color="default"
                        variant="contained"
                        onClick={props.handleCloseModal}
                      >
                        Đóng
                      </Button>
                    </Box>
                  </Grid>
                </Grid>
                <pre>{JSON.stringify({ values, errors }, null, 4)}</pre>
              </Form>
            )}
          </Formik>
        </ModalCustom>
      )}
      <Snackbar open={isOpen} autoHideDuration={1000} onClose={handleClose}>
        <Alert
          onClose={handleClose}
          severity={notifySetting.isSuccess ? "success" : "error"}
        >
          {notifySetting.message}
        </Alert>
      </Snackbar>
    </>
  );
};

export default FormModal;
