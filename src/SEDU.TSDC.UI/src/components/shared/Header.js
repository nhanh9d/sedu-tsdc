import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import BreadCrumbsCustom from '../BreadCrumbs/BreadCrumbsCustom';
import styles from './Header.module.css';
import images from '../../assets/index';
import { Box, Typography } from '@material-ui/core';
// import BreadScrumContext from '../../store/breadscrum-context';

// fake data
const categories = [
	{
		name: 'Xét duyệt hồ sơ',
		path: '/',
	},
	{
		name: 'Hội đồng tuyển sinh',
		path: '/hoi-dong-tuyen-sinh',
	},
	{
		name: 'Quản lý các đợt tuyển sinh',
		path: '/quan-ly-cac-dot-tuyen-sinh',
	},
	{
		name: 'Báo cáo - thống kê',
		path: '/bao-cao-thong-ke',
	},
];

const Header = ({ homeTabName }) => {
	const [listData, setListData] = useState('');
	// const [option, setOption] = useState('Menu');
	const history = useHistory();
    const CSGD =localStorage.getItem('CSGD');
    const user =localStorage.getItem('user');

	const changeOption = (pathname) => {
		if (pathname === '/') {
			return 'Xét duyệt hồ sơ';
		} else if (pathname === '/hoi-dong-tuyen-sinh') {
			return 'Hội đồng tuyển sinh';
		} else if (pathname === '/quan-ly-cac-dot-tuyen-sinh') {
			return 'Quản lý các đợt tuyển sinh';
		} else {
			return 'Báo cáo - thống kê';
		}
	};

	// update listData
	useEffect(() => {
		setListData(categories);
	}, [listData]);

	// render current option
	const currentOption = (
		<span className={styles['dropdown__text']}>
			{changeOption(history.location.pathname)}
		</span>
	);

	// render list of options
	const renderedOptions = listData ? (
		listData.map((item, index) => {
			return (
				<Link to={item.path} key={index}>
					<p>{item.name}</p>
				</Link>
			);
		})
	) : (
		<></>
	);

	return (
		<>
			<header className={styles['header']}>
				<Box px={4} py={2}>
					<div className={styles['header-wrapper']}>
						<div className={styles['header-left']}>
							<Link to="/">
								<img
									src={images.logo}
									style={{ width: 40 + 'px', height: 40 + 'px' }}
									alt="logo"
								/>
							</Link>
							<div className={styles.dropdown}>
								{/* <span className={styles['dropdown__text']}>{option}</span> */}
								{currentOption}
								<span className={`${styles['arrow']} ${styles['down']}`}></span>
								<div className={styles['dropdown__content']}>
									{renderedOptions}
								</div>
							</div>
							<div>Báo cáo -Thống Kê</div>
						</div>

						<div className={styles['header-right']}>
							<div className={styles.dropdown}>
								<span className={styles['dropdown__text']}>{user}</span>
								<span className={`${styles['arrow']} ${styles['down']}`}></span>
								<div className={styles['dropdown__content']}>
									<p>Thông tin tài khoản</p>
									<p>Đăng Xuất</p>
								</div>
							</div>
						</div>
					</div>
				</Box>
			</header>
			<Box mx={4} mb={2}>
				<BreadCrumbsCustom pathname={history.location.pathname} homeTabName={homeTabName} />

				<Typography>Chào Mừng Hội Đồng Tuyển Sinh {CSGD} !</Typography>
			</Box>
		</>
	);
};

export default Header;
