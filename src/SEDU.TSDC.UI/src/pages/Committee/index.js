import {
  Box,
  Button,
  // FilledInput,
  FormControl,
  // Input,
  // InputLabel,
  OutlinedInput,
  Typography,
  FormHelperText,
} from "@material-ui/core";
import React, { useEffect, useState, useRef, useContext } from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
// import TextField from '@material-ui/core/TextField';
import DataContext from "../../store/PassData-context";

import DataTableCustom from "../../components/DataTables/DataTableCustom";
// import fakeData from "./fakeData.json";
import axios from "axios";
import { IP_API, options_ticket } from "../../helper/helper";

//set Loading
import CircularProgress from "@material-ui/core/CircularProgress";
import Backdrop from "@material-ui/core/Backdrop";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "../../components/Alert/AlertCustom";

const useStyles = makeStyles({
  root: {
    margin: "16px 0",
  },
  form: {
    padding: "12px",
  },
  submitButton: {
    marginTop: "16px",
  },
});
const useStyles2 = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
}));
const columns = [
  { id: "STT", label: "STT", width: 20, align: "left" },
  {
    id: "Họ Tên",
    label: "Họ Tên",
    width: 200,
    align: "left",
  },
  {
    id: "SĐT",
    label: "SĐT",
    width: 200,
    align: "left",
  },
  {
    id: "Vai Trò",
    label: "Vai Trò",
    width: 300,
    align: "left",
  },

  {
    id: "Actions",
    label: "Actions",
    width: 300,
    align: "left",
  },
];
const Committee = () => {
  const context = useContext(DataContext);

  // DECLARE STATE SECTION
  const [listData, setListData] = useState([]);
  const [rows, setRows] = useState([]);
  // const [name, setName] = React.useState('');
  const [isError, setIsError] = useState(false);
  const classes = useStyles();
  const classesSpin = useStyles2();

  const inputRef = useRef();
  const [loading, isLoading] = useState(false);
  const [totalRecords, setTotalRecords] = useState(0);
  //notifySetting
  const [open, setOpen] = useState(false);
  const [notifySetting, setNotifySetting] = useState({
    isSuccess: true,
    message: "",
  });
  const [resultQuickAdd, setResultQuickAdd] = useState("");

  const handleClose = () => {
    setOpen(false);
  };
  const handleFetchCommitee = (searchKey = "", pageNumber = 1, pageSize = 5) => {
    const fetchData = async () => {
      isLoading(true);

      const params = {
        keyword: searchKey,
        pageNumber: pageNumber,
        pageSize: pageSize,
      };
      const options = {
        headers: {
          Ticket: localStorage.getItem("ticket"),
        },
      };
      await axios
        .post(
          IP_API + "/HoiDongTuyenSinh/get-paged-hoi-dong-tuyen-sinh",
          params,
          options
        )
        .then((respond) => {
          setListData(respond.data.data);
          setTotalRecords(respond.data.totalCount);
        })
        .catch((error) => {
          if (error.response.status === 401) {
            window.location.href =
              "https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback";
          }
        });
      isLoading(false);
    };
    fetchData();
  }
  // METHOD SECTION
  useEffect(() => {
    //set Data
    // set params dang form data
    handleFetchCommitee();
  }, [context]);
  useEffect(() => {
    let renderedItems = [...listData];
    let newArray;
    const handleEdit = (item) => {
      context.editItem(item);
    };

    const handleFilter = (item) => {
      // Xóa data
      axios
        .post(
          IP_API +
          "/HoiDongTuyenSinh/delete-hoi-dong-tuyen-sinh?hoiDongTuyenSinhId=" +
          item.id,
          null,
          options_ticket
        )
        .then((reponse) => {
          setOpen(true);
          setNotifySetting({
            isSuccess: true,
            message: "Xóa thành công",
          });
          handleFetchCommitee()

        })
        .catch((error) => {
          setOpen(true);
          setNotifySetting({
            isSuccess: false,
            message: "Xóa thất bại",
          });
        });
    };

    if (listData) {
      newArray = renderedItems.map((item) => {
        item.actions = (
          <div>
            <button
              style={{ border: "none", background: "white", color: "#3f51b5" }}
              onClick={handleFilter.bind(null, item)}
            >
              <i className="fas fa-trash"></i>
            </button>
            <Link to="/hoi-dong-tuyen-sinh/sua-thong-tin-thanh-vien">
              <button
                style={{ border: "none", background: "white", color: "red" }}
                onClick={handleEdit.bind(null, item)}
              >
                <i className="fas fa-edit"></i>
              </button>
            </Link>
          </div>
        );

        // let { tenNguoiDung, soDienThoai, vaiTro, actions, id } = item;
        // return { tenNguoiDung, soDienThoai, vaiTro, actions, id };
        let { tenNguoiDung, soDienThoai, vaiTro, actions, id } = item;
        return { tenNguoiDung, soDienThoai, vaiTro, actions, id };
      });
      setRows(newArray);
    }
  }, [listData, context]);
  const onSubmit = (e) => {
    let arrParams = [];
    const txtQuickAddition = document.getElementById("txt-quick-addition");
    e.preventDefault();
    if (txtQuickAddition.value === "") {
      setIsError(true);
    }
    //split line theo dau enter
    let lines = txtQuickAddition.value.split("\n");
    // console.log('Lines: ', lines);
    // let index = 0;
    for (let line of lines) {
      let [HoTen, SoDienThoai, ChucVu] = line.split(", ");
      if (
        HoTen !== "" &&
        SoDienThoai !== "" &&
        ChucVu !== "" &&
        HoTen.match(
          /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ .'-]+$/
        ) &&
        SoDienThoai.match(
          /^[ ]*([+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})$/
        ) &&
        ChucVu.match(
          /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ .'-]+$/
        )
      ) {
        // qua điều kiện
        const params = {
          idCoSoGiaoDuc: 1,
          hoTen: HoTen,
          soDienThoai: SoDienThoai,
          chucVu: ChucVu,
        };
        arrParams.push(params);
      }
    }
    if (arrParams.length > 0) {
      axios
        .post(
          IP_API + "/HoiDongTuyenSinh/them-nhieu-hoi-dong-tuyen-sinh",
          arrParams,
          options_ticket
        )
        .then((reponse) => {
          // vào đây
          console.log(reponse)
          if (reponse && reponse.data) {
            if (reponse.data.length > 0) {
              let textResult = "";
              for (let i = 0;i < reponse.data.length;i++) {
                textResult += "Dòng " + (reponse.data[i].stt + 1) + ": " + reponse.data[i].message + " | ";
              }
              setResultQuickAdd(textResult);
            }
          }
          handleFetchCommitee()
        })
        .catch((error) => {
          //lỗi
          setOpen(true);
          setNotifySetting({
            isSuccess: false,
            message: "Xóa thất bại",
          });
        });
    } else {
      alert("Vui lòng kiểm tra lại thông tin nhập");
    }
  };

  // RENDER SECTION
  return (
    <>
      <Backdrop className={classesSpin.backdrop} open={loading}>
        <CircularProgress color="inherit" />
      </Backdrop>

      <Box mx={4}>
        <h5>QUẢN LÝ THÀNH VIÊN HỘI ĐỒNG TUYỂN SINH</h5>
        <div className="mb-3">Các thành viên hội đồng tuyển sinh:</div>
        <DataTableCustom
          columns={columns}
          rows={rows}
          openTooltip={false}
          totalRecords={totalRecords}
          handleSearch={handleFetchCommitee}
        />
        <Box mt={5}>
          <Typography variant="h5">Thêm nhanh thành viên</Typography>
          <Typography>
            Nhập các thành viên mới, mỗi thành viên trên một dòng, theo định
            dạng: Họ tên, số điện thoại, vai trò
          </Typography>

          <form className={classes.root} autoComplete="off" onSubmit={onSubmit}>
            <FormControl variant="outlined" fullWidth error={isError}>
              {/* <InputLabel hiddenLabel htmlFor="component-input">
								Outline Label
							</InputLabel> */}
              <OutlinedInput
                id="txt-quick-addition"
                aria-describedby="txt-quick-addition-error"
                className={classes.form}
                multiline
                minRows={6}
                maxRows={Infinity}
                // value={name}
                // onChange={handleChange}
                placeholder={`Dinh Van A, 0941123493, Thanh vien HDQT\nDinh Van B, 0941123493, Chu Tich HDQT`}
                inputRef={inputRef}
              />
              {isError && (
                <FormHelperText id="txt-quick-addition-error">
                  Hãy gửi form đúng định dạng: Họ tên, Số điện thoại, vai trò.
                  Ví dụ: Dinh Van A, 0941123493, Thanh vien HDQT
                </FormHelperText>
              )}
            </FormControl>
            <Button
              variant="contained"
              type="submit"
              className={classes.submitButton}
              value="Thêm vào danh sách"
            >
              Thêm vào danh sách
            </Button>
          </form>
        </Box>
        {resultQuickAdd ? <Typography>{resultQuickAdd}</Typography> : ""}
        <Snackbar open={open} autoHideDuration={2000} onClose={handleClose}>
          <Alert
            onClose={handleClose}
            severity={notifySetting.isSuccess ? "success" : "error"}
          >
            {notifySetting.message}
          </Alert>
        </Snackbar>
      </Box>
    </>
  );
};

export default Committee;
