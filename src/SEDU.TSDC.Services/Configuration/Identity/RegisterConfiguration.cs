﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Configuration.Identity
{
    public class RegisterConfiguration
    {
        public bool Enabled { get; set; } = true;
    }
}
