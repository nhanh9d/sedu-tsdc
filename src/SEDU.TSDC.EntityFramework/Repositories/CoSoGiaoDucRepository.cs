﻿using Microsoft.EntityFrameworkCore;
using SEDU.TSDC.EntityFramework.DbContexts.Interfaces;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.EntityFramework.Extensions.Extensions;
using SEDU.TSDC.EntityFramework.Repositories.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Repositories
{
    public class CoSoGiaoDucRepository<TDbContext> : ICoSoGiaoDucRepository
        where TDbContext : ITsdcDbContext
    {
        protected readonly TDbContext dbContext;

        public CoSoGiaoDucRepository(TDbContext _dbContext)
        {
            dbContext = _dbContext;
        }

        public async Task<CO_SO_GIAO_DUC> CreateCoSoGiaoDucAsync(CO_SO_GIAO_DUC coSoGiaoDuc)
        {
            await dbContext.CO_SO_GIAO_DUC.AddAsync(coSoGiaoDuc);
            await dbContext.SaveChangeAsync();

            return coSoGiaoDuc;
        }

        public async Task<int> DeleteCoSoGiaoDucAsync(int id)
        {
            var csGiaoDuc = await dbContext.CO_SO_GIAO_DUC.FindAsync(id);

            if (csGiaoDuc != null)
            {
                csGiaoDuc.IS_DELETED = true;
                csGiaoDuc.DELETED_DATE = DateTime.Now;

                await dbContext.SaveChangeAsync();
            }

            return id;
        }

        public async Task<CO_SO_GIAO_DUC> EditCoSoGiaoDucAsync(CO_SO_GIAO_DUC coSoGiaoDuc)
        {
            var csGiaoDuc = await dbContext.CO_SO_GIAO_DUC.FindAsync(coSoGiaoDuc.ID);

            if (csGiaoDuc != null)
            {
                csGiaoDuc.CHI_TIEU = coSoGiaoDuc.CHI_TIEU;
                csGiaoDuc.ID_CHA = coSoGiaoDuc.ID_CHA;
                csGiaoDuc.IS_ACTIVE = coSoGiaoDuc.IS_ACTIVE;
                csGiaoDuc.UPDATED_DATE = DateTime.Now;
                csGiaoDuc.ID_CSGD_CDV = coSoGiaoDuc.ID_CSGD_CDV;
                csGiaoDuc.TEN_CSGD = coSoGiaoDuc.TEN_CSGD;
                csGiaoDuc.TEN_HUYEN = coSoGiaoDuc.TEN_HUYEN;
                csGiaoDuc.TEN_XA = coSoGiaoDuc.TEN_XA;
                await dbContext.SaveChangeAsync();
            }

            return csGiaoDuc;
        }

        public async Task<CO_SO_GIAO_DUC> GetCoSoGiaoDucByIdAsync(int id)
        {
            return await dbContext.CO_SO_GIAO_DUC.Where(c => c.ID == id && !c.IS_DELETED && c.IS_ACTIVE)
                                           .FirstOrDefaultAsync();
        }
        public async Task<PagedList<CO_SO_GIAO_DUC>> GetPagedCoSoGiaoDucAsync(string keyword, int pageNumber, int pageSize)
        {
            var pagedList = new PagedList<CO_SO_GIAO_DUC>();
            var querry = dbContext.CO_SO_GIAO_DUC
                .Where(x => !x.IS_DELETED && x.IS_ACTIVE && x.TEN_CSGD.Contains(keyword));
            var data = await querry
                .PageBy(x => x.ID, pageNumber, pageSize)
                .ToListAsync();

            pagedList.Data.AddRange(data);
            pagedList.PageSize = pageSize;
            pagedList.TotalCount = await querry.CountAsync();

            return pagedList;
        }
    }
}
