﻿using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Services.Interface
{
    public interface IHoSoService
    {
        public Task<HoSoDto> AddHoSoAsync(HoSoDto hoSoDto);
        public Task<HoSoDto> GetHoSoByIdAsync(int hoSoId);
        public Task<HoSoDto> EditHoSoAsync(HoSoDto hoSoDto);
        public Task<int> DeleteHoSoAsync(int hoSoId);
        Task<PagedList<HoSoDto>> GetPagedHoSoAsync(string trang_thai_duyet_ho_so,bool trang_thai_nhap_hoc, int nam_xet_tuyen,string keyword, int pageNumber, int pageSize);
    }
}
