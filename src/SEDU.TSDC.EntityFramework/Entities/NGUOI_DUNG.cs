﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Entities
{
    public class NGUOI_DUNG : BASE_ENTITY
    {
        [MaxLength(50, ErrorMessage = "Họ tên tối đa 50 kí tự")]
        public string HO_TEN { get; set; }
        [MaxLength(12, ErrorMessage = "Số điện thoại tối đa 12 kí tự")]
        public string SO_DT { get; set; }
        [MaxLength(64)]
        public string MAT_KHAU { get; set; }
        [MaxLength(100, ErrorMessage = "Email tối đa 12 kí tự")]
        public string EMAIL { get; set; }
        public string DVC_TOKEN { get; set; }
        #region relation props
        public virtual ICollection<XET_DUYET_HO_SO> NGUOI_XET_DUYET { get; set; }
        public virtual ICollection<HOI_DONG_TUYEN_SINH> NGUOI_THUOC_HOI_DONG_TUYEN_SINH { get; set; }
        public virtual ICollection<BAO_CAO> NGUOI_TAO_BAO_CAO { get; set; }

        #endregion

    }
}
