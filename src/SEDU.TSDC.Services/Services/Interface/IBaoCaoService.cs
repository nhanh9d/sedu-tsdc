﻿using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Services.Interface
{
    public interface IBaoCaoService
    {
        public Task<BaoCaoDto> AddBaoCaoAsync(BaoCaoDto baoCaoDto);
        public Task<BaoCaoDto> GetBaoCaoByIdAsync(int baoCaoId);
        public Task<BaoCaoDto> EditBaoCaoAsync(BaoCaoDto baoCaoDto);
        public Task<int> DeleteBaoCaoAsync(int baoCaoId);
        Task<PagedList<BaoCaoDto>> GetPagedBaoCaoAsync(string keyword, int pageNumber, int pageSize);
        Task<List<BaoCaoDto>> GetListDotTuyenSinh();
    }
}
