﻿

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Parameters.VaiTro
{
    public class CreateVaiTroParams
    {
        [MaxLength(100, ErrorMessage = "Tên vai trò tối đa 50 kí tự")]
        public string TenVaiTro { get; set; }
    }
}
