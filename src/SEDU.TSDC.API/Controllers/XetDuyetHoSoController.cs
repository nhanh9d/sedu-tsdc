﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SEDU.TSDC.API.Mappers;
using SEDU.TSDC.API.Parameters;
using SEDU.TSDC.API.Parameters.XetDuyetHoSo;
using SEDU.TSDC.Services.Dtos.Tsdc;
using SEDU.TSDC.Services.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors("SeduCors")]
    public class XetDuyetHoSoController : ControllerBase
    {

        private readonly ILogger<XetDuyetHoSoController> _logger;
        private readonly IXetDuyetHoSoService _xetDuyetHoSoService;

        public XetDuyetHoSoController(ILogger<XetDuyetHoSoController> logger, IXetDuyetHoSoService xetDuyetHoSoService)
        {
            _logger = logger;
            _xetDuyetHoSoService = xetDuyetHoSoService;
        }

        [Route("healthcheck")]
        [HttpGet]
        public string HealthCheck()
        {
            return "Ok";
        }

        [Route("add-xet-duyet-ho-so")]
        [HttpPost]
        public async Task<XetDuyetHoSoDto> AddXetDuyetHoSo(CreateXetDuyetHoSoParams pr)
        {
            var dto = pr.ToDto();
            return await _xetDuyetHoSoService.AddXetDuyetHoSoAsync(dto);
        }

        [Route("get-xet-duyet-ho-so-by-id")]
        [HttpPost]
        public async Task<object> GetXetDuyetHoSoById(int xetDuyetHoSoId)
        {
            try
            {
                return await _xetDuyetHoSoService.GetXetDuyetHoSoByIdAsync(xetDuyetHoSoId);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [Route("edit-xet-duyet-ho-so")]
        [HttpPost]
        public async Task<XetDuyetHoSoDto> EditXetDuyetHoSo(EditXetDuyetHoSoParams pr)
        {
            var dto = pr.ToDto();
            return await _xetDuyetHoSoService.EditXetDuyetHoSoAsync(dto);
        }

        [Route("delete-xet-duyet-ho-so")]
        [HttpPost]
        public async Task<int> DeleteXetDuyetHoSo(int xetDuyetHoSoId)
        {
            return await _xetDuyetHoSoService.DeleteXetDuyetHoSoAsync(xetDuyetHoSoId);
        }
        [Route("get-paged-xet-duyet-ho-so")]
        [HttpPost]
        public async Task<object> GetPagedXetDuyetHoSo(PagedParams pr)
        {
            var lst = await _xetDuyetHoSoService.GetPagedXetDuyetHoSoAsync(pr.keyword, pr.pageNumber, pr.pageSize);
            return lst;
        }
        [Route("get-paged-loai-xet-duyet-ho-so")]
        [HttpPost]
        public async Task<object> GetPagedLoaiXetDuyetHoSo(PagedHoSoParams pr)
        {
            var lst = await _xetDuyetHoSoService.GetPagedXetDuyetHoSoAsync(pr.trang_thai_duyet_ho_so, pr.trang_thai_nhap_hoc, pr.nam_xet_tuyen, pr.keyword, pr.pageNumber, pr.pageSize);
            return lst;
        }
    }
}
