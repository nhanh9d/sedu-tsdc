﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Extensions.Enums
{
    public enum ConnectorType
    {
        Input,
        Output
    }
}
