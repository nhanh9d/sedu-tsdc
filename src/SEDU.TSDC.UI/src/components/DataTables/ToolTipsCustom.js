import React, { useRef, useState } from 'react'
import { withStyles } from '@material-ui/core/styles'
import MaterialTooltip from '@material-ui/core/Tooltip'
import { IP_API } from '../../helper/helper'
import { options_ticket } from './../../helper/helper'
import axios from 'axios'
import FormModal from './FormModal'
import Snackbar from '@material-ui/core/Snackbar'
import Alert from './../Alert/AlertCustom'

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: theme.shadows[1],
    borderRadius: `2px`,
    fontSize: 11,
    padding: `0`,
  },
  // },
  // arrow: {
  //     color: "white"
  // }
}))(MaterialTooltip)

const ToolTipsCustom = (props) => {
  // debugger;
  const [position, setPosition] = React.useState({
    x: undefined,
    y: undefined,
  })

  // context
  // const context = useContext(FetchDataContext)

  const areaRef = useRef(null)
  const handlePosition = (e) => {
    setPosition({ x: e.clientX + 10, y: e.clientY + 10 })
  }
  const [notifySetting, setNotifySetting] = useState({
    isSuccess: true,
    message: '',
  })
  const [isOpen, setIsOpen] = useState(false)
  //hungdm - Xu ly chap nhan || Tu choi ho so
  const Accept = (item, ketQua) => {
    const IPv4 = localStorage.getItem('ip')
    const params = {
      thoiGianDuyet: new Date(),
      ipDuyet: IPv4,
      ketQua: ketQua,
      ghiChu: item.ghiChu ? item.ghiChu : '',
      idHoSo: item.id,
      idNguoiDuyet: 1,
    }
    const postData = async () => {
      await axios
        .post(
          IP_API + '/XetDuyetHoSo/add-xet-duyet-ho-so',
          params,
          options_ticket
        )
        .then((reponse) => {
          if (reponse && reponse.status === 200) {
            setIsOpen(true)
            if (ketQua === 2) {
              setNotifySetting({
                isSuccess: true,
                message: 'Đồng ý thành công',
              })
            } else {
              setNotifySetting({
                isSuccess: false,
                message: 'Từ chối thành công',
              })
              handleCloseModal()
            }

            // goi api cho tat ca
            // debugger;
            props.handleAdmitAndReject();
          }
        })
        .catch((error) => {
          debugger;
          if (error.response.status === 401) {
            window.location.href =
              'https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback'
          } else {
            setIsOpen(true)
            setNotifySetting({
              isSuccess: false,
              message: 'Lỗi Server',
            })
          }
        })
    }
    postData()
  }
  const [isOpenModal, setIsOpenModal] = useState(false)
  const handleOpenModal = () => {
    setIsOpenModal(true)
  }
  const handleCloseModal = () => {
    setIsOpenModal(false)
  }
  const handleClose = () => {
    setIsOpen(false)
  }
  return (
    <>
      <LightTooltip
        onMouseMove={handlePosition}
        interactive
        open={props.openTooltip}
        arrow
        ref={areaRef}
        PopperProps={{
          anchorEl: {
            clientHeight: 0,
            clientWidth: 0,
            getBoundingClientRect: () => ({
              top: areaRef.current?.getBoundingClientRect().top ?? 0,
              left: position.x,
              right: position.x,
              bottom: areaRef.current?.getBoundingClientRect().bottom ?? 0,
              width: 0,
              height: 0,
            }),
          },
        }}
        title={
          <>
            <h2
              className="tooltip-title"
              style={{
                fontSize: `15px`,
                backgroundColor: `#d4e5e0`,
                padding: `5px 10px`,
                width: `100%`,
                borderBottom: `1px solid black`,
              }}
            >
              {`${props.row.maHoSo} - ${props.row.hoDem} ${props.row.ten}`}
            </h2>
            <div
              className="tooltip-content d-flex flex-column justify-content-evenly"
              style={{
                margin: `5px 10px`,
                width: `100%`,
              }}
            >
              {props.row.trangThaiDuyetHoSo === 1 && (
                <div
                  style={{
                    margin: `5px 10px`,
                    width: `100%`,
                    minHeight: `50px`,
                  }}
                >
                  <div>Lý do bị từ chối:</div>
                  <div>abcdef....</div>
                </div>
              )}
              {props.row.trangThaiDuyetHoSo === 0 && (
                <div
                  className="d-flex flex-column justify-content-evenly"
                  style={{
                    margin: `5px 10px`,
                    width: `100%`,
                    minHeight: `100px`,
                  }}
                >
                  <a href="/" style={{ cursor: `pointer` }}>
                    Download hồ sơ
                  </a>
                  <div className="d-flex">
                    <button
                      type="button"
                      className="btn btn-primary"
                      style={{ fontSize: `10px`, marginRight: `10px` }}
                      onClick={() => Accept(props.row, 2)}
                    >
                      Chấp nhận
                    </button>
                    <button
                      type="button"
                      className="btn btn-danger"
                      style={{ fontSize: `10px` }}
                      onClick={handleOpenModal}
                    >
                      Từ chối
                    </button>
                  </div>
                </div>
              )}
            </div>
          </>
        }
      >
        {props.children}
      </LightTooltip>
      {isOpenModal && (
        <FormModal
          handleCloseModal={handleCloseModal}
          maHoSo={props.row.maHoSo}
          item={props.row}
          saveData={Accept}
        />
      )}
      <Snackbar open={isOpen} autoHideDuration={1000} onClose={handleClose}>
        <Alert
          onClose={handleClose}
          severity={notifySetting.isSuccess ? 'success' : 'error'}
        >
          {notifySetting.message}
        </Alert>
      </Snackbar>
    </>
  )
}
export default ToolTipsCustom
