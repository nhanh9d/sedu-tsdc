﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Parameters.CoSoGiaoDuc
{
    public class CreateCoSoGiaoDucParams
    {
        [MaxLength(100, ErrorMessage = "Tên cơ sở giáo dục tối đa 100 kí tự")]
        public string TenCSGD { get; set; }
        [MaxLength(50, ErrorMessage = "ID cơ sở giáo dục trên cổng dịch vụ tối đa 50 kí tự")]
        public string IDCoSoGiaoDucCDV { get; set; }
        [MaxLength(100, ErrorMessage = "Tên huyện tối đa 100 kí tự")]
        public string TenHuyen { get; set; }
        [MaxLength(100, ErrorMessage = "Tên xã tối đa 100 kí tự")]
        public string TenXa { get; set; }
        public int ChiTieu { get; set; }
        public int IdCha { get; set; }
    }
}
