﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Dtos
{
    public abstract class BaseDto
    {
        public BaseDto()
        {
            IsActive = true;
        }
        public int Id { get; set; }
        public bool IsActive { get; set; }
    }
}
