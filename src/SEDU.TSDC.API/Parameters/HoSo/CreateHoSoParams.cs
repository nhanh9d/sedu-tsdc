﻿

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Parameters.HoSo
{
    public class CreateHoSoParams
    {
        [MaxLength(50, ErrorMessage = "Mã hồ sơ tối đa 50 kí tự")]
        public string MaHoSo { get; set; }
        public int NamXetTuyen { get; set; }
        [MaxLength(30, ErrorMessage = "Họ và đệm tối đa 30 kí tự")]
        public string HoDem { get; set; }
        [MaxLength(10, ErrorMessage = "Tên tối đa 10 kí tự")]
        public string Ten { get; set; }
        public DateTime NgaySinh { get; set; }
        [MaxLength(50, ErrorMessage = "ID của CSGD trên cổng dịch vụ tối đa 50 kí tự")]
        public string IdCoSoGiaoDucCDV { get; set; }
        public int TrangThaiDuyetHoSo { get; set; }
        public DateTime ThoiDiemCapNhatCuoi { get; set; }
        public bool TrangThaiNhapHoc { get; set; }
    }
}
