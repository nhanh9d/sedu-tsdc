﻿

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Parameters.HoiDongTuyenSinh
{
    public class CreateHoiDongTuyenSinhParams
    {
        public int IdNguoiDung { get; set; }
        public int IdCoSoGiaoDuc { get; set; }
        public int NamTuyenSinh { get; set; }
        public int IdVaiTro { get; set; }
    }
}
