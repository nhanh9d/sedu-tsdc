﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SEDU.TSDC.API.Mappers;
using SEDU.TSDC.API.Parameters;
using SEDU.TSDC.API.Parameters.HoSo;
using SEDU.TSDC.Services.Dtos.Tsdc;
using SEDU.TSDC.Services.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors("SeduCors")]
    public class HoSoController : ControllerBase
    {

        private readonly ILogger<HoSoController> _logger;
        private readonly IHoSoService _hoSoService;

        public HoSoController(ILogger<HoSoController> logger, IHoSoService hoSoService)
        {
            _logger = logger;
            _hoSoService = hoSoService;
        }

        [Route("healthcheck")]
        [HttpGet]
        public string HealthCheck()
        {
            return "Ok";
        }

        [Route("add-ho-so")]
        [HttpPost]
        public async Task<HoSoDto> AddHoSo(CreateHoSoParams pr)
        {
            var dto = pr.ToDto();
            return await _hoSoService.AddHoSoAsync(dto);
        }

        [Route("get-ho-so-by-id")]
        [HttpPost]
        public async Task<object> GetHoSoById(int hoSoId)
        {
            try
            {
                return await _hoSoService.GetHoSoByIdAsync(hoSoId);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [Route("edit-ho-so")]
        [HttpPost]
        public async Task<HoSoDto> EditHoSo(EditHoSoParams pr)
        {
            var dto = pr.ToDto();
            return await _hoSoService.EditHoSoAsync(dto);
        }

        [Route("delete-ho-so")]
        [HttpPost]
        public async Task<int> DeleteHoSo(int hoSoId)
        {
            return await _hoSoService.DeleteHoSoAsync(hoSoId);
        }
        [Route("get-paged-ho-so")]
        [HttpPost]
        public async Task<object> GetPagedHoSo(PagedHoSoParams pr)
        {
            var lst = await _hoSoService.GetPagedHoSoAsync(pr.trang_thai_duyet_ho_so, pr.trang_thai_nhap_hoc, pr.nam_xet_tuyen, pr.keyword, pr.pageNumber, pr.pageSize);
            return lst;
        }
    }
}
