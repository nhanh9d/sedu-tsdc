﻿using AutoMapper;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.Services.Mappers
{
    public static class XetDuyetHoSoMapper
    {
        internal static IMapper DtoMapper;
        internal static IMapper EntityMapper;

        static XetDuyetHoSoMapper()
        {
            DtoMapper = new MapperConfiguration(config => config.AddProfile<XetDuyetHoSoDtoMapperProfile>()).CreateMapper();
            EntityMapper = new MapperConfiguration(config => config.AddProfile<XetDuyetHoSoEntityMapperProfile>()).CreateMapper();
        }

        //public static List<Activity> ToModel(this List<APP_ACTIVITY> _APP_ACTIVITY)
        //{
        //    return Mapper.Map<List<Activity>>(_APP_ACTIVITY);
        //}

        //public static Activity ToModel(this APP_ACTIVITY _APP_ACTIVITY)
        //{
        //    return Mapper.Map<Activity>(_APP_ACTIVITY);
        //}

        public static XET_DUYET_HO_SO ToEntity(this XetDuyetHoSoDto xetDuyetHoSoDto)
        {
            return EntityMapper.Map<XET_DUYET_HO_SO>(xetDuyetHoSoDto);
        }

        public static XetDuyetHoSoDto ToDto(this XET_DUYET_HO_SO xetDuyetHoSo)
        {
            return DtoMapper.Map<XetDuyetHoSoDto>(xetDuyetHoSo);
        }

        public static PagedList<XetDuyetHoSoDto> ToDto(this PagedList<XET_DUYET_HO_SO> xetDuyetHoSo)
        {
            return DtoMapper.Map<PagedList<XetDuyetHoSoDto>>(xetDuyetHoSo);
        }
    }
}
