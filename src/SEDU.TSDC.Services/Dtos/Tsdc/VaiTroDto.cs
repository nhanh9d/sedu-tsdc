﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Dtos.Tsdc
{
    public class VaiTroDto : BaseDto
    {
        public VaiTroDto() : base()
        {
        }
        [MaxLength(100, ErrorMessage = "Tên vai trò tối đa 50 kí tự")]
        public string TenVaiTro { get; set; }
    }
}
