﻿using AutoMapper;
using SEDU.TSDC.API.Parameters.HoiDongTuyenSinh;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System;

namespace SEDU.TSDC.API.Mappers
{
    public class HoiDongTuyenSinhMapperProfile : Profile
    {
        public HoiDongTuyenSinhMapperProfile()
        {
            CreateMap<CreateHoiDongTuyenSinhParams, HoiDongTuyenSinhDto>(MemberList.Destination);
            CreateMap<EditHoiDongTuyenSinhParams, HoiDongTuyenSinhDto>(MemberList.Destination);
            CreateMap<ThemNhieuHoiDongTuyenSinhParams, HoiDongTuyenSinhDto>(MemberList.Destination)
                .ForMember(dto => dto.TenNguoiDung, opt => opt.MapFrom(pr => pr.HoTen))
                .ForMember(dto => dto.VaiTro, opt => opt.MapFrom(pr => pr.ChucVu));
        }
    }
}
