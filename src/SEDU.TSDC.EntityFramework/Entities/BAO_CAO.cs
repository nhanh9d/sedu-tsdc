﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Entities
{
    public class BAO_CAO : BASE_ENTITY
    {
        public DateTime THOI_GIAN_BAT_DAU { get; set; }
        public DateTime THOI_GIAN_KET_THUC { get; set; }
        public DateTime THOI_GIAN_TAO { get; set; }
        public int TONG_CHI_TIEU { get; set; }
        public int TONG_HO_SO { get; set; }
        public int TONG_TRUNG_TUYEN { get; set; }
        public int TONG_BI_LOAI { get; set; }
        public string BC_THUAN_LOI { get; set; }
        public string BC_KHO_KHAN { get; set; }
        public string BC_DE_XUAT { get; set; }
        public string BC_NOI_DUNG_KHAC { get; set; }

        #region Relations Props

        public int ID_CSGD { get; set; }
        public virtual CO_SO_GIAO_DUC CSGD { get; set; }

        public int ID_CHU_TICH_HDTS { get; set; }
        public virtual NGUOI_DUNG CHU_TICH_HDTS { get; set; }

        public string DS_THANH_VIEN_HDTS { get; set; }

        public int ID_NGUOI_TAO { get; set; }
        public virtual NGUOI_DUNG NGUOI_TAO_BAO_CAO { get; set; }

        #endregion
    }
}
