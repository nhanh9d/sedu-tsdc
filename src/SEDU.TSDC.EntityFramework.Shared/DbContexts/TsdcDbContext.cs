﻿using Microsoft.EntityFrameworkCore;
using SEDU.TSDC.EntityFramework.DbContexts.Interfaces;
using SEDU.TSDC.EntityFramework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Shared.DbContexts
{
    public class TsdcDbContext : DbContext, ITsdcDbContext
    {
        public TsdcDbContext(DbContextOptions<TsdcDbContext> dbContextOptions) :
            base(dbContextOptions)
        {
        }
        public DbSet<CO_SO_GIAO_DUC> CO_SO_GIAO_DUC { get; set; }
        public DbSet<BAO_CAO> BAO_CAO { get; set; }
        public DbSet<NGUOI_DUNG> NGUOI_DUNG { get; set; }
        public DbSet<VAI_TRO> VAI_TRO { get; set; }
        public DbSet<HOI_DONG_TUYEN_SINH> HOI_DONG_TUYEN_SINH { get; set; }
        public DbSet<HO_SO> HO_SO { get; set; }
        public DbSet<XET_DUYET_HO_SO> XET_DUYET_HO_SO { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region ID
            modelBuilder.Entity<CO_SO_GIAO_DUC>()
                .HasKey(p => p.ID);
            modelBuilder.Entity<BAO_CAO>()
                .HasKey(c => c.ID);
            modelBuilder.Entity<NGUOI_DUNG>()
                .HasKey(c => c.ID);
            modelBuilder.Entity<VAI_TRO>()
                .HasKey(n => n.ID);
            modelBuilder.Entity<HOI_DONG_TUYEN_SINH>()
                .HasKey(s => s.ID);
            modelBuilder.Entity<HO_SO>()
                .HasKey(s => s.ID);
            modelBuilder.Entity<XET_DUYET_HO_SO>()
                .HasKey(s => s.ID);
            #endregion

            #region Indexes
            modelBuilder.Entity<BAO_CAO>().HasIndex(bc => bc.ID_CSGD);
            modelBuilder.Entity<BAO_CAO>().HasIndex(bc => bc.ID_CHU_TICH_HDTS);
            modelBuilder.Entity<BAO_CAO>().HasIndex(bc => bc.ID_NGUOI_TAO);
            modelBuilder.Entity<NGUOI_DUNG>().HasIndex(nd => nd.EMAIL);
            modelBuilder.Entity<XET_DUYET_HO_SO>().HasIndex(xdhs => xdhs.ID_HO_SO);
            #endregion

            #region Relations
            //========================XET_DUYET_HO_SO========================
            modelBuilder.Entity<XET_DUYET_HO_SO>()
                        .HasOne(xdhs => xdhs.HO_SO)
                        .WithMany(hs => hs.BO_XET_DUYET)
                        .HasForeignKey(xdhs => xdhs.ID_HO_SO)
                        .IsRequired(false)
                        .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<XET_DUYET_HO_SO>()
                        .HasOne(xdhs => xdhs.NGUOI_DUYET)
                        .WithMany(nd => nd.NGUOI_XET_DUYET)
                        .HasForeignKey(xdhs => xdhs.ID_NGUOI_DUYET)
                        .IsRequired(false)
                        .OnDelete(DeleteBehavior.Cascade);

            //========================HOI_DONG_TUYEN_SINH========================
            modelBuilder.Entity<HOI_DONG_TUYEN_SINH>()
                        .HasOne(hdts => hdts.NGUOI_DUNG)
                        .WithMany(nd => nd.NGUOI_THUOC_HOI_DONG_TUYEN_SINH)
                        .HasForeignKey(hdts => hdts.ID_NGUOI_DUNG)
                        .IsRequired(false)
                        .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<HOI_DONG_TUYEN_SINH>()
                        .HasOne(hdts => hdts.CSGD)
                        .WithMany(csgd => csgd.CSGD_THUOC_HDTS)
                        .HasForeignKey(hdts => hdts.ID_CSGD)
                        .IsRequired(false)
                        .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<HOI_DONG_TUYEN_SINH>()
                        .HasOne(hdts => hdts.VAI_TRO)
                        .WithMany(vt => vt.VAI_TRO_TRONG_HDTS)
                        .HasForeignKey(hdts => hdts.ID_VAI_TRO)
                        .IsRequired(false)
                        .OnDelete(DeleteBehavior.Cascade);

            //========================BAO_CAO========================
            modelBuilder.Entity<BAO_CAO>()
                        .HasOne(bc => bc.NGUOI_TAO_BAO_CAO)
                        .WithMany(nd => nd.NGUOI_TAO_BAO_CAO)
                        .HasForeignKey(dts => dts.ID_NGUOI_TAO)
                        .IsRequired(false)
                        .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<BAO_CAO>()
                        .HasOne(bc => bc.CSGD)
                        .WithMany(nd => nd.CSGD_TRONG_BAO_CAO)
                        .HasForeignKey(dts => dts.ID_CSGD)
                        .IsRequired(false)
                        .OnDelete(DeleteBehavior.Cascade);

            //========================CO_SO_GIAO_DUC========================
            modelBuilder.Entity<CO_SO_GIAO_DUC>()
                        .HasOne(bc => bc.CSGD_CHU_QUAN)
                        .WithMany(nd => nd.CSGD_TRUC_THUOC)
                        .HasForeignKey(dts => dts.ID_CHA)
                        .IsRequired(false)
                        .OnDelete(DeleteBehavior.Cascade);
            #endregion

            base.OnModelCreating(modelBuilder);
        }

        public Task<int> SaveChangeAsync()
        {
            return base.SaveChangesAsync();
        }
    }
}
