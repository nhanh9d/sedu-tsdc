﻿using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using System;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Repositories.Interfaces
{
    public interface IHoSoRepository
    {
        Task<HO_SO> CreateHoSoAsync(HO_SO client);
        Task<int> DeleteHoSoAsync(int id);
        Task<HO_SO> EditHoSoAsync(HO_SO client);
        Task<HO_SO> GetHoSoByIdAsync(int id);
        Task<PagedList<HO_SO>> GetPagedHoSoAsync(int trang_thai_duyet_hoso, bool trang_thai_nhap_hoc, int nam_xet_tuyen, string keyword, int pageNumber, int pageSize);
    }
}
