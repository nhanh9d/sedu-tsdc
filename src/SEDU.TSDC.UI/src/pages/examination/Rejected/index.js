import React, { useState, useEffect } from "react";
import DataTableCustom from "../../../components/DataTables/DataTableCustom";
import moment from "moment";

import { IP_API, options_ticket } from "./../../../helper/helper";
import axios from "axios";

// import for Loading effect
import LoadingEffect from "../../../components/LoadingEffect";

const columns = [
  { id: "STT", label: "STT", width: 20, align: "left" },
  { id: "Mã hồ sơ", label: "Mã hồ sơ", width: 130, align: "left" },
  {
    id: "Họ đệm",
    label: "Họ đệm",
    width: 130,
    align: "left",
  },
  {
    id: "Tên",
    label: "Tên",
    width: 130,
    align: "left",
  },
  {
    id: "Ngày sinh",
    label: "Ngày sinh",
    width: 130,
    align: "left",
  },
  {
    id: "Lý do",
    label: "Lý do",
    width: 130,
    align: "left",
  },
];

// param for calling api

const Rejected = ({ listData }) => {
  const [rows, setRows] = useState([]);
  const [loading, isLoading] = useState(false);
  const [listRejected, setListRejected] = useState([]);
  const [totalRecords, setTotalRecords] = useState(0);

  // context
  // const context = useContext(FetchDataContext)

  // function to call api
  const fetchRejected = (searchKey = "", pageNumber = 1, pageSize = 5) => {
    const params = {
      trang_thai_nhap_hoc: true,
      trang_thai_duyet_ho_so: "1",
      nam_xet_tuyen: 2021,
      keyword: searchKey,
      pageNumber: pageNumber,
      pageSize: pageSize,
    };
    const fetchData = async () => {
      isLoading(true);
      await axios
        .post(
          IP_API + "/XetDuyetHoSo/get-paged-loai-xet-duyet-ho-so",
          params,
          options_ticket
        )
        .then((response) => {
          // call API thanh cong
          // setListDataReal
          if (response && response.data) {
            setTotalRecords(response.data.totalCount);
            setListRejected(response.data.data);
          }
        })
        .catch((error) => {
          if (error.response.status === 401) {
            window.location.href =
              "https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback";
          }
        });
      isLoading(false);
    };
    fetchData();
  };

  useEffect(() => {
    fetchRejected();
  }, []);

  // get rows
  useEffect(() => {
    let renderedItems;
    if (listRejected) {
      renderedItems = listRejected.map((item) => {
        let { maHoSo, hoDem, ten, ngaySinh, trangThaiDuyetHoSo, id } =
          item.hoSo;
        ngaySinh = moment(ngaySinh).format("DD/MM/YYYY");
        let trangThaiDuyetHoSo_Text = "Đã từ chối";
        return {
          maHoSo,
          hoDem,
          ten,
          ngaySinh,
          trangThaiDuyetHoSo,
          trangThaiDuyetHoSo_Text,
          id,
        };
      });
      setRows(renderedItems);
    }
  }, [listRejected]);

  return (
    <>
      <LoadingEffect loading={loading} />
      <DataTableCustom
        columns={columns}
        openTooltip={false}
        rows={rows}
        searchBar={true}
        status="1"
        handleAdmitAndReject={fetchRejected}
        handleSearch={fetchRejected}
        totalRecords={totalRecords}
      />
    </>
  );
};

export default Rejected;
