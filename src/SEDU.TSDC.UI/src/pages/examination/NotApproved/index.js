import React, { useState, useEffect,useCallback } from 'react'
import DataTableCustom from '../../../components/DataTables/DataTableCustom'
import { IP_API, options_ticket } from "./../../../helper/helper";
import axios from "axios";
// import FetchDataContext from '../../../store/fetchData-context'
import moment from 'moment'

// import for Loading effect
import LoadingEffect from "../../../components/LoadingEffect";

// column to filter data
const columns = [
  { id: 'STT', label: 'STT', width: 20, align: 'left' },
  { id: 'Mã hồ sơ', label: 'Mã hồ sơ', width: 130, align: 'left' },
  {
    id: 'Họ đệm',
    label: 'Họ đệm',
    width: 130,
    align: 'left',
  },
  {
    id: 'Tên',
    label: 'Tên',
    width: 130,
    align: 'left',
  },
  {
    id: 'Ngày sinh',
    label: 'Ngày sinh',
    width: 130,
    align: 'left',
  },
  {
    id: 'Trạng thái',
    label: 'Trạng thái',
    width: 130,
    align: 'left',
  },
]





const NotApproved = (props) => {
  // const [columns, setColumns] = useState([]);
  const [rows, setRows] = useState([])
  const [loading, isLoading] = useState(false);
  const [listNotApproved, setlistNotApproved] = useState([]);
  const [totalRecords, setTotalRecords] = useState(0);
  // const [searchKey, setSearchKey] = useState("");

  // context
  // const context = useContext(FetchDataContext)

  // function to call api
  const fetchNotApproved = useCallback((searchKey="", pageNumber=1, pageSize=5) => {

    const fetchData = async () => {
      // param for calling api
      const params = {
        trang_thai_nhap_hoc: true,
        trang_thai_duyet_ho_so: "0",
        nam_xet_tuyen: 2021,
        keyword: searchKey,
        pageNumber: pageNumber,
        pageSize: pageSize,
      };

      isLoading(true);
      await axios
        .post(IP_API + "/HoSo/get-paged-ho-so", params, options_ticket)
        .then((response) => {
          // call API thanh cong
          // setListDataReal
          if (response && response.data) {
            setTotalRecords(response.data.totalCount);
            setlistNotApproved(response.data.data);
          }
        })
        .catch((error) => {
          debugger;
          console.log("unauthorize");
          if (error.response && error.response.status === 401) {
            window.location.href =
              "https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback";
          }
        });
      isLoading(false);
    };
    fetchData();
  },[])

  // call api to get data
  useEffect(() => {
    console.log("SET DATA")

    fetchNotApproved();
  }, [fetchNotApproved])

  // get rows
  useEffect(() => {
    let renderedItems
    if (listNotApproved) {
      renderedItems = listNotApproved.map((item) => {
        let { maHoSo, hoDem, ten, ngaySinh, trangThaiDuyetHoSo, id } = item
        ngaySinh = moment(ngaySinh).format('DD/MM/YYYY')
        let trangThaiDuyetHoSo_Text =
          trangThaiDuyetHoSo === 0 ? 'Xét duyệt' : ''
        return {
          maHoSo,
          hoDem,
          ten,
          ngaySinh,
          trangThaiDuyetHoSo,
          trangThaiDuyetHoSo_Text,
          id,
        }
      })
      setRows(renderedItems)
    }
  }, [listNotApproved])

  return (
    <>
      <LoadingEffect loading={loading} />
      <DataTableCustom
        columns={columns}
        rows={rows}
        searchBar={true}
        status="0"
        handleAdmitAndReject={fetchNotApproved}
        handleSearch={fetchNotApproved}
        totalRecords={totalRecords}
      />
    </>
  );
}

export default NotApproved
