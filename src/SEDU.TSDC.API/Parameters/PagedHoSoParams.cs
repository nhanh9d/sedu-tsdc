﻿

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Parameters
{
    public class PagedHoSoParams
    {
        //true: Đã đăng ký nhập học
        //false: Không nhập học
        public bool trang_thai_nhap_hoc { get; set; }
        //0: Chưa duyệt
        //1: Từ chối
        //2: Đồng ý
        //3: Đã duyệt (Lấy cả 1 vs 2) 
        public string trang_thai_duyet_ho_so { get; set; }
        public int nam_xet_tuyen { get; set; }
        public string keyword { get; set; }
        public int pageNumber { get; set; }
        public int pageSize { get; set; }
    }
}
