﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SEDU.TSDC.API.Mappers;
using SEDU.TSDC.API.Parameters;
using SEDU.TSDC.API.Parameters.BaoCao;
using SEDU.TSDC.Services.Dtos.Tsdc;
using SEDU.TSDC.Services.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors("SeduCors")]
    public class BaoCaoController : ControllerBase
    {

        private readonly ILogger<BaoCaoController> _logger;
        private readonly IBaoCaoService _baoCaoService;

        public BaoCaoController(ILogger<BaoCaoController> logger, IBaoCaoService baoCaoService)
        {
            _logger = logger;
            _baoCaoService = baoCaoService;
        }

        [Route("healthcheck")]
        [HttpGet]
        public string HealthCheck()
        {
            return "Ok";
        }

        [Route("add-bao-cao")]
        [HttpPost]
        public async Task<BaoCaoDto> AddBaoCao(CreateBaoCaoParams pr)
        {
            var dto = pr.ToDto();
            return await _baoCaoService.AddBaoCaoAsync(dto);
        }

        [Route("get-bao-cao-by-id")]
        [HttpPost]
        public async Task<object> GetBaoCaoById(int baoCaoId)
        {
            try
            {
                return await _baoCaoService.GetBaoCaoByIdAsync(baoCaoId);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [Route("edit-bao-cao")]
        [HttpPost]
        public async Task<BaoCaoDto> EditBaoCao(EditBaoCaoParams pr)
        {
            var dto = pr.ToDto();
            return await _baoCaoService.EditBaoCaoAsync(dto);
        }

        [Route("delete-bao-cao")]
        [HttpPost]
        public async Task<int> DeleteBaoCao(int baoCaoId)
        {
            return await _baoCaoService.DeleteBaoCaoAsync(baoCaoId);
        }
        [Route("get-paged-bao-cao")]
        [HttpPost]
        public async Task<object> GetPagedBaoCao(PagedParams pr)
        {
            var lst = await _baoCaoService.GetPagedBaoCaoAsync(pr.keyword, pr.pageNumber, pr.pageSize);
            return lst;
        }
        [Route("get-list-dot-tuyen-sinh")]
        [HttpPost]
        public async Task<object> GetListDotTuyenSinh()
        {
            var lst = await _baoCaoService.GetListDotTuyenSinh();
            return lst;
        }
    }
}
