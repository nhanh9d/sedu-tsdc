﻿using AutoMapper;
using SEDU.TSDC.API.Parameters.HoiDongTuyenSinh;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System.Collections.Generic;

namespace SEDU.TSDC.API.Mappers
{
    public static class HoiDongTuyenSinhMapper
    {
        internal static IMapper DtoMapper;

        static HoiDongTuyenSinhMapper()
        {
            DtoMapper = new MapperConfiguration(config => config.AddProfile<HoiDongTuyenSinhMapperProfile>()).CreateMapper();
        }

        public static HoiDongTuyenSinhDto ToDto(this CreateHoiDongTuyenSinhParams nguoiDung)
        {
            return DtoMapper.Map<HoiDongTuyenSinhDto>(nguoiDung);
        }

        public static HoiDongTuyenSinhDto ToDto(this EditHoiDongTuyenSinhParams nguoiDung)
        {
            return DtoMapper.Map<HoiDongTuyenSinhDto>(nguoiDung);
        }

        public static PagedList<HoiDongTuyenSinhDto> ToDto(this PagedList<NGUOI_DUNG> nguoiDung)
        {
            return DtoMapper.Map<PagedList<HoiDongTuyenSinhDto>>(nguoiDung);
        }

        public static List<HoiDongTuyenSinhDto> ToDto(this List<ThemNhieuHoiDongTuyenSinhParams> lstHdts)
        {
            return DtoMapper.Map<List<HoiDongTuyenSinhDto>>(lstHdts);
        }
    }
}
