import React, { useState } from "react";
const BreadScrumContext = React.createContext({
  breadCrumblist: ["/"],
});

export const BreadScrumContextProvider = (props) => {
  // const [breadCrumblist, setBreadScrumbList] = useState(["/"]);
  const [path, setPath] = useState("");

  return (
    <BreadScrumContext.Provider
      value={{
        breadCrumblist: path,
      }}
    >
      {props.children}
    </BreadScrumContext.Provider>
  );
};

export default BreadScrumContext;
