﻿using AutoMapper;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.Services.Mappers
{
    public class CoSoGiaoDucDtoMapperProfile : Profile
    {
        public CoSoGiaoDucDtoMapperProfile()
        {
            SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
            DestinationMemberNamingConvention = new PascalCaseNamingConvention();
            CreateMap<CO_SO_GIAO_DUC, CoSoGiaoDucDto>(MemberList.Destination)
                .ForMember(dto => dto.IDCoSoGiaoDucCDV, opt => opt.MapFrom(e => e.ID_CSGD_CDV));
            CreateMap<PagedList<CO_SO_GIAO_DUC>, PagedList<CoSoGiaoDucDto>>(MemberList.Destination).ReverseMap();
        }
    }
    public class CoSoGiaoDucEntityMapperProfile : Profile
    {
        public CoSoGiaoDucEntityMapperProfile()
        {
            SourceMemberNamingConvention = new PascalCaseNamingConvention();
            DestinationMemberNamingConvention = new LowerUnderscoreNamingConvention();
            CreateMap<CoSoGiaoDucDto, CO_SO_GIAO_DUC>(MemberList.Destination)
                .ForMember(e => e.ID_CSGD_CDV, opt => opt.MapFrom(dto => dto.IDCoSoGiaoDucCDV));
        }
    }
}
