﻿using SEDU.TSDC.Services.Dtos.Tsdc;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Services.Interface
{
    public interface ICoSoGiaoDucService
    {
        public Task<CoSoGiaoDucDto> AddCoSoGiaoDucAsync(CoSoGiaoDucDto coSoGiaoDucDto);
        public Task<CoSoGiaoDucDto> GetCoSoGiaoDucByIdAsync(int coSoGiaoDucId);
        public Task<CoSoGiaoDucDto> EditCoSoGiaoDucAsync(CoSoGiaoDucDto coSoGiaoDucDto);
        public Task<int> DeleteCoSoGiaoDucAsync(int coSoGiaoDucId);
        Task<PagedList<CoSoGiaoDucDto>> GetPagedCoSoGiaoDucAsync(string keyword, int pageNumber, int pageSize);
    }
}
