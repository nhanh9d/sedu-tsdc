﻿using AutoMapper;
using SEDU.TSDC.API.Parameters.CoSoGiaoDuc;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.API.Mappers
{
    public static class CoSoGiaoDucMapper
    {
        internal static IMapper DtoMapper;

        static CoSoGiaoDucMapper()
        {
            DtoMapper = new MapperConfiguration(config => config.AddProfile<CoSoGiaoDucMapperProfile>()).CreateMapper();
        }

        public static CoSoGiaoDucDto ToDto(this CreateCoSoGiaoDucParams coSoGiaoDuc)
        {
            return DtoMapper.Map<CoSoGiaoDucDto>(coSoGiaoDuc);
        }

        public static CoSoGiaoDucDto ToDto(this EditCoSoGiaoDucParams coSoGiaoDuc)
        {
            return DtoMapper.Map<CoSoGiaoDucDto>(coSoGiaoDuc);
        }

        public static PagedList<CoSoGiaoDucDto> ToDto(this PagedList<CO_SO_GIAO_DUC> coSoGiaoDuc)
        {
            return DtoMapper.Map<PagedList<CoSoGiaoDucDto>>(coSoGiaoDuc);
        }
    }
}
