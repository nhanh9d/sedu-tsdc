﻿

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Parameters.BaoCao
{
    public class CreateBaoCaoParams
    {
        public DateTime ThoiGianBatDau { get; set; }
        public DateTime ThoiGianKetThuc { get; set; }
        public DateTime ThoiGianTao { get; set; }
        public int TongChiTieu { get; set; }
        public int TongHoSo { get; set; }
        public int TongTrungTuyen { get; set; }
        public int TongBiLoai { get; set; }
        public string BCThuanLoi { get; set; }
        public string BCKhoKhan { get; set; }
        public string BCDeXuat { get; set; }
        public string BCNoiDungKhac { get; set; }
        public int IDCoSoGiaoDuc { get; set; }
        public int IDChuTichHDTS { get; set; }
        public int IDNguoiTao { get; set; }
        public string DSThanhVienHDTS { get; set; }
    }
}
