﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Parameters.NguoiDung
{
    public class EditNguoiDungParams : CreateNguoiDungParams
    {
        [Required]
        public int Id { get; set; }
    }
}
