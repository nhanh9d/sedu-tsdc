import React, { Fragment, useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import DataTableCustom from "../../components/DataTables/DataTableCustom";
import * as Yup from "yup";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";

import {
  // Paper,
  Box,
  Grid,
  TextField,
  FormControl,
  Select,
  MenuItem,
  Typography,
  Button,
} from "@material-ui/core";
import { Formik } from "formik";

// fake data
import { IP_API, options_ticket } from "./../../helper/helper";
import axios from "axios";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "./../../components/Alert/AlertCustom";
import moment from "moment";
// columns for table
const columns = [
  { id: "STT", label: "STT", width: 20, align: "left" },
  { id: "Ngày bắt đầu", label: "Ngày bắt đầu", width: 100, align: "left" },
  {
    id: "Ngày kết thúc",
    label: "Ngày kết thúc",
    width: 100,
    align: "left",
  },
  {
    id: "Tổng số chỉ tiêu",
    label: "Tổng số chỉ tiêu",
    width: 100,
    align: "left",
  },
  {
    id: "Số hồ sơ đăng ký",
    label: "Số hồ sơ đăng ký",
    width: 100,
    align: "left",
  },
  {
    id: "Tổng duyệt",
    label: "Tổng duyệt",
    width: 100,
    align: "left",
  },
  {
    id: "Tổng từ chối",
    label: "Tổng từ chối",
    width: 100,
    align: "left",
  },
  // {
  //   id: "Tổng nhập học",
  //   label: "Tổng nhập học",
  //   width: 100,
  //   align: "left",
  // },
];

// styles
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: "flex",
    flexWrap: "wrap",
  },
  chip: {
    margin: 2,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(name, personName, theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

// main
const Management = (props) => {
  const classes = useStyles();

  // table states
  const [rows, setRows] = useState([]);
  const [listData, setListData] = useState([]);
  //notify
  const [notifySetting, setNotifySetting] = useState({
    isSuccess: true,
    message: "",
  });
  const [isOpen, setIsOpen] = useState(false);
  const handleClose = () => {
    setIsOpen(false);
  };
  const [totalRecords, setTotalRecords] = useState(0);

  const [listHDTS, setListHDTS] = useState([]);
  const [thoiGianBatDau, setThoiGianBatDau] = useState(Date().toLocaleString());
  const [thoiGianKetThuc, setThoiGianKetThuc] = useState(
    Date().toLocaleString()
  );
  const DataStart = {
    tongChiTieu: "",
    tongHoSo: "",
    tongTrungTuyen: "",
    tongBiLoai: "",
    thoiGianBatDau: Date().toLocaleString(),
    thoiGianKetThuc: Date().toLocaleString(),
    idNguoiTao: 1,
    idCoSoGiaoDuc: 1,
    idChuTichHDTS: "",
  };
  // // get data
  const handleFetchManagement = (searchKey = "", pageNumber = 1, pageSize = 5)=>{
    const fetchData = async () => {
      const params = {
        keyword: searchKey,
        pageNumber: pageNumber,
        pageSize: pageSize,
      };
      await axios
        .post(IP_API + "/BaoCao/get-paged-bao-cao", params, options_ticket)
        .then((reponse) => {
          // Call API success
          console.log(reponse)
          if (reponse && reponse.data) {
            setListData(reponse.data.data);
            setTotalRecords(reponse.data.totalCount);
          }
        })
        .catch((error) => {
          // Faild Server to call api
        });
    };
    fetchData()
  }
  useEffect(() => {
    handleFetchManagement();
  }, []);

  // get table rows
  useEffect(() => {
    let renderedItems;
    if (listData) {
      renderedItems = listData.map((item) => {
        let {
          thoiGianBatDau,
          thoiGianKetThuc,
          tongChiTieu,
          tongHoSo,
          tongTrungTuyen,
          tongBiLoai,
        } = item;
        thoiGianBatDau = moment(thoiGianBatDau).format("DD/MM/YYYY");
        thoiGianKetThuc = moment(thoiGianKetThuc).format("DD/MM/YYYY");
        return {
          thoiGianBatDau,
          thoiGianKetThuc,
          tongChiTieu,
          tongHoSo,
          tongTrungTuyen,
          tongBiLoai,
        };
      });
      setRows(renderedItems);
    }
  }, [listData]);
  useEffect(() => {
    // Get Api Đợt tuyển sinh
    // Sẽ cấu hình tự lấy
    if (!listHDTS || listHDTS.length === 0) {
      const params_ID_CSGD = 1;
      const fetchData = async () => {
        await axios
          .post(
            IP_API +
              "/HoiDongTuyenSinh/get-list-hdts-by-idcsgd?idCSGD=" +
              params_ID_CSGD,
            null,
            options_ticket
          )
          .then((reponse) => {
            // Call API success
            if (reponse && reponse.data) {
              setListHDTS(reponse.data);
            }
          })
          .catch((error) => {
            // Faild Server to call api
          });
      };
      fetchData();
    }
  });

  const onSubmit = (data, actions) => {
    // Call api add
    const params = {
      thoiGianBatDau: new Date(data.thoiGianBatDau),
      thoiGianKetThuc: new Date(data.thoiGianKetThuc),
      thoiGianTao: new Date(),
      tongChiTieu: parseInt(data.tongChiTieu),
      tongHoSo: parseInt(data.tongHoSo),
      tongTrungTuyen: parseInt(data.tongTrungTuyen),
      tongBiLoai: parseInt(data.tongBiLoai),
      bcThuanLoi: "",
      bcKhoKhan: "",
      bcDeXuat: "",
      bcNoiDungKhac: "",
      // sau sẽ lấy tự động theo user đăng nhập
      idCoSoGiaoDuc: data.idCoSoGiaoDuc,
      idChuTichHDTS: data.idChuTichHDTS,
      // sau sẽ lấy tự động theo user đăng nhập
      idNguoiTao: data.idNguoiTao,
      dsThanhVienHDTS: "",
    };
    const postData = async () => {
      await axios
        .post(IP_API + "/BaoCao/add-bao-cao", params, options_ticket)
        .then((reponse) => {
          setIsOpen(true);
          if (reponse && reponse.status === 200) {
            setNotifySetting({
              isSuccess: true,
              message: "Thêm mới thành công",
            });
          } else {
            setNotifySetting({
              isSuccess: false,
              message: "Thêm mới thất bại",
            });
          }
          actions.resetForm({ values: DataStart });
          handleClearInput();
        })
        .catch((error) => {
          setIsOpen(true);
          setNotifySetting({
            isSuccess: false,
            message: "Lỗi máy chủ xin vui lòng thử lại !",
          });
        });
    };
    postData();
  };
  const handleClearInput = () => {
    setThoiGianBatDau(Date().toLocaleString());
    setThoiGianBatDau(Date().toLocaleString());
  };

  // console.log(typeof startDate);

  return (
    <Fragment>
      <Box px={3} pb={5} pt={3}>
        <Box mb={5}>
          <Typography variant="h6" align="center" margin="dense">
            QUẢN LÝ CÁC ĐỢT TUYỂN SINH
          </Typography>
        </Box>
        <Box mb={5}>
          <Typography variant="h6" align="left" margin="dense">
            Các đợt tuyển sinh trong năm
          </Typography>
          <DataTableCustom
            columns={columns}
            rows={rows}
            openTooltip={false}
            searchbar={false}
            totalRecords={totalRecords}
            handleSearch={handleFetchManagement}
          />
        </Box>
        <Formik
          //gia tri khoi tao
          initialValues={DataStart}
          //validation
          validationSchema={Yup.object().shape({
            tongChiTieu: Yup.string("").required("Vui lòng nhập thông tin"),
            tongHoSo: Yup.string("").required("Vui lòng nhập thông tin"),
            tongTrungTuyen: Yup.string("").required("Vui lòng nhập thông tin"),
            tongBiLoai: Yup.string("").required("Vui lòng nhập thông tin"),
            thoiGianBatDau: Yup.string("").required("Vui lòng nhập thông tin"),
            thoiGianKetThuc: Yup.string("").required("Vui lòng nhập thông tin"),
            idChuTichHDTS: Yup.string("").required("Vui lòng chọn thông tin"),
          })}
          //khi submit
          onSubmit={(values, actions) => {
            onSubmit(values, actions);
          }}
        >
          {/* render */}
          {({
            values,
            errors,
            touched,
            isSubmitting,
            handleChange,
            handleSubmit,
            handleBlur,
            setFieldValue,
          }) => (
            <form onSubmit={handleSubmit}>
              <Box mb={7} border={1} p={3} borderColor="grey.500">
                <Grid
                  container
                  spacing={3}
                  justifyContent="center"
                  alignItems="center"
                >
                  <Grid item xs={3}>
                    <Box pt={3} ml={5}>
                      <Typography>Tổng chỉ tiêu tuyển sinh sự kiến:</Typography>
                    </Box>
                  </Grid>
                  <Grid item xs={9} p={0}>
                    <TextField
                      required
                      label="Tổng chỉ tiêu tuyển sinh sự kiến"
                      id="tongChiTieu"
                      onChange={handleChange}
                      name="tongChiTieu"
                      fullWidth={true}
                      onBlur={handleBlur}
                      error={errors.tongChiTieu ? true : false}
                      value={values.tongChiTieu}
                      type="number"
                    />
                    <Grid item xs={12} sm={12}>
                      <Typography variant="inherit" color="error">
                        {errors.tongChiTieu ? errors.tongChiTieu : ""}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={3}
                  justifyContent="center"
                  alignItems="center"
                >
                  <Grid item xs={3}>
                    <Box pt={3} ml={5}>
                      <Typography>Tổng hồ sơ:</Typography>
                    </Box>
                  </Grid>
                  <Grid item xs={9} p={0}>
                    <TextField
                      required
                      label="Tổng hồ sơ"
                      id="tongHoSo"
                      onChange={handleChange}
                      name="tongHoSo"
                      fullWidth={true}
                      onBlur={handleBlur}
                      error={errors.tongHoSo ? true : false}
                      type="number"
                      value={values.tongHoSo}
                    />
                    <Grid item xs={12} sm={12}>
                      <Typography variant="inherit" color="error">
                        {errors.tongHoSo ? errors.tongHoSo : ""}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid
                  container
                  spacing={3}
                  justifyContent="center"
                  alignItems="center"
                >
                  <Grid item xs={3}>
                    <Box pt={3} ml={5}>
                      <Typography>Tổng trúng tuyển:</Typography>
                    </Box>
                  </Grid>
                  <Grid item xs={9} p={0}>
                    <TextField
                      required
                      label="Tổng trúng tuyển"
                      id="tongTrungTuyen"
                      onChange={handleChange}
                      name="tongTrungTuyen"
                      fullWidth={true}
                      onBlur={handleBlur}
                      error={errors.tongTrungTuyen ? true : false}
                      type="number"
                      value={values.tongTrungTuyen}
                    />
                    <Grid item xs={12} sm={12}>
                      <Typography variant="inherit" color="error">
                        {errors.tongTrungTuyen ? errors.tongTrungTuyen : ""}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                {/* Row 1: end */}
                <Grid
                  container
                  spacing={3}
                  justifyContent="center"
                  alignItems="center"
                >
                  <Grid item xs={3}>
                    <Box pt={3} ml={5}>
                      <Typography>Tổng bị loại:</Typography>
                    </Box>
                  </Grid>
                  <Grid item xs={9} p={0}>
                    <TextField
                      required
                      label="Tổng bị loại"
                      id="tongBiLoai"
                      onChange={handleChange}
                      name="tongBiLoai"
                      fullWidth={true}
                      onBlur={handleBlur}
                      error={errors.tongBiLoai ? true : false}
                      type="number"
                      value={values.tongBiLoai}
                    />
                    <Grid item xs={12} sm={12}>
                      <Typography variant="inherit" color="error">
                        {errors.tongBiLoai ? errors.tongBiLoai : ""}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                {/* Row 1: end */}

                {/* Row 2: start */}
                <Grid
                  container
                  spacing={3}
                  justifyContent="center"
                  alignItems="center"
                >
                  <Grid item xs={3}>
                    <Box mt={3} ml={5}>
                      <Typography>Thời gian bắt đầu dự kiến:</Typography>
                    </Box>
                  </Grid>
                  <Grid item xs={9} p={0}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <Grid container justifyContent="flex-start">
                        <KeyboardDatePicker
                          disableToolbar
                          variant="inline"
                          format="dd/MM/yyyy"
                          margin="normal"
                          id="date-picker-inline"
                          label="Thời gian bắt đầu dự kiến"
                          name="thoiGianBatDau"
                          value={thoiGianBatDau}
                          onChange={(val) => {
                            setThoiGianBatDau(val);
                          }}
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                        />
                      </Grid>
                    </MuiPickersUtilsProvider>
                    <Grid item xs={12} sm={12}>
                      <Typography variant="inherit" color="error">
                        {errors.thoiGianBatDau ? errors.thoiGianBatDau : ""}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                {/* Row 2: end */}

                {/* Row 3: start */}
                <Grid
                  container
                  spacing={1}
                  justifyContent="center"
                  alignItems="center"
                >
                  <Grid item xs={3}>
                    <Box mt={3} ml={5}>
                      <Typography>Thời gian kết thúc dự kiến:</Typography>
                    </Box>
                  </Grid>
                  <Grid item xs={9} p={0}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <Grid container justifyContent="flex-start">
                        <KeyboardDatePicker
                          disableToolbar
                          variant="inline"
                          format="dd/MM/yyyy"
                          margin="normal"
                          id="date-picker-inline"
                          label="Thời gian kết thúc dự kiến"
                          name="thoiGianKetThuc"
                          value={thoiGianKetThuc}
                          onChange={(val) => {
                            setThoiGianKetThuc(val);
                          }}
                          KeyboardButtonProps={{
                            "aria-label": "change date",
                          }}
                        />
                      </Grid>
                    </MuiPickersUtilsProvider>

                    <Grid item xs={12} sm={12}>
                      <Typography variant="inherit" color="error">
                        {errors.thoiGianKetThuc ? errors.thoiGianKetThuc : ""}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                {/* Row 3: end */}

                {/* Row 4: start */}
                <Grid
                  container
                  spacing={3}
                  justifyContent="center"
                  alignItems="center"
                >
                  <Grid item xs={3}>
                    <Box pt={3} ml={5}>
                      <Typography>Chủ tịch HĐTS:</Typography>
                    </Box>
                  </Grid>
                  <Grid item xs>
                    <FormControl
                      variant="filled"
                      className={classes.formControl}
                    >
                      <Select
                        id="demo-simple-select-filled"
                        name="idChuTichHDTS"
                        onChange={handleChange}
                        value={values.idChuTichHDTS}
                      >
                        <MenuItem disabled value={0}>
                          Chọn
                        </MenuItem>
                        {listHDTS && listHDTS.length
                          ? listHDTS.map((item, index) => {
                              return (
                                <MenuItem value={item.idNguoiDung} key={index}>
                                  {item.tenNguoiDung}
                                </MenuItem>
                              );
                            })
                          : ""}
                      </Select>
                      <Grid item xs={12} sm={12}>
                        <Typography variant="inherit" color="error">
                          {errors.idChuTichHDTS ? errors.idChuTichHDTS : ""}
                        </Typography>
                      </Grid>
                    </FormControl>

                    {/* <Grid item xs={12} sm={12}>
                <Typography variant="inherit" color="error">
                  {errors.endDate?.message}
                </Typography>
              </Grid> */}
                  </Grid>
                </Grid>
                {/* Row 4: end */}

                {/* Submit Button */}
                <Grid
                  container
                  spacing={1}
                  justifyContent="flex-start"
                  alignItems="center"
                >
                  <Box mt={3} ml={5}>
                    <Grid item xs={1}>
                      <Button variant="contained" color="primary" type="submit">
                        Lưu
                      </Button>
                    </Grid>
                  </Box>
                  <Box mt={3} ml={3}>
                    <Grid item xs={1}>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={handleClearInput}
                      >
                        Hủy
                      </Button>
                    </Grid>
                  </Box>
                </Grid>
              </Box>
            </form>
          )}
        </Formik>
        <Snackbar open={isOpen} autoHideDuration={1000} onClose={handleClose}>
          <Alert
            onClose={handleClose}
            severity={notifySetting.isSuccess ? "success" : "error"}
          >
            {notifySetting.message}
          </Alert>
        </Snackbar>
      </Box>
    </Fragment>
  );
};

export default Management;
