import React from "react";
import { Route } from "react-router-dom";
import axios from "axios";
// import { options_ticket, IP_API } from "./../helper/helper";
const routeWapper = (Layout, Page) => {
  const LayoutSwitcher = (props) => (
    <Layout>
      <Page {...props} />
    </Layout>
  );
  return LayoutSwitcher;
};

const SeduRoutes = (props) => {
  const { layout, page, path, exact, ...rest } = props;
  // let history = useHistory();
  if (!localStorage.getItem("ticket") && !path.includes('login-callback') && process.env.NODE_ENV === 'production') {
		// history.push("/login-callback")
		window.location.href =
			'https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback';
	}
  // axios
  //   .get(`http://180.93.15.29:8000/NguoiDung/healthcheck`, options_ticket)
  //   .then((response) => {})
  //   .catch((error) => {
  //     if (error.response.status===401) {
  //       window.location.href =
  //         "https://dangnhap.hagiang.gov.vn/cas/login?service=http://sedu.tsdc.vn:3000/login-callback";
  //     }
  //   });
  return (
    <Route exact={exact} path={path}>
      {routeWapper(layout, page)(rest)}
    </Route>
  );
};

export default SeduRoutes;
