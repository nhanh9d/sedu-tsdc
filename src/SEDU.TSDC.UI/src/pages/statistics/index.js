import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {
  TextField,
  Typography,
  Button,
  Box,
  Grid,
  Paper
} from '@material-ui/core';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

// import { makeStyles } from '@material-ui/core/styles';

// const useStyles = makeStyles((theme) => ({
//   root: {
//     '& .MuiTextField-root': {
//       margin: theme.spacing(1),
//     },
//   },
// }));
const Statistics = (props) => {
  const [selectedDateStart, setSelectedDateStart] = React.useState(new Date(Date.now()));
  const [selectedDateEnd, setSelectedDateEnd] = React.useState(null);

  // const classes = useStyles();

  const handleDateChangeStart = (date) => {
    setSelectedDateStart(date);
  };
  const handleDateChangeEnd = (date) => {
    setSelectedDateEnd(date);
  };
  const validationSchema = Yup.object({
    // email: Yup
    //   .string('Enter your email')
    //   .email('Enter a valid email')
    //   .required('Email is required'),
    // password: Yup
    //   .string('Enter your password')
    //   .min(8, 'Password should be of minimum 8 characters length')
    //   .required('Password is required'),
    textAreaThuanLoi: Yup.string('').required('Không Được Bỏ Trống'),
    textAreaKhoKhan: Yup.string('').required('Không Được Bỏ Trống'),
    textAreaDeXuat: Yup.string('').required('Không Được Bỏ Trống'),
    textAreaKhac: Yup.string('').required('Không Được Bỏ Trống')

  });
  const formik = useFormik({
    initialValues: {
      textAreaThuanLoi: "",
      textAreaKhoKhan: "",
      textAreaDeXuat: "",
      textAreaKhac: "",
      // email: 'foobar@example.com',
      // password: 'foobar',
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      console.log(JSON.stringify(values))
    },
  });
  return (
    <>
        <Box mt={4} px={4} >
          <Paper>
            <Box >
              <Typography variant="h6" align="center" margin="dense">
                BÁO CÁO - THỐNG KÊ
              </Typography>
              <Box p={2}>
              <form onSubmit={formik.handleSubmit}>
                {/* Date Picker */}
                <Grid container
                  alignItems="center">
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <Grid item xs={3}>
                      <Typography variant="body1" align="left" margin="dense">
                        Chọn Đợt Tuyển Sinh
                      </Typography>
                    </Grid>
                    <Grid item xs={3}>
                      <KeyboardDatePicker
                        style={{ margin: 0 }}
                        disableToolbar
                        variant="inline"
                        format="dd/MM/yyyy"
                        margin="normal"
                        id="data-from"
                        label="Từ Ngày"
                        value={selectedDateStart}
                        onChange={handleDateChangeStart}
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                      />

                    </Grid>
                    <Grid item xs={3}>
                      <KeyboardDatePicker
                        style={{ margin: 0 }}
                        variant="inline"
                        format="dd/MM/yyyy"
                        margin="normal"
                        id="data-to"
                        label="Đến Ngày"
                        value={selectedDateEnd}
                        onChange={handleDateChangeEnd}
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                      />
                    </Grid>
                    <Grid item xs={3}>
                      <Button color="secondary" variant="contained" fullWidth type="button" onClick={() => { console.log('test') }}>Tải Dữ Liệu Tuyển Sinh</Button>
                    </Grid>
                  </MuiPickersUtilsProvider>

                </Grid>
                {/* text chi tieu  */}
                <Box my={3} >
                  <Grid container>
                    <Grid item xs={3}>
                      <Typography variant="body1" align="left" margin="dense">
                        Tổng Chỉ Tiêu Tuyển Sinh:
                      </Typography>
                    </Grid>
                    <Grid item xs={3}>
                      <Typography variant="body1" align="left" margin="dense">
                        {props.text || "100 chỉ tiêu"}
                      </Typography>
                    </Grid>
                    <Grid item xs={3}>
                      <Typography variant="body1" align="left" margin="dense">
                        Tổng hồ Sơ Dự Tuyển:
                      </Typography>
                    </Grid>
                    <Grid item xs={2}>
                      <Typography variant="body1" align="left" margin="dense">
                        {props.text || "50 Hồ Sơ"}
                      </Typography>
                    </Grid>
                  </Grid>
                </Box>
                <Box mb={3}>
                  <Grid container>
                    <Grid item xs={3}>
                      <Typography variant="body1" align="left" margin="dense">
                        Tổng Số Hồ Sơ Trúng Tuyển:
                      </Typography>
                    </Grid>
                    <Grid item xs={3}>
                      <Typography variant="body1" align="left" margin="dense">
                        {props.text || "40 Hồ Sơ"}
                      </Typography>
                    </Grid>
                    <Grid item xs={3}>
                      <Typography variant="body1" align="left" margin="dense">
                        Tổng hồ Sơ Bị Loại:
                      </Typography>
                    </Grid>
                    <Grid item xs={2}>
                      <Typography variant="body1" align="left" margin="dense">
                        {props.text || "40 Hồ Sơ"}
                      </Typography>
                    </Grid>
                  </Grid>
                </Box>
                <Typography>Thuận Lợi</Typography>
                <TextField
                  style={{ width: 100 + "%" }}
                  rows={5}
                  variant="filled"
                  multiline
                  id="textAreaThuanLoi"
                  name="textAreaThuanLoi"
                  label="Thuận Lợi"
                  value={formik.values.textAreaThuanLoi}
                  onInput={formik.handleChange}
                  error={formik.touched.textAreaThuanLoi && Boolean(formik.errors.textAreaThuanLoi)}
                  helperText={formik.touched.textAreaThuanLoi && formik.errors.textAreaThuanLoi}
                />
                <Box mt={3}>
                  <Typography>Khó Khăn</Typography>
                  <TextField
                    style={{ width: 100 + "%" }}
                    rows={5}
                    multiline
                    variant="filled"
                    id="textAreaKhoKhan "
                    name="textAreaKhoKhan"
                    label="Khó Khăn"
                    value={formik.values.textAreaKhoKhan}
                    onInput={formik.handleChange}
                    error={formik.touched.textAreaKhoKhan && Boolean(formik.errors.textAreaKhoKhan)}
                    helperText={formik.touched.textAreaKhoKhan && formik.errors.textAreaKhoKhan}
                  />
                </Box>
                <Box mt={3}>
                  <Typography>Đề Xuất</Typography>
                  <TextField
                    style={{ width: 100 + "%" }}
                    rows={5}
                    multiline
                    variant="filled"
                    id="textAreaDeXuat"
                    name="textAreaDeXuat"
                    label="Đề Xuất"
                    value={formik.values.textAreaDeXuat}
                    onInput={formik.handleChange}
                    error={formik.touched.textAreaDeXuat && Boolean(formik.errors.textAreaDeXuat)}
                    helperText={formik.touched.textAreaDeXuat && formik.errors.textAreaDeXuat}
                  />
                </Box>
                <Box mt={3}>
                  <Typography>Các Nội dung khác</Typography>
                  <TextField
                    style={{ width: 100 + "%" }}
                    rows={5}
                    multiline
                    variant="filled"
                    id="textAreaKhac"
                    name="textAreaKhac"
                    label="Khác"
                    value={formik.values.textAreaKhac}
                    onInput={formik.handleChange}
                    error={formik.touched.textAreaKhac && Boolean(formik.errors.textAreaKhac)}
                    helperText={formik.touched.textAreaKhac && formik.errors.textAreaKhac}
                  />
                </Box>
                {/* <TextField
            fullWidth
            id="email"
            name="email"
            label="Email"
            value={formik.values.email}
            onChange={formik.handleChange}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
          />
          <TextField
            fullWidth
            id="password"
            name="password"
            label="Password"
            type="password"
            value={formik.values.password}
            onChange={formik.handleChange}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password && formik.errors.password}
          /> */}
                <Box my={3} style={{ display: "flex" }}>
                  <Button color="primary" variant="contained" type="submit">
                    Lưu Báo Cáo
                  </Button>
                  <Box ml={3}>
                    <Button color="primary" variant="contained" type="button">
                      Tải Xuống
                    </Button>
                  </Box>
                </Box>
              </form>
              </Box>
            </Box>
          </Paper>
        </Box>

    </>
  )
}
export default Statistics;
