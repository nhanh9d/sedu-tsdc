﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEDU.TSDC.EntityFramework.Entities
{
    public class XET_DUYET_HO_SO : BASE_ENTITY
    {
        /// <summary>
        /// Thời gian duyệt hồ sơ
        /// </summary>
        public DateTime THOI_GIAN_DUYET { get; set; }
        /// <summary>
        /// Địa chỉ IP của người duyệt hồ sơ.
        /// </summary>
        [MaxLength(50, ErrorMessage = "Địa chỉ IP của người duyệt hồ sơ tối đa 50 kí tự")]
        public string IP_DUYET { get; set; }
        /// <summary>
        /// 1: Từ chối
        /// 2: Đồng ý
        /// </summary>
        public int KET_QUA { get; set; }
        /// <summary>
        /// Lý do bị loại (nếu có)
        /// </summary>
        public string GHI_CHU { get; set; }

        #region Relation props
        /// <summary>
        /// id hồ sơ, tham chiếu tới ho_so.id
        /// </summary>
        public int ID_HO_SO { get; set; }
        public virtual HO_SO HO_SO { get; set; }
        /// <summary>
        /// ID của người duyệt hồ sơ trong bảng nguoi_dung
        /// </summary>
        public int ID_NGUOI_DUYET { get; set; }
        public virtual NGUOI_DUNG NGUOI_DUYET { get; set; }
        #endregion

    }
}
