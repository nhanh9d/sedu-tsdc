import { Box } from '@material-ui/core';
import React from 'react';
import styles from './Footer.module.css';

const Footer = () => {
	return (
		<Box pt={4}>
			<div className={styles['footer']}>
				<Box px={4} pt={4}>
					<h2>
						Copyright (<i className="fas fa-copyright"></i>) 2021 by TSC
					</h2>
				</Box>
			</div>
		</Box>
	);
};

export default Footer;
