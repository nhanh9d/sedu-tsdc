﻿using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Services.Interface
{
    public interface IXetDuyetHoSoService
    {
        public Task<XetDuyetHoSoDto> AddXetDuyetHoSoAsync(XetDuyetHoSoDto xetDuyetHoSoDto);
        public Task<XetDuyetHoSoDto> GetXetDuyetHoSoByIdAsync(int xetDuyetHoSoId);
        public Task<XetDuyetHoSoDto> EditXetDuyetHoSoAsync(XetDuyetHoSoDto xetDuyetHoSoDto);
        public Task<int> DeleteXetDuyetHoSoAsync(int xetDuyetHoSoId);
        Task<PagedList<XetDuyetHoSoDto>> GetPagedXetDuyetHoSoAsync(string keyword, int pageNumber, int pageSize);
        Task<PagedList<XetDuyetHoSoDto>> GetPagedXetDuyetHoSoAsync(string trang_thai_duyet_ho_so, bool trang_thai_nhap_hoc, int nam_xet_tuyen, string keyword, int pageNumber, int pageSize);
    }
}
