﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SEDU.TSDC.API.Mappers;
using SEDU.TSDC.API.Parameters;
using SEDU.TSDC.API.Parameters.VaiTro;
using SEDU.TSDC.Services.Dtos.Tsdc;
using SEDU.TSDC.Services.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors("SeduCors")]
    public class VaiTroController : ControllerBase
    {

        private readonly ILogger<VaiTroController> _logger;
        private readonly IVaiTroService _vaiTroService;

        public VaiTroController(ILogger<VaiTroController> logger, IVaiTroService vaiTroService)
        {
            _logger = logger;
            _vaiTroService = vaiTroService;
        }

        [Route("healthcheck")]
        [HttpGet]
        public string HealthCheck()
        {
            return "Ok";
        }

        [Route("add-vai-tro")]
        [HttpPost]
        public async Task<VaiTroDto> AddVaiTro(CreateVaiTroParams pr)
        {
            var dto = pr.ToDto();
            return await _vaiTroService.AddVaiTroAsync(dto);
        }

        [Route("get-vai-tro-by-id")]
        [HttpPost]
        public async Task<object> GetVaiTroById(int vaiTroId)
        {
            try
            {
                return await _vaiTroService.GetVaiTroByIdAsync(vaiTroId);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [Route("edit-vai-tro")]
        [HttpPost]
        public async Task<VaiTroDto> EditVaiTro(EditVaiTroParams pr)
        {
            var dto = pr.ToDto();
            return await _vaiTroService.EditVaiTroAsync(dto);
        }

        [Route("delete-vai-tro")]
        [HttpPost]
        public async Task<int> DeleteVaiTro(int vaiTroId)
        {
            return await _vaiTroService.DeleteVaiTroAsync(vaiTroId);
        }
        [Route("get-paged-vai-tro")]
        [HttpPost]
        public async Task<object> GetPagedNguoiDung(PagedParams pr)
        {
            var lst = await _vaiTroService.GetPagedVaiTroAsync(pr.keyword, pr.pageNumber, pr.pageSize);
            return lst;
        }
    }
}
