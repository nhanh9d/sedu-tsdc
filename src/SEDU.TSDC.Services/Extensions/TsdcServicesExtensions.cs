﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SEDU.TSDC.EntityFramework.Configuration.Configuration;
using SEDU.TSDC.EntityFramework.Configuration.PostgreSql;
using SEDU.TSDC.EntityFramework.DbContexts.Interfaces;
using SEDU.TSDC.EntityFramework.Repositories;
using SEDU.TSDC.EntityFramework.Repositories.Interfaces;
using SEDU.TSDC.Services.Services;
using SEDU.TSDC.Services.Services.Interface;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class TsdcServicesExtensions
    {
        /// <summary>
        /// Inject các service cần thiết
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddTsdcServices<TTsdcDbContext>(this IServiceCollection services, IConfiguration configuration)
            where TTsdcDbContext : DbContext, ITsdcDbContext
        {
            //services
            services.AddTransient<IBaoCaoService, BaoCaoService>();
            services.AddTransient<ICoSoGiaoDucService, CoSoGiaoDucService>();
            services.AddTransient<INguoiDungService, NguoiDungService>();
            services.AddTransient<IHoiDongTuyenSinhService, HoiDongTuyenSinhService>();
            services.AddTransient<IHoSoService, HoSoService>();
            services.AddTransient<IVaiTroService, VaiTroService>();
            services.AddTransient<IXetDuyetHoSoService, XetDuyetHoSoService>();
            //services.AddTransient<IActivityService, ActivityService>();
            //services.AddTransient<IConnectorService, ConnectorService>();
            //services.AddTransient<IViettelService, ViettelService>();

            //repositories
            services.AddTransient<IBaoCaoRepository, BaoCaoRepository<TTsdcDbContext>>();
            services.AddTransient<ICoSoGiaoDucRepository, CoSoGiaoDucRepository<TTsdcDbContext>>();
            services.AddTransient<INguoiDungRepository, NguoiDungRepository<TTsdcDbContext>>();
            services.AddTransient<IHoiDongTuyenSinhRepository, HoiDongTuyenSinhRepository<TTsdcDbContext>>();
            services.AddTransient<IHoSoRepository, HoSoRepository<TTsdcDbContext>>();
            services.AddTransient<IVaiTroRepository, VaiTroRepository<TTsdcDbContext>>();
            services.AddTransient<IXetDuyetHoSoRepository, XetDuyetHoSoRepository<TTsdcDbContext>>();
            //services.AddTransient<IActivityRepository, ActivityRepository<TTsdcDbContext>>();
            //services.AddTransient<IConnectorRepository, ConnectorRepository<TTsdcDbContext>>();
            return services;
        }
        /// <summary>
        /// Inject các service cần thiết
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddDbContexts<TDbContext>(this IServiceCollection services, 
            ConnectionStringsConfiguration connectionStrings,
            DatabaseMigrationsConfiguration databaseMigrations) 
            where TDbContext : DbContext, ITsdcDbContext
        {
            //services
            services.RegisterPostgreSqlDbContexts<TDbContext>(connectionStrings, databaseMigrations);

            return services;
        }
    }
}
