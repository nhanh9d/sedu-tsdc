﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Filters
{
    public class SeduFilter : IAsyncActionFilter
    {

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            // var ticket = context.HttpContext.Request.Headers["Ticket"];
            // //Kiểm tra xem có truyền header Authorization không
            // var isValid = ticket.Count() > 0;
            // var result = string.Empty;

            // if (isValid)
            // {
            //     HttpClient client = new();
            //     var response = await client.GetAsync($"https://dangnhap.hagiang.gov.vn/cas/serviceValidate?ticket={ticket[0]}&service=http://sedu.tsdc.vn:3000/login-callback");
            //     isValid = response.IsSuccessStatusCode;
            //     if (isValid)
            //     {
            //         result = await response.Content.ReadAsStringAsync();
            //     }
            // }

            // isValid = isValid && !result.Contains("authenticationFailure");

            // if (!isValid)
            // {
            //     context.Result = new UnauthorizedResult();
            //     return;
            // }

            _ = await next();
        }
    }
}
