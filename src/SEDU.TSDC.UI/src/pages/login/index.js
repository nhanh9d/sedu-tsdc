import React from "react";
import { useHistory } from "react-router-dom";

const LoginCallback = (props) => {
  const history = useHistory();
  const tk = window.location.href.split("=")[1];
  localStorage.setItem('ticket', tk);
  history.push("/");
  
  return (
    <>
      <div>Đang quay trở lại trang chủ</div>
    </>
  );
};

export default LoginCallback;
