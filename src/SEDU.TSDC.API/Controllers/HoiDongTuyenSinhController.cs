﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SEDU.TSDC.API.Mappers;
using SEDU.TSDC.API.Parameters;
using SEDU.TSDC.API.Parameters.HoiDongTuyenSinh;
using SEDU.TSDC.Services.Dtos.Tsdc;
using SEDU.TSDC.Services.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors("SeduCors")]
    public class HoiDongTuyenSinhController : ControllerBase
    {

        private readonly ILogger<HoiDongTuyenSinhController> _logger;
        private readonly IHoiDongTuyenSinhService _hoiDongTuyenSinhService;

        public HoiDongTuyenSinhController(ILogger<HoiDongTuyenSinhController> logger, IHoiDongTuyenSinhService hoiDongTuyenSinhService)
        {
            _logger = logger;
            _hoiDongTuyenSinhService = hoiDongTuyenSinhService;
        }

        [Route("healthcheck")]
        [HttpGet]
        public string HealthCheck()
        {
            return "Ok";
        }

        [Route("add-hoi-dong-tuyen-sinh")]
        [HttpPost]
        public async Task<HoiDongTuyenSinhDto> AddHoiDongTuyenSinh(CreateHoiDongTuyenSinhParams pr)
        {
            var dto = pr.ToDto();
            return await _hoiDongTuyenSinhService.AddHoiDongTuyenSinhAsync(dto);
        }
        [Route("quick-add-hoi-dong-tuyen-sinh")]
        [HttpPost]
        public async Task<HoiDongTuyenSinhDto> QuickAddHoiDongTuyenSinh(QuickAddHDTSParams pr)
        {
            var dto = pr.ToDto();
            dto.VaiTro_QuickAdd = pr.TenVaiTro;
            dto.TenNguoiDung = pr.TenNguoiDung;
            dto.SoDienThoai = pr.SoDienThoai;
            return await _hoiDongTuyenSinhService.AddHoiDongTuyenSinhAsync(dto);
        }

        [Route("get-hoi-dong-tuyen-sinh-by-id")]
        [HttpPost]
        public async Task<object> GetHoiDongTuyenSinhById(int hoiDongTuyenSinhId)
        {
            try
            {
                return await _hoiDongTuyenSinhService.GetHoiDongTuyenSinhByIdAsync(hoiDongTuyenSinhId);
            }
            catch (Exception ex)
            {
                return ex;
            }
        }

        [Route("edit-hoi-dong-tuyen-sinh")]
        [HttpPost]
        public async Task<HoiDongTuyenSinhDto> EditHoiDongTuyenSinh(EditHoiDongTuyenSinhParams pr)
        {
            var dto = pr.ToDto();
            return await _hoiDongTuyenSinhService.EditHoiDongTuyenSinhAsync(dto);
        }
        [Route("edit-hoi-dong-tuyen-sinh-va-nguoi-dung")]
        [HttpPost]
        public async Task<HoiDongTuyenSinhDto> EditHDTSWithUser(EditHDTSWithUserParams pr)
        {
            var dto = pr.ToDto();
            dto.SoDienThoai = pr.SoDienThoai;
            dto.TenNguoiDung = pr.TenNguoiDung;
            dto.Id = pr.Id;
            return await _hoiDongTuyenSinhService.EditHDTSWithUserAsync(dto);
        }

        [Route("delete-hoi-dong-tuyen-sinh")]
        [HttpPost]
        public async Task<int> DeleteHoiDongTuyenSinh(int hoiDongTuyenSinhId)
        {
            return await _hoiDongTuyenSinhService.DeleteHoiDongTuyenSinhAsync(hoiDongTuyenSinhId);
        }
        [Route("get-paged-hoi-dong-tuyen-sinh")]
        [HttpPost]
        public async Task<object> GetPagedHoiDongTuyenSinh(PagedParams pr)
        {
            var lst = await _hoiDongTuyenSinhService.GetPagedHoiDongTuyenSinhAsync(pr.keyword, pr.pageNumber, pr.pageSize);
            return lst;
        }
        [Route("get-list-hdts-by-idcsgd")]
        [HttpPost]
        public async Task<object> GetListHDTSByIdCSGD(int idCSGD)
        {
            var lst = await _hoiDongTuyenSinhService.GetListHDTSByIdCSGD(idCSGD);
            return lst;
        }
        [Route("them-nhieu-hoi-dong-tuyen-sinh")]
        [HttpPost]
        public async Task<object> ThemNhieuHoiDongTuyenSinh(List<ThemNhieuHoiDongTuyenSinhParams> pr)
        {
            var lst = await _hoiDongTuyenSinhService.ThemNhieuHoiDongTuyenSinh(pr.ToDto());
            return lst;
        }
    }
}