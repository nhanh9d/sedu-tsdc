﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Shared.Validation
{
    public class StringValidation
    {
        /// <summary>
        /// Kiểm tra số điện thoại
        /// </summary>
        /// <returns></returns>
        public static bool CheckSoDienThoai(string soDienThoai)
        {
            var vnf_regex = "^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$";
            if (soDienThoai != "")
            {
                var result = Regex.Match(soDienThoai, vnf_regex);
                return Regex.Match(soDienThoai, vnf_regex).Success;
            }
            return false;
        }
    }
}
