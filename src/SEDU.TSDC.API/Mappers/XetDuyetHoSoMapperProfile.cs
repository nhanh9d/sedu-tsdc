﻿using AutoMapper;
using SEDU.TSDC.API.Parameters.XetDuyetHoSo;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.Services.Dtos.Tsdc;
using System;

namespace SEDU.TSDC.API.Mappers
{
    public class XetDuyetHoSoMapperProfile : Profile
    {
        public XetDuyetHoSoMapperProfile()
        {
            CreateMap<CreateXetDuyetHoSoParams, XetDuyetHoSoDto>(MemberList.Destination);
            CreateMap<EditXetDuyetHoSoParams, XetDuyetHoSoDto>(MemberList.Destination);
        }
    }
}
