﻿using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.EntityFramework.Repositories.Interfaces;
using SEDU.TSDC.Services.Dtos.Tsdc;
using SEDU.TSDC.Services.Mappers;
using SEDU.TSDC.Services.Services.Interface;
using System;
using System.Threading.Tasks;

namespace SEDU.TSDC.Services.Services
{
    public class CoSoGiaoDucService : ICoSoGiaoDucService
    {
        private readonly ICoSoGiaoDucRepository coSoGiaoDucRepository;

        public CoSoGiaoDucService(ICoSoGiaoDucRepository _coSoGiaoDucRepository)
        {
            coSoGiaoDucRepository = _coSoGiaoDucRepository;
            // To save physical files to a path provided by configuration:
        }

        public async Task<CoSoGiaoDucDto> AddCoSoGiaoDucAsync(CoSoGiaoDucDto coSoGiaoDuc)
        {
            var coSoGiaoDucEntity = coSoGiaoDuc.ToEntity();
            coSoGiaoDucEntity = await coSoGiaoDucRepository.CreateCoSoGiaoDucAsync(coSoGiaoDucEntity);
            return coSoGiaoDucEntity.ToDto();
        }

        public async Task<int> DeleteCoSoGiaoDucAsync(int coSoGiaoDucId)
        {
            return await coSoGiaoDucRepository.DeleteCoSoGiaoDucAsync(coSoGiaoDucId);
        }

        public async Task<CoSoGiaoDucDto> EditCoSoGiaoDucAsync(CoSoGiaoDucDto coSoGiaoDuc)
        {
            var coSoGiaoDucEntity = coSoGiaoDuc.ToEntity();
            coSoGiaoDucEntity = await coSoGiaoDucRepository.EditCoSoGiaoDucAsync(coSoGiaoDucEntity);
            return coSoGiaoDucEntity.ToDto();
        }

        public async Task<CoSoGiaoDucDto> GetCoSoGiaoDucByIdAsync(int coSoGiaoDucId)
        {
            var coSoGiaoDucEntity = await coSoGiaoDucRepository.GetCoSoGiaoDucByIdAsync(coSoGiaoDucId);

            if (coSoGiaoDucEntity == null)
            {
                return default;
            }

            return coSoGiaoDucEntity.ToDto();
        }
        public async Task<PagedList<CoSoGiaoDucDto>> GetPagedCoSoGiaoDucAsync(string keyword, int pageNumber, int pageSize)
        {
            var listEntity = await coSoGiaoDucRepository.GetPagedCoSoGiaoDucAsync(keyword, pageNumber, pageSize);
            return listEntity.ToDto();
        }
    }
}
