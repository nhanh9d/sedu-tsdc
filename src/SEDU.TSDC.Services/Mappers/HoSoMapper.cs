﻿using AutoMapper;
using SEDU.TSDC.EntityFramework.Entities;
using SEDU.TSDC.EntityFramework.Extensions.Common;
using SEDU.TSDC.Services.Dtos.Tsdc;

namespace SEDU.TSDC.Services.Mappers
{
    public static class HoSoMapper
    {
        internal static IMapper DtoMapper;
        internal static IMapper EntityMapper;

        static HoSoMapper()
        {
            DtoMapper = new MapperConfiguration(config => config.AddProfile<HoSoDtoMapperProfile>()).CreateMapper();
            EntityMapper = new MapperConfiguration(config => config.AddProfile<HoSoEntityMapperProfile>()).CreateMapper();
        }

        //public static List<Activity> ToModel(this List<APP_ACTIVITY> _APP_ACTIVITY)
        //{
        //    return Mapper.Map<List<Activity>>(_APP_ACTIVITY);
        //}

        //public static Activity ToModel(this APP_ACTIVITY _APP_ACTIVITY)
        //{
        //    return Mapper.Map<Activity>(_APP_ACTIVITY);
        //}

        public static HO_SO ToEntity(this HoSoDto hoSoDto)
        {
            return EntityMapper.Map<HO_SO>(hoSoDto);
        }

        public static HoSoDto ToDto(this HO_SO hoSo)
        {
            return DtoMapper.Map<HoSoDto>(hoSo);
        }

        public static PagedList<HoSoDto> ToDto(this PagedList<HO_SO> hoSo)
        {
            return DtoMapper.Map<PagedList<HoSoDto>>(hoSo);
        }
    }
}
