﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Parameters.NguoiDung
{
    public class CreateNguoiDungParams
    {
        [MaxLength(50, ErrorMessage = "Họ tên tối đa 50 kí tự")]
        public string HoTen { get; set; }
        [MaxLength(12, ErrorMessage = "Số điện thoại tối đa 12 kí tự")]
        public string SoDT { get; set; }
        [MaxLength(64)]
        public string MatKhau { get; set; }
        [MaxLength(100, ErrorMessage = "Email tối đa 12 kí tự")]
        public string Email { get; set; }
        public string DVCToken { get; set; }
    }
}
