﻿

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SEDU.TSDC.API.Parameters.VaiTro
{
    public class EditVaiTroParams: CreateVaiTroParams
    {
        [Required]
        public int Id { get; set; }
    }
}
